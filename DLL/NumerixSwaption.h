#pragma once

#include <iostream>
#include <vector>
#include "NumeriX/Pro/Application.h"
#include "NumeriX/Pro/Basis.h"
#include "NumeriX/Pro/Structuring/ComponentsInstrument.h"
#include "NumeriX/Pro/Currency.h"
#include "NumeriX/Pro/Date.h"
#include "NumeriX/Pro/Structuring/FloatingIndex.h"
#include "NumeriX/Core/Holidays.h"
#include "NumeriX/Pro/Structuring/InterestSchedule.h"
#include "NumeriX/Pro/Kernel/KernelPricer.h"
#include "NumeriX/Pro/Models/Model.h"
#include "NumeriX/Pro/Structuring/OptionsSchedule.h"
#include "NumeriX/Core/RollConvention.h"
#include "NumeriX/Pro/Structuring/Script.h"
#include "NumeriX/Core/Tenor.h"

#include "NumerixMarketData.h"

using namespace std;


class NumerixSwaption : public NumerixMarketData
{

public:NumerixSwaption(NumeriX::Pro::Application& app,
	bool isEuropean_,
	double spread_, double notional_,
	string& numerixIndexFrequency_,
	string& numerixFixedFrequency_, string& numerixFloatingFrequency_,
	double fixedrate_,
	string& IndexBasis_, string& FixedBasis_, string& floatingBasis_, NumeriX::Core::Date& todayDate_,
	NumeriX::Core::Date& startDate_, NumeriX::Core::Date& maturity_);
	   void setEngine(Model& model);
	   double NPV();



private:NumeriX::Pro::Application nxApp;
		bool isEuropean;
		double spread;
		double notional;

		string numerixIndexFrequency;
		string numerixFixedFrequency;
		string numerixFloatingFrequency;

		double fixedrate;

		string IndexBasis;
		string FixedBasis;
		string floatingBasis;

		NumeriX::Core::Date todayDate;
		NumeriX::Core::Date startDate;
		NumeriX::Core::Date maturityDate;

		double npv;
		vector<string> createScript();

};

