#pragma once
#include "EfficiencyLibProduct.h"
#include "NumerixSwap.h"
//#include"MustProduct.h"
using namespace QuantLib;
#include "CurveData.h"
#include "NumeriX/Pro/Application.h"
#include "DllHeader.h"




class EFFICIEBCYLIBDLL_API MustVanilleSwap : public EfficiencyLibProduct
{
public:
	
	MustVanilleSwap(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdldoc, string trId);
	virtual ~MustVanilleSwap(){};
	void setComponentsQuantLib(Handle<QuantLib::YieldTermStructure> forwardingTermStructure);
	void makeSwap(Handle<QuantLib::YieldTermStructure> forwardingTermStructure);
	virtual Real price();
	boost::shared_ptr<Swap>  swap;
	boost::shared_ptr<NumerixSwap> numerixswap;
#pragma region"Swap Vanille Parametres"
private:
	RelinkableHandle<YieldTermStructure> forwardingTermStructure;
	boost::shared_ptr<IborIndex> myIndex;
	Leg fixedLeg;
	Leg floatingLeg;
	EfficiencyProduct::EfficiencyModelProduct modelProduct;
#pragma endregion 
	Real computeDelta(Real shift);
	Real computeGamma(Real shift);
	Real computeVega();
	Real computeTheta();
	
	string convert_basis(QuantLib::DayCounter basis)
	{
		//cette fonction fait la conversion 
		//de la convention de calcul (basis) de type string lue de XML de MUST

		// daycounter;
		if (basis == Actual360())
			return "A360";

		if (basis == Actual365Fixed() )
			return "A365";

		if (basis == ActualActual())
			return "Actual";

		if (basis == Thirty360(Thirty360::EurobondBasis))
			return "30/360";


		return "30/360";

	}
};
