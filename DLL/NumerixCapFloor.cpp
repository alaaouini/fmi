#include "NumerixCapFloor.h"

#include "NumeriX/Pro/Basis.h"
#include "NumeriX/Pro/Structuring/ComponentsInstrument.h"
#include "NumeriX/Pro/Date.h"
#include "NumeriX/Pro/Structuring/DataIndex.h"
#include "NumeriX/Pro/Structuring/FloatingIndex.h"
#include "NumeriX/Pro/Structuring/InterestSchedule.h"
#include "NumeriX/Pro/Kernel/KernelPricer.h"
#include "NumeriX/Pro/Structuring/Script.h"

#include <iostream>
using namespace std;



NumerixCapFloor::NumerixCapFloor(NumeriX::Pro::Application& app, double notional_, string numerixFloatingLegFreq, bool isCap_, NumeriX::Pro::Date& todayDate_, NumeriX::Pro::Date& startDate_, NumeriX::Pro::Date& endDate_, vector<double>& strikes_, string& cashFlowBasis_):
	nxApp(app), notional(notional_), floatingLegFreq(numerixFloatingLegFreq),isCap(isCap_), todayDate(todayDate_), startDate(startDate_), endDate(endDate_), strikes(strikes_), cashFlowBasis(cashFlowBasis_)
{

}

void NumerixCapFloor::setEngine(Model& model)
{
	vector<Index> indices;
	vector<Event> events;

	///
	/// Build a InterestSchedule.
	/// Create an interest rate schedule with specific start and end dates
	/// \param name Name used in script
	/// \param basis Supply basis to enable the events object to supply a DCF index
	/// \param startDate Start date of events
	/// \param endDate End date of events (should be entered unadjusted)
	/// \param frequency Frequency of the events
	/// \param optargs Optional arguments
	///

	string name1("CouponDates");
	NumeriX::Pro::Basis basis1 = cashFlowBasis; //("ACT/360");
	Tenor frequency1 = floatingLegFreq;
	InterestSchedule::OptionalArgsNonIMM optargs1 = InterestSchedule::OptionalArgsNonIMM();
	Tenor fixingLag1("0d");
	optargs1.setFixingLag(fixingLag1);
	bool frontPay1 = false;
	optargs1.setFrontPay(frontPay1);
	Holidays payCalendar("EUTA");
	optargs1.setPayCalendar(payCalendar);
	RollConvention payConvention1 = RollConvention::MF;
	optargs1.setPayConvention(payConvention1);

	cout << "NumerixCapFloor::setEngine CouponDates event: basis:" << cashFlowBasis << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: freq:" << floatingLegFreq << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: fixingLag:" << "0d" << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: frontPay:" << false << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: payCalendar:" << "EUTA" << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: payConvention:" << "RollConvention::MF" << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: startDate:" << startDate << endl;
	cout << "NumerixCapFloor::setEngine CouponDates event: endDate:" << endDate << endl;
	InterestSchedule event1(nxApp, name1, basis1, startDate, endDate, frequency1, optargs1);
	events.push_back(event1);
	
	///
	/// Build a FloatingIndex.
	/// Create a LIBOR index for the specified Tenor of LIBOR
	/// \param name Name used in script
	/// \param tenor Tenor of the floating-rate period
	/// \param currency Object's currency
	/// \param accrualCalendar Code for holiday calendar[s], eg USNY GBLO, for accrual dates
	/// \param accrualConvention Method of adjusting accrual dates. Typically MF,F or None (ie unadjusted)
	/// \param fixingCalendar Code for holiday calendar[s], eg USNY GBLO for fixing dates
	/// \param spotLag Period between fixing and accrual start. Typically 2BD or 0BD
	/// \param optargs Optional arguments
	///
	string name2("Floating");
	Tenor tenor2 = floatingLegFreq;
	Currency currency2("EUR");
	Holidays accrualCalendar2("EUTA");
	Tenor spotLag2("2BD");
	RollConvention accrualConvention2 = RollConvention::MF;
	Holidays fixingCalendar2("EUTA");
	FloatingIndex::OptionalArgsTenor optargs2;
	optargs2.setFixingConvention("P");
	cout << "NumerixCapFloor::setEngine Floating index: floatingLegFreq:" << floatingLegFreq << endl;
	cout << "NumerixCapFloor::setEngine Floating index: currency:" << "EUR" << endl;
	cout << "NumerixCapFloor::setEngine Floating index: accrualCalendar:" << "EUTA" << endl;
	cout << "NumerixCapFloor::setEngine Floating index: spotLag:" << "2BD" << endl;
	cout << "NumerixCapFloor::setEngine Floating index: accrualConvention:" << "RollConvention::MF" << endl;
	cout << "NumerixCapFloor::setEngine Floating index: fixingCalendar:" << "EUTA" << endl;
	cout << "NumerixCapFloor::setEngine Floating index: fixingConvention:" << "P" << endl;
	FloatingIndex index2(nxApp, name2, tenor2, currency2, accrualCalendar2, accrualConvention2, fixingCalendar2, spotLag2, optargs2);
	indices.push_back(index2);


	///
	/// Build a DataIndex.
	/// Create a data index from a list of values
	/// \param name Name used in script
	/// \param values List of values for each period in chosen events
	///
	string name3("StrikeDataIndex");
	DataIndex index3(nxApp, name3, strikes, name1);
	indices.push_back(index3);

	cout << "NumerixCapFloor::setEngine StrikeDataIndex:name" << name3 << endl;
	for (int i = 0; i < strikes.size(); i++)
	{
		cout << "NumerixCapFloor::setEngine StrikeDataIndex:strikes["<< i << "]=" << strikes[i] << endl;
	}
	cout << "NumerixCapFloor::setEngine StrikeDataIndex:events:" << name1 << endl;


	std::vector<ComponentsInstrument::Data> dataNameValue;
	dataNameValue.push_back(ComponentsInstrument::Data(string("Notional"), notional));

	// Load Script
	Script script(nxApp, createScript(), Script::BACKWARD);
	ComponentsInstrument componentsInstrument(nxApp, indices, events, dataNameValue, script);

	cout << "NumerixCapFloor::setEngine Notional:" << notional << endl;


	string method("BackwardAnalytic");//BackwardLattice
	MethodParameters quality(0);
	KernelPricer::OptionalArgs optargs;

	cout << "NumerixCapFloor::setEngine todayDate:" << todayDate << endl;
	KernelPricer kernelPricer(nxApp, componentsInstrument, model, method, quality, todayDate, optargs);

	cout << "NumerixCapFloor::setEngine method:" << method << endl;
	cout << "NumerixCapFloor::setEngine quality:" << 0 << endl;

	if (isCap)
	{
	    string cap = "Cap";
		npv = kernelPricer.getPV(cap);
    }
	else
	{
		string floor = "Floor";
		npv = kernelPricer.getPV(floor);
	}

	/*
	const std::string capLogName = "CapLog";
	const NumeriX::Pro::KernelPricer::PaymentLog*  capLog = kernelPricer.getPaymentLog(capLogName);

	const std::string floorLogName = "FloorLog";
	const NumeriX::Pro::KernelPricer::PaymentLog*  floorLog = kernelPricer.getPaymentLog(floorLogName);


	for (int i = 0; i < capLog->size(); i++)
	{
		NumeriX::Pro::Date  date = capLog->getDate(i);
		string dateString = date.toString();
		cout << " NumerixCapFloor::setEngine capLog dateString: " << dateString << endl;


		double pv = capLog->getPV(date);
		cout << " NumerixCapFloor::setEngine capLog pv: " << pv << endl;
		


		double actual = capLog->getActual(date);
		cout << " NumerixCapFloor::setEngine capLog actual: " << actual << endl;

		cout << "---------------------------------" << endl;

	}

	for (int i = 0; i < floorLog->size(); i++)
	{
		NumeriX::Pro::Date  date = floorLog->getDate(i);
		string dateString = date.toString();
		cout << " NumerixCapFloor::setEngine floorLog dateString: " << dateString << endl;

		double pv = floorLog->getPV(date);
		cout << " NumerixCapFloor::setEngine floorLog pv: " << pv << endl;



		double actual = floorLog->getActual(date);
		cout << " NumerixCapFloor::setEngine floorLog actual: " << actual << endl;

		cout << "---------------------------------" << endl;

	}*/

}

double NumerixCapFloor::NPV()
{
	return npv;
}

vector<string> NumerixCapFloor::createScript() {
	const char *s[] =
	{
		"PRODUCTS",
		"    DISCOUNTING Cap, Floor",
		"    TEMPORARY Caplet, Floorlet",
		"    PAYMENTLOG CapLog, FloorLog",
		"END PRODUCTS",
		"PAYOFFSCRIPT",
		"    IF ISACTIVE(CouponDates) THEN",
		"        Caplet = MAX(Floating - StrikeDataIndex, 0) * Notional * CouponDatesDCF",
		"        Cap += CASH(Caplet, CouponDates, THISPAY)",
		"        LOGPAYMENT(Caplet, CouponDates, THISPAY, CapLog)",
		"        Floorlet = MAX(StrikeDataIndex - Floating, 0) * Notional * CouponDatesDCF", 
		"        Floor += CASH(Floorlet, CouponDates, THISPAY)",
		"        LOGPAYMENT(Floorlet, CouponDates, THISPAY, FloorLog)",
		"    END IF",
		"END PAYOFFSCRIPT"
	};
	vector<string> scriptVector;
	int length = sizeof(s) / sizeof(*s);
	for (int i = 0; i<length; ++i)
		scriptVector.push_back(s[i]);
	return scriptVector;
}

