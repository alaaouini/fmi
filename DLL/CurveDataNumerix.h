// Test_09_01_2017_b.cpp : Defines the entry point for the console application.
//
#pragma once

#include "NumeriX/Pro/Application.h"
#include "NumeriX/Pro/ApplicationException.h"
#include "NumeriX/Pro/Instruments/CashDeposit.h"
#include "NumeriX/Pro/Instruments/FRA.h"
#include "NumeriX/Pro/Instruments/Instrument.h"
#include "NumeriX/Pro/Instruments/InstrumentCollection.h"
#include "NumeriX/Pro/Instruments/IRFuture.h"
#include "NumeriX/Pro/MarketInformation/MarketQuotes.h"
#include "NumeriX/Pro/Tenor.h"
#include "NumeriX/Pro/Instruments/VanillaIRSwap.h"
#include "NumeriX/Pro/MarketInformation/YieldCurve.h"

#include <boost/format.hpp>

#include "NumerixMarketData.h"

using namespace NumeriX::Pro;
using namespace std;




class CurveDataNumerix:public NumerixMarketData {
private:
	NumeriX::Pro::Application nxApp;

	boost::shared_ptr<MarketQuotes::QuoteData>  s1yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s2yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s3yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s4yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s5yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s6yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s7yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s8yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s9yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s10yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s11yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s12yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s13yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s14yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s15yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s16yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s17yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s18yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s19yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s20yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s21yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s22yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s23yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s24yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s25yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s26yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s27yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s28yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s29yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s30yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s35yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s40yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s45yRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  s50yRate;



	boost::shared_ptr<MarketQuotes::QuoteData>  Mm1dRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm1wRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm2wRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm3wRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm1mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm2mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm3mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm4mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm5mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm6mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm9mRate;
	boost::shared_ptr<MarketQuotes::QuoteData>  Mm12mRate;







public:
	CurveDataNumerix(NumeriX::Pro::Application& app) :nxApp(app),
		s1yRate(new MarketQuotes::QuoteData("s1yRate", 0.00421)),
		s2yRate(new MarketQuotes::QuoteData("s2yRate", 0.00551)),
		s3yRate(new MarketQuotes::QuoteData("s3yRate", 0.00724)),
		s4yRate(new MarketQuotes::QuoteData("s4yRate", 0.00947)),
		s5yRate(new MarketQuotes::QuoteData("s5yRate", 0.01166)),
		s6yRate(new MarketQuotes::QuoteData("s6yRate", 0.013745)),
		s7yRate(new MarketQuotes::QuoteData("s7yRate", 0.0156)),
		s8yRate(new MarketQuotes::QuoteData("s8yRate", 0.01728)),
		s9yRate(new MarketQuotes::QuoteData("s9yRate", 0.01879)),
		s10yRate(new MarketQuotes::QuoteData("s10yRate", 0.02015)),
		s11yRate(new MarketQuotes::QuoteData("s11yRate", 0.02133)),
		s12yRate(new MarketQuotes::QuoteData("s12yRate", 0.02236)),
		s13yRate(new MarketQuotes::QuoteData("s13yRate", 0.02323)),
		s14yRate(new MarketQuotes::QuoteData("s14yRate", 0.02396)),
		s15yRate(new MarketQuotes::QuoteData("s15yRate", 0.02457)),
		s16yRate(new MarketQuotes::QuoteData("s16yRate", 0.02504)),
		s17yRate(new MarketQuotes::QuoteData("s17yRate", 0.02541)),
		s18yRate(new MarketQuotes::QuoteData("s18yRate", 0.02568)),
		s19yRate(new MarketQuotes::QuoteData("s19yRate", 0.02589)),
		s20yRate(new MarketQuotes::QuoteData("s20yRate", 0.02577)),
		s21yRate(new MarketQuotes::QuoteData("s21yRate", 0.02618)),
		s22yRate(new MarketQuotes::QuoteData("s22yRate", 0.02628)),
		s23yRate(new MarketQuotes::QuoteData("s23yRate", 0.02637)),
		s24yRate(new MarketQuotes::QuoteData("s24yRate", 0.02643)),
		s25yRate(new MarketQuotes::QuoteData("s25yRate", 0.02648)),
		s26yRate(new MarketQuotes::QuoteData("s26yRate", 0.02621)),
		s27yRate(new MarketQuotes::QuoteData("s27yRate", 0.02653)),
		s28yRate(new MarketQuotes::QuoteData("s28yRate", 0.02654)),
		s29yRate(new MarketQuotes::QuoteData("s29yRate", 0.02656)),
		s30yRate(new MarketQuotes::QuoteData("s30yRate", 0.02628)),
		s35yRate(new MarketQuotes::QuoteData("s35yRate", 0.026639)),
		s40yRate(new MarketQuotes::QuoteData("s40yRate", 0.02685)),
		s45yRate(new MarketQuotes::QuoteData("s45yRate", 0.027027)),
		s50yRate(new MarketQuotes::QuoteData("s50yRate", 0.02723)),
		Mm1dRate(new MarketQuotes::QuoteData("Mm1dRate", 0.00103)),
		Mm1wRate(new MarketQuotes::QuoteData("Mm1wRate", 0.00102)),
		Mm2wRate(new MarketQuotes::QuoteData("Mm2wRate", 0.00109)),
		Mm3wRate(new MarketQuotes::QuoteData("Mm3wRate", 0.00117)),
		Mm1mRate(new MarketQuotes::QuoteData("Mm1mRate", 0.00129)),
		Mm2mRate(new MarketQuotes::QuoteData("Mm2mRate", 0.00178)),
		Mm3mRate(new MarketQuotes::QuoteData("Mm3mRate", 0.00228)),
		Mm4mRate(new MarketQuotes::QuoteData("Mm4mRate", 0.00265)),
		Mm5mRate(new MarketQuotes::QuoteData("Mm5mRate", 0.00265)),
		Mm6mRate(new MarketQuotes::QuoteData("Mm6mRate", 0.00306)),
		Mm9mRate(new MarketQuotes::QuoteData("Mm9mRate", 0.00355))
		//Mm12mRate(new MarketQuotes::QuoteData("Mm12mRate",  0.0037)),
	{
	}



	void bumpCurve(CurveDataNumerix &cd, double bump) {
		// swaps


		cd.s1yRate->value_m = s1yRate->value_m + bump;
		cd.s2yRate->value_m = s2yRate->value_m + bump;
		cd.s3yRate->value_m = s3yRate->value_m + bump;
		cd.s4yRate->value_m = s4yRate->value_m + bump;

		cd.s5yRate->value_m = s5yRate->value_m + bump;
		cd.s6yRate->value_m = s6yRate->value_m + bump;
		cd.s7yRate->value_m = s7yRate->value_m + bump;
		cd.s8yRate->value_m = s8yRate->value_m + bump;
		cd.s9yRate->value_m = s9yRate->value_m + bump;

		cd.s10yRate->value_m = s10yRate->value_m + bump;
		cd.s11yRate->value_m = s11yRate->value_m + bump;
		cd.s12yRate->value_m = s12yRate->value_m + bump;
		cd.s13yRate->value_m = s13yRate->value_m + bump;
		cd.s14yRate->value_m = s14yRate->value_m + bump;
		cd.s15yRate->value_m = s15yRate->value_m + bump;
		cd.s16yRate->value_m = s16yRate->value_m + bump;
		cd.s17yRate->value_m = s17yRate->value_m + bump;


		cd.s18yRate->value_m = s18yRate->value_m + bump;
		cd.s19yRate->value_m = s19yRate->value_m + bump;
		cd.s20yRate->value_m = s20yRate->value_m + bump;
		cd.s21yRate->value_m = s21yRate->value_m + bump;
		cd.s22yRate->value_m = s22yRate->value_m + bump;
		cd.s23yRate->value_m = s23yRate->value_m + bump;
		cd.s24yRate->value_m = s24yRate->value_m + bump;
		cd.s25yRate->value_m = s25yRate->value_m + bump;

		cd.s26yRate->value_m = s26yRate->value_m + bump;
		cd.s27yRate->value_m = s27yRate->value_m + bump;
		cd.s28yRate->value_m = s28yRate->value_m + bump;
		cd.s29yRate->value_m = s29yRate->value_m + bump;
		cd.s30yRate->value_m = s30yRate->value_m + bump;

		cd.s35yRate->value_m = s35yRate->value_m + bump;
		cd.s40yRate->value_m = s40yRate->value_m + bump;
		cd.s45yRate->value_m = s45yRate->value_m + bump;
		cd.s50yRate->value_m = s50yRate->value_m + bump;


		//Money Market

		cd.Mm1dRate->value_m = Mm1dRate->value_m + bump;
		cd.Mm1wRate->value_m = Mm1wRate->value_m + bump;
		cd.Mm2wRate->value_m = Mm2wRate->value_m + bump;
		cd.Mm3wRate->value_m = Mm3wRate->value_m + bump;
		cd.Mm1mRate->value_m = Mm1mRate->value_m + bump;
		cd.Mm2mRate->value_m = Mm2mRate->value_m + bump;
		cd.Mm3mRate->value_m = Mm3mRate->value_m + bump;
		cd.Mm4mRate->value_m = Mm4mRate->value_m + bump;
		cd.Mm5mRate->value_m = Mm5mRate->value_m + bump;
		cd.Mm6mRate->value_m = Mm6mRate->value_m + bump;
		cd.Mm9mRate->value_m = Mm9mRate->value_m + bump;
		//cd.Mm12mRate->value_m = Mm12mRate->value_m + bump;
	}



	YieldCurve buildCurve(const CurveDataNumerix &cd, NumeriX::Pro::Date& settlementDate) {

		loadGlobalConventions(nxApp);

		std::vector<NumeriX::Pro::Instrument > instruments;
 

		string EUR_Swap_AnnMoney = "EUR_Swap_AnnMoney";
		Conventions eurSwapAnnMoneyConvention = EUR_Swap_AnnMoney;
		Tenor s1yNumerixEndTenor = "1Y";
		NumeriX::Pro::Quote s1yNumerixQuote = cd.s1yRate->value_m;
		Tenor interval = "2B";
		instruments.push_back(VanillaIRSwap(nxApp, currency, s1yNumerixEndTenor, s1yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s2yNumerixEndTenor = "2Y";
		NumeriX::Pro::Quote s2yNumerixQuote = cd.s2yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s2yNumerixEndTenor, s2yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));


		Tenor s3yNumerixEndTenor = "3Y";
		NumeriX::Pro::Quote s3yNumerixQuote = cd.s3yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s3yNumerixEndTenor, s3yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s4yNumerixEndTenor = "4Y";
		NumeriX::Pro::Quote s4yNumerixQuote = cd.s4yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s4yNumerixEndTenor, s4yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s5yNumerixEndTenor = "5Y";
		NumeriX::Pro::Quote s5yNumerixQuote = cd.s5yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s5yNumerixEndTenor, s5yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s6yNumerixEndTenor = "6Y";
		NumeriX::Pro::Quote s6yNumerixQuote = cd.s6yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s6yNumerixEndTenor, s6yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s7yNumerixEndTenor = "7Y";
		NumeriX::Pro::Quote s7yNumerixQuote = cd.s7yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s7yNumerixEndTenor, s7yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s8yNumerixEndTenor = "8Y";
		NumeriX::Pro::Quote s8yNumerixQuote = cd.s8yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s8yNumerixEndTenor, s8yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s9yNumerixEndTenor = "9Y";
		NumeriX::Pro::Quote s9yNumerixQuote = cd.s9yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s9yNumerixEndTenor, s9yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s10yNumerixEndTenor = "10Y";
		NumeriX::Pro::Quote s10yNumerixQuote = cd.s10yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s10yNumerixEndTenor, s10yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s11yNumerixEndTenor = "11Y";
		NumeriX::Pro::Quote s11yNumerixQuote = cd.s11yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s11yNumerixEndTenor, s11yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s12yNumerixEndTenor = "12Y";
		NumeriX::Pro::Quote s12yNumerixQuote = cd.s12yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s12yNumerixEndTenor, s12yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s13yNumerixEndTenor = "13Y";
		NumeriX::Pro::Quote s13yNumerixQuote = cd.s13yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s13yNumerixEndTenor, s13yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s14yNumerixEndTenor = "14Y";
		NumeriX::Pro::Quote s14yNumerixQuote = cd.s14yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s14yNumerixEndTenor, s14yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s15yNumerixEndTenor = "15Y";
		NumeriX::Pro::Quote s15yNumerixQuote = cd.s15yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s15yNumerixEndTenor, s15yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s16yNumerixEndTenor = "16Y";
		NumeriX::Pro::Quote s16yNumerixQuote = cd.s16yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s16yNumerixEndTenor, s16yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s17yNumerixEndTenor = "17Y";
		NumeriX::Pro::Quote s17yNumerixQuote = cd.s17yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s17yNumerixEndTenor, s17yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s18yNumerixEndTenor = "18Y";
		NumeriX::Pro::Quote s18yNumerixQuote = cd.s18yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s18yNumerixEndTenor, s18yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s19yNumerixEndTenor = "19Y";
		NumeriX::Pro::Quote s19yNumerixQuote = cd.s19yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s19yNumerixEndTenor, s19yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s20yNumerixEndTenor = "20Y";
		NumeriX::Pro::Quote s20yNumerixQuote = cd.s20yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s20yNumerixEndTenor, s20yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s21yNumerixEndTenor = "21Y";
		NumeriX::Pro::Quote s21yNumerixQuote = cd.s21yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s21yNumerixEndTenor, s21yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s22yNumerixEndTenor = "22Y";
		NumeriX::Pro::Quote s22yNumerixQuote = cd.s22yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s22yNumerixEndTenor, s22yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s23yNumerixEndTenor = "23Y";
		NumeriX::Pro::Quote s23yNumerixQuote = cd.s23yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s23yNumerixEndTenor, s23yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s24yNumerixEndTenor = "24Y";
		NumeriX::Pro::Quote s24yNumerixQuote = cd.s24yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s24yNumerixEndTenor, s24yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s25yNumerixEndTenor = "25Y";
		NumeriX::Pro::Quote s25yNumerixQuote = cd.s25yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s25yNumerixEndTenor, s25yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s26yNumerixEndTenor = "26Y";
		NumeriX::Pro::Quote s26yNumerixQuote = cd.s26yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s26yNumerixEndTenor, s26yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s27yNumerixEndTenor = "27Y";
		NumeriX::Pro::Quote s27yNumerixQuote = cd.s27yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s27yNumerixEndTenor, s27yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s28yNumerixEndTenor = "28Y";
		NumeriX::Pro::Quote s28yNumerixQuote = cd.s28yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s28yNumerixEndTenor, s28yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s29yNumerixEndTenor = "29Y";
		NumeriX::Pro::Quote s29yNumerixQuote = cd.s29yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s29yNumerixEndTenor, s29yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s30yNumerixEndTenor = "30Y";
		NumeriX::Pro::Quote s30yNumerixQuote = cd.s30yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s30yNumerixEndTenor, s30yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s35yNumerixEndTenor = "35Y";
		NumeriX::Pro::Quote s35yNumerixQuote = cd.s35yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s35yNumerixEndTenor, s35yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s40yNumerixEndTenor = "40Y";
		NumeriX::Pro::Quote s40yNumerixQuote = cd.s40yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s40yNumerixEndTenor, s40yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s45yNumerixEndTenor = "45Y";
		NumeriX::Pro::Quote s45yNumerixQuote = cd.s45yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s45yNumerixEndTenor, s45yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));

		Tenor s50yNumerixEndTenor = "50Y";
		NumeriX::Pro::Quote s50yNumerixQuote = cd.s50yRate->value_m;
		instruments.push_back(VanillaIRSwap(nxApp, currency, s50yNumerixEndTenor, s50yNumerixQuote, 2, interval, eurSwapAnnMoneyConvention));


 

		// setup Deposit/Forward

 

		Conventions eurShortRatesConvention = "EUR_ShortRates";
		Conventions eurLiborConvention = "EUR_LIBOR";
		NumeriX::Pro::Quote Mm1dNumerixQuote = cd.Mm1dRate->value_m;
		Tenor Mm1dNumerixMaturityTenor = "1D";
		instruments.push_back(CashDeposit(nxApp, currency, Mm1dNumerixQuote, Mm1dNumerixMaturityTenor, 3, interval, eurShortRatesConvention));


		NumeriX::Pro::Quote Mm1wNumerixQuote = cd.Mm1wRate->value_m;
		Tenor Mm1wNumerixMaturityTenor = "1W";
		instruments.push_back(CashDeposit(nxApp, currency, Mm1wNumerixQuote, Mm1wNumerixMaturityTenor, 3, interval, eurShortRatesConvention));

		NumeriX::Pro::Quote Mm2wNumerixQuote = cd.Mm2wRate->value_m;
		Tenor Mm2wNumerixMaturityTenor = "2W";
		instruments.push_back(CashDeposit(nxApp, currency, Mm2wNumerixQuote, Mm2wNumerixMaturityTenor, 3, interval, eurShortRatesConvention));

		NumeriX::Pro::Quote Mm3wNumerixQuote = cd.Mm3wRate->value_m;
		Tenor Mm3wNumerixMaturityTenor = "3W";
		instruments.push_back(CashDeposit(nxApp, currency, Mm3wNumerixQuote, Mm3wNumerixMaturityTenor, 3, interval, eurShortRatesConvention));

		NumeriX::Pro::Quote Mm1mNumerixQuote = cd.Mm1mRate->value_m;
		Tenor Mm1mNumerixMaturityTenor = "1M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm1mNumerixQuote, Mm1mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm2mNumerixQuote = cd.Mm2mRate->value_m;
		Tenor Mm2mNumerixMaturityTenor = "2M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm2mNumerixQuote, Mm2mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm3mNumerixQuote = cd.Mm3mRate->value_m;
		Tenor Mm3mNumerixMaturityTenor = "3M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm3mNumerixQuote, Mm3mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm4mNumerixQuote = cd.Mm4mRate->value_m;
		Tenor Mm4mNumerixMaturityTenor = "4M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm4mNumerixQuote, Mm4mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm5mNumerixQuote = cd.Mm5mRate->value_m;
		Tenor Mm5mNumerixMaturityTenor = "5M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm5mNumerixQuote, Mm5mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm6mNumerixQuote = cd.Mm6mRate->value_m;
		Tenor Mm6mNumerixMaturityTenor = "6M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm6mNumerixQuote, Mm6mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		NumeriX::Pro::Quote Mm9mNumerixQuote = cd.Mm9mRate->value_m;
		Tenor Mm9mNumerixMaturityTenor = "9M";
		instruments.push_back(CashDeposit(nxApp, currency, Mm9mNumerixQuote, Mm9mNumerixMaturityTenor, 3, interval, eurLiborConvention));

		/*NumeriX::Pro::Quote Mm12mNumerixQuote = cd.Mm12mRate->value_m;
		Tenor Mm12mNumerixMaturityTenor = "12M";
		instruments.push_back(CashDeposit(nxApp, NumerixMarketData::currency, Mm12mNumerixQuote, Mm12mNumerixMaturityTenor, 3, interval, eurLiborConvention));*/




		NumeriX::Pro::YieldCurve::InterpolationType  interpolationType = YieldCurve::LINEAR_ZERO;
		NumeriX::Pro::YieldCurve result(nxApp, currency, InstrumentCollection(nxApp, instruments), interpolationType, settlementDate);



		return result;
	}





};