#pragma once
#include <ql/quantlib.hpp>
#include "CurveDataNumerix.h"
#include "MustSwaption.h"
#include "NumeriX/Pro/Models/HW1F.h" 

#include "NumerixSwaptionVolatility.h"
#include "NumerixUtilities.h"

MustSwaption::MustSwaption(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdldoc, string trId) : EfficiencyLibProduct(valuationDate, typeProd, paths, modelProd, hdldoc, trId){
	modelProduct = modelProd;
}
void MustSwaption::setComponentsQuantLib(){
	
	this->setComponentsForSwaption();
	//index
	MustSwaption::myIndex = ComponentIndexMust.ConstructIndex(forwardingTermStructure);

	//FixedLeg
	SwapFixedLeg fixed_Leg;
	fixed_Leg.setComponent(fixedRateObj.matrixRate, fixedLegObj.basisQ, floatingLegObj.freqQ);



	////FloatingLeg
	SwapFloatingLeg floating_leg;
	floating_leg.setComponent(MustSwaption::myIndex, floatingLegObj.basisQ, floatingLegObj.freqQ);


	if (ComponentIndexMust.spreads.size()<1) //condition to check the Sread
	{
		MustSwaption::fixedLeg = fixed_Leg.constructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
		MustSwaption::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
	}
	else
	{
		MustSwaption::fixedLeg = fixed_Leg.constructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
		MustSwaption::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal, ComponentIndexMust.matrixSpread);
	}

	//SwapType
	VanillaSwap::Type swapType = VanillaSwap::Payer;
	Settlement::Type settlementType = Settlement::Physical;
	if (style == EfficiencyProduct::SwaptionStyle::BERM){
		/* set vetcor of exercise dates*/
		QuantLib::Date exDate = startDateObj.dateQ;
		while (exDate <= endDateObj.dateQ)
		{
			exerciseDates.push_back(exDate);
			exDate.operator+=(Period(componentOption.freqQ));
		}

	}

}
std::vector<Real> MustSwaption::fixedRatesFreq(std::vector<std::tuple<QuantLib::Date, QuantLib::Date, Real>> matrixRate, QuantLib::Date settlementDate, QuantLib::Date maturity, Frequency fixedLegFrequency)
{
	std::vector<Real> fixedRateFreq;

	QuantLib::Date startDate = settlementDate;
	startDate.operator+=(Period(fixedLegFrequency));
	int j = 0;
	while (startDate < maturity){

		while (startDate > std::get<1>(matrixRate[j])) { j++; }

		fixedRateFreq.push_back(std::get<2>(matrixRate[j]));
		startDate.operator+=(Period(fixedLegFrequency));
	}
	return fixedRateFreq;
}

boost::shared_ptr< VanillaSwap >  MustSwaption::constructVanillaSwap(boost::shared_ptr<IborIndex> myIndex){

	//FixedLeg
	SwapFixedLeg fixed_Leg;
	fixed_Leg.setComponent(fixedRateObj.matrixRate, fixedLegObj.basisQ, floatingLegObj.freqQ);

	cout << "MustSwaption::constructVanillaSwap floatingLegObj.basisQ:" << floatingLegObj.basisQ << endl;
	cout << "MustSwaption::constructVanillaSwap floatingLegObj.freqQ:" << floatingLegObj.freqQ << endl;

	//FloatingLeg
	SwapFloatingLeg floating_leg;
	floating_leg.setComponent(MustSwaption::myIndex, floatingLegObj.basisQ, floatingLegObj.freqQ);

	VanillaSwap::Type swapType = VanillaSwap::Payer;
	Settlement::Type settlementType = Settlement::Cash;


	Real fixedrate = MustSwaption::fixedRatesFreq(fixedRateObj.matrixRate, maturityObj.dateQ, endDateObj.dateQ, fixedLegObj.freqQ)[0];
	
	cout << "MustSwaption::constructVanillaSwap fixedrate:" << fixedrate << endl;

	switch (style){
	case EfficiencyProduct::SwaptionStyle::EURO:
	{
		boost::shared_ptr<VanillaSwap> swap(
			new VanillaSwap(swapType, ComponentPrincipalMust.nominal[0],
			fixed_Leg.fixedSchedule(maturityObj.dateQ, endDateObj.dateQ), fixedrate, fixedLegObj.basisQ,
			floating_leg.floatSchedule(maturityObj.dateQ, endDateObj.dateQ), myIndex, 0.0,
			floatingLegObj.basisQ));
		return swap;
	}
	break;
	case EfficiencyProduct::SwaptionStyle::AMER:
	{
		QuantLib::Date startDate = TARGET().advance(startDateObj.dateQ, 2, Days);
		boost::shared_ptr<VanillaSwap> swap(
			new VanillaSwap(swapType, ComponentPrincipalMust.nominal[0],
			fixed_Leg.fixedSchedule(startDate, endDateObj.dateQ), fixedrate, fixedLegObj.basisQ,
			floating_leg.floatSchedule(startDate, endDateObj.dateQ), myIndex, 0.0,
			floatingLegObj.basisQ));
		return swap;
	}
	break;
	case EfficiencyProduct::SwaptionStyle::BERM:
	{
		QuantLib::Date startDate = startDateObj.dateQ.operator+=(Period(componentOption.freqQ));

		boost::shared_ptr<VanillaSwap> swap(
			new VanillaSwap(swapType, ComponentPrincipalMust.nominal[0],
			fixed_Leg.fixedSchedule(startDate, endDateObj.dateQ), fixedrate, fixedLegObj.basisQ,
			floating_leg.floatSchedule(startDate, endDateObj.dateQ), myIndex, 0.0,
			floatingLegObj.basisQ));
		return swap;
	}
	break;
	default:
	{
		boost::shared_ptr<VanillaSwap> swap(
			new VanillaSwap(swapType, ComponentPrincipalMust.nominal[0],
			fixed_Leg.fixedSchedule(maturityObj.dateQ, endDateObj.dateQ), fixedrate, fixedLegObj.basisQ,
			floating_leg.floatSchedule(maturityObj.dateQ, endDateObj.dateQ), myIndex, 0.0,
			floatingLegObj.basisQ));
		return swap;
	}
	break;
	}

	boost::shared_ptr<VanillaSwap> swap(
		new VanillaSwap(swapType, ComponentPrincipalMust.nominal[0],
		fixed_Leg.fixedSchedule(maturityObj.dateQ, endDateObj.dateQ), fixedrate, fixedLegObj.basisQ,
		floating_leg.floatSchedule(maturityObj.dateQ, endDateObj.dateQ), myIndex, 0.0,
		floatingLegObj.basisQ));
	return swap;
}
void MustSwaption::makeSwaption(Handle<QuantLib::YieldTermStructure> forwardingTermStructure){
	
	this->setComponentsQuantLib();
	boost::shared_ptr<VanillaSwap> swap = MustSwaption::constructVanillaSwap(myIndex);

	cout << " MustSwaption::makeSwaption maturityObj.dateQ:" << maturityObj.dateQ << endl;
	cout << " MustSwaption::makeSwaption settlementType:" << settlementType << endl;

	switch (style){
		case EfficiencyProduct::SwaptionStyle::EURO:
		{
			
			MustSwaption::swaption = boost::shared_ptr<QuantLib::Swaption>(new QuantLib::Swaption(swap, boost::shared_ptr<Exercise>(
				new EuropeanExercise(maturityObj.dateQ)),
				settlementType));
		}
		break;
		case EfficiencyProduct::SwaptionStyle::AMER:
		{
			QuantLib::Date startDate = TARGET().advance(startDateObj.dateQ, 2, Days);
			MustSwaption::swaption = boost::shared_ptr<QuantLib::Swaption>(new QuantLib::Swaption(swap, boost::shared_ptr<Exercise>(
				new AmericanExercise(startDate, maturityObj.dateQ, false)), settlementType));
		}
		break;
		case EfficiencyProduct::SwaptionStyle::BERM:
		{
			MustSwaption::swaption = boost::shared_ptr<QuantLib::Swaption>(new QuantLib::Swaption(swap, boost::shared_ptr<Exercise>(
				new BermudanExercise(exerciseDates)),
				settlementType));
		}
		break;
		default:
		{
			MustSwaption::swaption = boost::shared_ptr<QuantLib::Swaption>(new QuantLib::Swaption(swap, boost::shared_ptr<Exercise>(
				new EuropeanExercise(maturityObj.dateQ)),
				settlementType));
		}
		break;
		}
}
Real MustSwaption::price(){
	std::cout << "--->MustSwaption::price startDateObj.dateQ:" << startDateObj.dateQ << endl;
	std::cout << "--->MustSwaption::price endDateObj.dateQ:" << endDateObj.dateQ << endl;
	std::cout << "--->MustSwaption::price floatingLegObj.freqQ:" << floatingLegObj.freqQ << endl;
	std::cout << "--->MustSwaption::price today:" << today << endl;
	switch (modelProduct) {
		case EfficiencyProduct::EfficiencyModelProduct::EFFICIENCY_LMM:
		{
		LMMEfficiencyEngine* lmmEngine = new  LMMEfficiencyEngine(startDateObj.dateQ, endDateObj.dateQ, floatingLegObj.freqQ);

		////TO DO
		//
		SwaptionEfficiencyPricer SwaptionPricer;
		SwaptionPricer.SetEngine(lmmEngine);
		//loat strike, Frequency Frequency, Date debut, Date fin, Date datedecalcul

		float strikex = MustSwaption::fixedRatesFreq(fixedRateObj.matrixRate, maturityObj.dateQ, endDateObj.dateQ, fixedLegObj.freqQ)[0];
		Frequency Frequencyx = floatingLegObj.freqQ;
		QuantLib::Date debutx = startDateObj.dateQ;
		QuantLib::Date finx = endDateObj.dateQ;
		QuantLib::Date datedecalculx = startDateObj.dateQ;


		return SwaptionPricer.Price(strikex, floatingLegObj.freqQ, startDateObj.dateQ, endDateObj.dateQ, startDateObj.dateQ);
		//return 0.0;
		}
		break;
		case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
		{
			//TO DO
			boost::shared_ptr<PricingEngine> QLMMEngine = setEngineQuantLibLMM();
			swaption->setPricingEngine(QLMMEngine);
			Real npv = swaption->NPV();
			try{
				vega = swaption->result<Real>("vega");
				// vega = riskparameter.
			}
			catch (...){
				vega = 0;
			}
			return npv;
		} 
		break;
		case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_BLACKFT:
		{
			boost::shared_ptr<PricingEngine> QBftEngine = setEngineQuantLibBlackFT();
			swaption->setPricingEngine(QBftEngine);
			Real npv = swaption->NPV();
			Real shift = 0.001;
			/*delta = computeDelta(shift);
			vega = this->computeVega();*/
			return npv;
		}
		break;
		case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_HULLWHITE:
		{
			//TO DO
			boost::shared_ptr<PricingEngine> HwEngine = setEngineQuantLibHullWhite();
			swaption->setPricingEngine(HwEngine);
			return	swaption->NPV();
		}
		break;
		case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_BLACKFT:
		{
			this->setComponentsForNumerixSwaption();
			boost::shared_ptr<NumeriX::Pro::SwaptionAnalytic> swaptionAnalytic = MustSwaption::setEngineNumerixBlackFT();

			delta = swaptionAnalytic->getDelta();
			gamma = swaptionAnalytic->getGamma();
			vega = swaptionAnalytic->getVega();
			double npv = swaptionAnalytic->getPrice();

			cout << " MustCapFloor::price() npv:=" << npv << endl;

			return npv;
		}
		break;
		case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_HW:
		{
			this->setComponentsForNumerixSwaption();
			this->numerixSwaption = setEngineNumerixHW();
			double npv = numerixSwaption->NPV();
			return npv;
		}
		break;
		default://just to do something
		{
			return 0;
		}
		break;
		}
}


Real MustSwaption::computeDelta(Real shift){ return 0; }
Real MustSwaption::computeGamma(Real shift){ return 0; }
Real MustSwaption::computeVega(){ return swaption->result<Real>("vega"); }
Real MustSwaption::computeTheta(){ return 0; }

boost::shared_ptr<PricingEngine> MustSwaption::setEngineQuantLibLMM(){
	//MustSwaption::setEngineQuantLibLMM Error :Missing EURIB6M Actual/360 fixing for February 10th, 2017
	CurveData cData;
	// Fill out with some sample market data
	cData.sampleMktData(cData);
	
	// Build a curve linked to this market data
	boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));

	// Link the curve to the term structure
	//boost::shared_ptr<IborIndex> index = boost::shared_ptr<IborIndex>(new Euribor6M(forwardingTermStructure));
	QuantLib::Date todaysDate =
		myIndex->fixingCalendar().adjust(QuantLib::Date(6, October, 2014));
	Settings::instance().evaluationDate() = today;

	this->forwardingTermStructure.linkTo(ocurve);

	const Size size = floatingLeg.size();

	boost::shared_ptr<LiborForwardModelProcess> process = boost::shared_ptr<LiborForwardModelProcess>(
			new LiborForwardModelProcess(size, myIndex));

	

	// set-up the model

	const Real a = 0.025;
	const Real b = 0.12;
	const Real c = 0.1;
	const Real d = 0.01;

	
	cout << " MustSwaption::setEngineQuantLibLMM() process->fixingTimes().size() " << process->fixingTimes().size() << endl;
	cout << " MustSwaption::setEngineQuantLibLMM() floatingLeg.size(): " << size << endl;

	boost::shared_ptr<LmVolatilityModel> volaModel = boost::shared_ptr<LmVolatilityModel>(new LmLinearExponentialVolatilityModel(process->fixingTimes(), a, b, c, d));

	


	boost::shared_ptr<LmCorrelationModel> corrModel(
		new LmLinearExponentialCorrelationModel(size, 0.1, 0.1));

	// set-up pricing engine
	/*	process->setCovarParam(boost::shared_ptr<LfmCovarianceParameterization>(
	new LfmCovarianceProxy(volaModel, corrModel)));
	*/
	boost::shared_ptr<LiborForwardModel>
		liborModel(new LiborForwardModel(process, volaModel, corrModel));

	return boost::shared_ptr<PricingEngine> (
		new LfmSwaptionEngine(liborModel,
		myIndex->forwardingTermStructure()));
}
boost::shared_ptr<PricingEngine> MustSwaption::setEngineQuantLibBlackFT(){
	//TO DO
	CurveData cData;
	// Fill out with some sample market data
	cData.sampleMktData(cData);
	boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));
	QuantLib::Date todaysDate =
		myIndex->fixingCalendar().adjust(QuantLib::Date(6, October, 2014));
	Settings::instance().evaluationDate() = today;
	// Link the curve to the term structure
	this->forwardingTermStructure.linkTo(ocurve);

	SwaptionVolatility vol;
	vol.termStructure.linkTo(ocurve);
	volatility = vol.atmVolMatrix;

	return boost::shared_ptr<PricingEngine>(
		new BlackSwaptionEngine(forwardingTermStructure, volatility));

	
}
boost::shared_ptr<PricingEngine> MustSwaption::setEngineQuantLibHullWhite(){
	
	Real a = 0.020275, sigma = 0.0001;
	// For American Swaption

	CurveData cData;
	// Fill out with some sample market data
	cData.sampleMktData(cData);
	boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));

	// Link the curve to the term structure
	this->forwardingTermStructure.linkTo(ocurve);
	const boost::shared_ptr<HullWhite> model(new HullWhite(forwardingTermStructure, a, sigma));
	/*boost::shared_ptr<G2> modelG2(new G2(termStructure));
	return boost::shared_ptr<PricingEngine>(
	new G2SwaptionEngine(modelG2, 6, 1000));*/
	/*return boost::shared_ptr<PricingEngine>(
	new TreeSwaptionEngine(model, 100));*/

	return boost::shared_ptr<PricingEngine>(new FdHullWhiteSwaptionEngine(model, 1000, 1000));
}


boost::shared_ptr<NumeriX::Pro::SwaptionAnalytic> MustSwaption::setEngineNumerixBlackFT() {
	//maturit� c pour l option et 
	//le end date c  est pour le swap
	NumeriX::Pro::Date todayNumerix = NumeriX::Pro::Date(today.serialNumber());
	NumeriX::Pro::Swaption::OptionType optionType = NumeriX::Pro::Swaption::CALL;

	NumeriX::Pro::Date startDate = NumeriX::Pro::Date(MustSwaption::NumerixstartDateObj.dateReference);
	NumeriX::Pro::Date maturityDate = NumeriX::Pro::Date(MustSwaption::NumerixmaturityObj.dateReference);
	NumeriX::Pro::Date endDate = NumeriX::Pro::Date(MustSwaption::NumerixendDateObj.dateReference);

	cout << "---->>>>MustSwaption::setEngineNumerixBlackFT startDate: " << startDate << endl;
	cout << "---->>>>MustSwaption::setEngineNumerixBlackFT maturityDate: " << maturityDate << endl;
	cout << "---->>>>MustSwaption::setEngineNumerixBlackFT endDate: " << endDate << endl;

	Tenor optionTenor = getTenorDurationInDays(todayNumerix, maturityDate);
	if (style != EfficiencyProduct::SwaptionStyle::EURO)
	{
		optionTenor = getTenorDurationInDays(todayNumerix, startDate);
	}
	cout << "---->MustSwaption::setEngineNumerixBlackFT optionTenor: " << optionTenor << endl;

	Tenor swapTenor = getTenorDurationInDays(maturityDate, endDate);
	if (style != EfficiencyProduct::SwaptionStyle::EURO)
	{
		swapTenor = getTenorDurationInDays(startDate, endDate);
	}
	cout << "---->MustSwaption::setEngineNumerixBlackFT swapTenor: " << swapTenor << endl;



	NumerixSwaptionVolatility numerixSwaptionVolatility(nxApp, todayNumerix, currency);
	NumeriX::Pro::SwaptionVolSurface swaptionVolSurface = numerixSwaptionVolatility.getSwaptionVolSurface();
	NumeriX::Pro::Swaption::Volatility sigma = swaptionVolSurface;
	Basis basis = "A365";
	cout << " MustSwaption::setEngineNumerixBlackFT basis: " << basis << endl;


	string numerixFloatingLegFreq = MustSwaption::NumerixfloatingLeg.freqQ;
	Tenor frequency = numerixFloatingLegFreq;
	
	double strike = MustSwaption::numerixFixedRate.rates[0];

	
	NumeriX::Pro::Swaption::OptionalArgsTenors optionalArgsTenors;
	double notional = MustSwaption::NumeriXPrincipalMust.nominal;
	optionalArgsTenors.setNotional(notional);

	cout << " MustSwaption::setEngineNumerixBlackFT currency: " << currency << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT optionType: " << optionType << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT optionTenor: " << optionTenor << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT swapTenor: " << swapTenor << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT strike: " << strike << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT basis: " << basis << endl;
	cout << " MustSwaption::setEngineNumerixBlackFT frequency: " << frequency << endl;

	NumeriX::Pro::Swaption	swaption(nxApp, currency, optionType, optionTenor, swapTenor, strike, sigma, basis, frequency, optionalArgsTenors);

	cout << " MustSwaption::setEngineNumerixBlackFT todayNumerix: " << todayNumerix << endl;
	cout << "----------------------------------------------------------------------------------" << endl;
	CurveDataNumerix curveDataNumerix(nxApp);
	YieldCurve yieldCurve = curveDataNumerix.buildCurve(curveDataNumerix, todayNumerix);

	boost::shared_ptr<NumeriX::Pro::SwaptionAnalytic> result;
	result = boost::shared_ptr<NumeriX::Pro::SwaptionAnalytic>(new NumeriX::Pro::SwaptionAnalytic(nxApp, swaption, yieldCurve, todayNumerix));
 
	return result;
}



boost::shared_ptr<NumerixSwaption> MustSwaption::setEngineNumerixHW() {
	boost::shared_ptr<NumerixSwaption> result;

	string numerixFloatingLegFreq = MustSwaption::NumerixfloatingLeg.freqQ;
	//string numerixFixedLegFreq = MustSwaption::NumerixfixedLeg.freqQ;

	double nominal = MustSwaption::NumeriXPrincipalMust.nominal;


	NumeriX::Pro::Date todayNumerix = NumeriX::Pro::Date(today.serialNumber());

	NumeriX::Pro::Date startDate = NumeriX::Pro::Date(MustSwaption::NumerixstartDateObj.dateReference);

	NumeriX::Pro::Date maturityDate = NumeriX::Pro::Date(MustSwaption::NumerixmaturityObj.dateReference);

	string floatingLegCashFlowBasis = MustSwaption::NumerixfloatingLeg.CashFlowBasis;
	string fixedLegCashFlowBasis = MustSwaption::NumerixfixedLeg.CashFlowBasis;
	string indexCashFlowBasis = "A360";

	double spread = MustSwaption::NumeriXIndexMust.spread;

	EfficiencyProduct::SwaptionStyle style = EfficiencyProduct::stringToSwaptionStyle(styleOptionObj.valeur);
	bool isEuropean = false;
	if (style == EfficiencyProduct::SwaptionStyle::EURO)
	{
		isEuropean = true;
	}

	string numerixIndexFrequency = MustSwaption::NumeriXIndexMust.numerixIndexFrequency;
	if (style == EfficiencyProduct::SwaptionStyle::AMER)
	{
		numerixIndexFrequency = "1D";
	}


	double fixedrate = MustSwaption::numerixFixedRate.rates[0];

	cout << " MustSwaption::setEngineNumerixHW() numerixFloatingLegFreq:" << numerixFloatingLegFreq << endl;
	//cout << " MustSwaption::setEngineNumerixHW() numerixFixedLegFreq:" << numerixFixedLegFreq << endl;
	cout << " MustSwaption::setEngineNumerixHW() nominal:" << nominal << endl;
	cout << " MustSwaption::setEngineNumerixHW() todayNumerix:" << todayNumerix << endl;
	cout << " MustSwaption::setEngineNumerixHW() startDate:" << startDate << endl;
	cout << " MustSwaption::setEngineNumerixHW() maturityDate:" << maturityDate << endl;
	cout << " MustSwaption::setEngineNumerixHW() floatingLegCashFlowBasis:" << floatingLegCashFlowBasis << endl;
	cout << " MustSwaption::setEngineNumerixHW() fixedLegCashFlowBasis:" << fixedLegCashFlowBasis << endl;
	cout << " MustSwaption::setEngineNumerixHW() indexCashFlowBasis:" << indexCashFlowBasis << endl;
	cout << " MustSwaption::setEngineNumerixHW() isEuropean:" << isEuropean << endl;
	cout << " MustSwaption::setEngineNumerixHW() spread:" << spread << endl;
	cout << " MustSwaption::setEngineNumerixHW() numerixIndexFrequency:" << numerixIndexFrequency << endl;
	cout << " MustSwaption::setEngineNumerixHW() rate:" << fixedrate << endl;

	result = boost::shared_ptr<NumerixSwaption>(new NumerixSwaption(nxApp,
		isEuropean, spread, nominal, numerixIndexFrequency,
		numerixFloatingLegFreq, numerixFloatingLegFreq,
		fixedrate, indexCashFlowBasis, floatingLegCashFlowBasis, floatingLegCashFlowBasis,
		todayNumerix, startDate, maturityDate));

	CurveDataNumerix curveDataNumerix(nxApp);
	double meanReversion = 0.020275;
	double hwVolatility = 0.0001;

	cout << " MustSwaption::setEngineNumerixHW() meanReversion:" << meanReversion << endl;
	cout << " MustSwaption::setEngineNumerixHW() hwVolatility:" << hwVolatility << endl;

	YieldCurve yieldCurve = curveDataNumerix.buildCurve(curveDataNumerix, todayNumerix);

	HW1F numerixModel(nxApp, currency, yieldCurve, meanReversion, hwVolatility);


	result->setEngine(numerixModel);
	return result;
}


//
//void MustSwaption::SetPricingEngine(string pricingEngineName, Handle<QuantLib::YieldTermStructure> discountingTermStructure, Handle<QuantLib::YieldTermStructure> forwardingTermStructure)
//{
//
//	RelinkableHandle<YieldTermStructure> termStructure;
//	termStructure.linkTo(termStructureforLMM());
//	boost::shared_ptr<IborIndex> index2(new Euribor6M(termStructure));
//	//index2->fixingCalendar().adjust(Date(6, October, 2014));
//
//
//	const Size size = 2 * Actual360().yearFraction(maturity.dateQ, tenor.dateQ);
//	boost::shared_ptr<LiborForwardModelProcess> process(
//		new LiborForwardModelProcess(size, index2));
//	// set-up the model
//	const Real a = 0.02;
//	const Real b = 0.4;
//	const Real c = 0.12;
//	const Real d = 0.01;
//
//	boost::shared_ptr<LmVolatilityModel> volaModel(
//		new LmLinearExponentialVolatilityModel(process->fixingTimes(), a, b, c, d));
//	boost::shared_ptr<LmCorrelationModel> corrModel(
//		new LmLinearExponentialCorrelationModel(size, 0.1, 0.1));
//
//	boost::shared_ptr<LiborForwardModel>
//		liborModel(new LiborForwardModel(process, volaModel, corrModel));
//
//
//	if (pricingEngineName == "LMM_QuantLib"){
//		boost::shared_ptr<PricingEngine> engine(
//			new LfmSwaptionEngine(liborModel,
//			index2->forwardingTermStructure()));
//		swaptionquantlibEngine = engine;
//	}
//	else{
//		if (pricingEngineName == "SwapLMMEngine"){
//			boost::shared_ptr<PricingEngine> engine(new DiscountingSwapEngine(index2->forwardingTermStructure()));
//			swapquantlibEngine = engine;
//		}
//
//
//		else {
//			SwaptionVolatility vol;
//			vol.termStructure.linkTo(discountingTermStructure.currentLink());
//			Handle<SwaptionVolatilityStructure> volatility = vol.atmVolMatrix;
//			swaptionquantlibEngine = boost::shared_ptr<PricingEngine>(
//				new BlackSwaptionEngine(discountingTermStructure, volatility));
//		}
//	}
//}
//double MustSwaption::Price(Handle<QuantLib::YieldTermStructure> discountingTermStructure, Handle<QuantLib::YieldTermStructure> forwardingTermStructure, int i, string pricingEngineName)
//{
//	RelinkableHandle<YieldTermStructure> termStructure;
//	termStructure.linkTo(termStructureforLMM());
//	//index
//	boost::shared_ptr<IborIndex> myIndex = index.ConstructIndex(termStructure);
//
//
//	//FixedLeg
//	SwapFixedLeg* fixed_Leg = new SwapFixedLeg(fixed_Rate.matrixRate, fixed_LegFlow.basisQ, fixed_LegFlow.freqQ);
//
//	//FloatingLeg
//	SwapFloatingLeg* floating_leg = new SwapFloatingLeg(myIndex, floating_LegFlow.basisQ, floating_LegFlow.freqQ);
//
//	Leg fixedLeg;
//	Leg floatingLeg;
//
//
//	if (index.spreads.size()<1) //condition to check the Sread
//	{
//		fixedLeg = fixed_Leg->constructLeg(maturity.dateQ, tenor.dateQ, principal.nominal);
//		floatingLeg = floating_leg->ConstructLeg(maturity.dateQ, tenor.dateQ, principal.nominal);
//	}
//	else
//	{
//		fixedLeg = fixed_Leg->constructLeg(maturity.dateQ, tenor.dateQ, principal.nominal);
//		floatingLeg = floating_leg->ConstructLeg(maturity.dateQ, tenor.dateQ, principal.nominal, index.matrixSpread);
//	}
//
//
//	//SwapType
//	VanillaSwap::Type swapType = VanillaSwap::Payer;
//	Settlement::Type settlementType = Settlement::Physical;
//	//Swap swap(fixedLeg, floatingLeg);
//	boost::shared_ptr<VanillaSwap> swap = constructVanillaSwap(myIndex);
//
//	if (pricingEngineName != "LMM_QuantLib") swapquantlibEngine = boost::shared_ptr<PricingEngine>(new DiscountingSwapEngine(discountingTermStructure));
//	else {
//		SetPricingEngine("SwapLMMEngine", discountingTermStructure, forwardingTermStructure);
//	}
//
//	swap->setPricingEngine(swapquantlibEngine);
//	Real a = swap->NPV();
//	Date maturityDate = TARGET().advance(maturity.dateQ, -2, Days);
//	boost::shared_ptr<Swaption> swaption(new Swaption(swap,
//		boost::shared_ptr<Exercise>(
//		new EuropeanExercise(maturity.dateQ)),
//		settlementType));
//	swaption->setPricingEngine(swaptionquantlibEngine);
//	Real b = swaption->NPV();
//
//
//	return b;
//}
