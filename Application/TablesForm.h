#pragma once

#include <sstream>
#include <string>
#include <iostream>
#include "Catalogue.h"
#include "Mapping.h"
#include "TablesForm.h"
#include "DetailsForms.h"
#include "GrecsForm.h"
#include "tinyxml.h"
#include "EfficiencyLibProducts.h"
#include <boost/foreach.hpp>
namespace EfficiencyPricerLibApplication {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for TablesForm
	/// </summary>
	public ref class TablesForm : public System::Windows::Forms::Form
	{
	public:
		TablesForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			tableauNPV = new vector<std::tuple<string, string, double, double, double> >;
			tableauGrecs = new vector<std::tuple<string, string, double, double, double, double> >;
			catalogue = new Catalogue();
			catalogueIsChanged = false;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~TablesForm()
		{
			if(components)
			{
				delete components;
			}
		}

		Catalogue* catalogue;
		Mapping* mapping;




	public:
	private:
	protected:



	public:
		bool catalogueIsChanged;
	public:
	private:

	public:
















	private:

	public:

	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


	private:
		vector<std::tuple<string, string, double, double, double> >* tableauNPV;
		vector<std::tuple<string, string, double, double, double, double> >* tableauGrecs;
		EfficiencyLibProducts* EffLibProducts;
		TablesForm ^ formTable;









	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;


























	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel4;
	private: System::Windows::Forms::TextBox^  LoadTextBox;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  Loadbutton;


	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::Panel^  panel4;
	private: System::Windows::Forms::SplitContainer^  splitContainer2;
	public: System::Windows::Forms::DataGridView^  dataGridView1;
	private:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ProductColumn;
	public:
	private: System::Windows::Forms::DataGridViewComboBoxColumn^  TypeColumn;
	private: System::Windows::Forms::DataGridViewCheckBoxColumn^  ActivateColumn;
	private: System::Windows::Forms::DataGridViewComboBoxColumn^  ModelColumn;
	private: System::Windows::Forms::Button^  CancelTypeProdButton;
	private: System::Windows::Forms::Button^  SaveCataloguebutton;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::DataGridView^  dataG;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column5;
	private: System::Windows::Forms::DataGridViewComboBoxColumn^  ObjectQuantLibColumn;
	private: System::Windows::Forms::Panel^  panel2;
	public: System::Windows::Forms::ComboBox^  ProductTypecomboBox;
	private:
	private: System::Windows::Forms::TextBox^  ProductTypeTextBox;
	public:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  Cancelbutton;
	private: System::Windows::Forms::Button^  SaveMappingbutton;
	private: System::Windows::Forms::Button^  Creatbutton;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel5;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel14;
	private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel3;




































private: System::Windows::Forms::DateTimePicker^  dateTimePicker1;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel6;






















private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel7;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel13;
private: System::Windows::Forms::Button^  SaveButton;







private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel9;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel12;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel15;
private: System::Windows::Forms::Button^  Closebutton;
private: System::Windows::Forms::Button^  Pricebutton;
private: System::Windows::Forms::GroupBox^  Valuation;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel10;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel16;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::TextBox^  VegaTextBox;
private: System::Windows::Forms::TextBox^  NPVTextBox;
private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::TextBox^  GammaTextBox;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::TextBox^  DeltaTextBox;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel11;
private: System::Windows::Forms::Button^  NPVDetailButton;
private: System::Windows::Forms::Button^  RiskFactorButton;
private: System::Windows::Forms::GroupBox^  groupBox2;
private: System::Windows::Forms::DataGridView^  dataGridView2;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  ProductNameGrill;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  ProductTypeGrill;
private: System::Windows::Forms::DataGridViewComboBoxColumn^  PricingModelGrill;
private: System::Windows::Forms::GroupBox^  groupBox1;
private: System::Windows::Forms::RichTextBox^  richTextBox1;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel8;






























































private: System::Windows::Forms::Label^  label8;


































































































































#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// Required method for Designer support - do not modify
			 /// the contents of this method with the code editor.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->panel1 = (gcnew System::Windows::Forms::Panel());
				 this->label8 = (gcnew System::Windows::Forms::Label());
				 this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
				 this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
				 this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel4 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->LoadTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->label1 = (gcnew System::Windows::Forms::Label());
				 this->label5 = (gcnew System::Windows::Forms::Label());
				 this->Loadbutton = (gcnew System::Windows::Forms::Button());
				 this->dateTimePicker1 = (gcnew System::Windows::Forms::DateTimePicker());
				 this->tableLayoutPanel3 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel6 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel9 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel12 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel15 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->Closebutton = (gcnew System::Windows::Forms::Button());
				 this->Valuation = (gcnew System::Windows::Forms::GroupBox());
				 this->tableLayoutPanel10 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->tableLayoutPanel16 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->label3 = (gcnew System::Windows::Forms::Label());
				 this->NPVTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->DeltaTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->label4 = (gcnew System::Windows::Forms::Label());
				 this->label7 = (gcnew System::Windows::Forms::Label());
				 this->label6 = (gcnew System::Windows::Forms::Label());
				 this->GammaTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->VegaTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->tableLayoutPanel11 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->NPVDetailButton = (gcnew System::Windows::Forms::Button());
				 this->RiskFactorButton = (gcnew System::Windows::Forms::Button());
				 this->tableLayoutPanel8 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->Pricebutton = (gcnew System::Windows::Forms::Button());
				 this->tableLayoutPanel7 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
				 this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
				 this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
				 this->tableLayoutPanel13 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->SaveButton = (gcnew System::Windows::Forms::Button());
				 this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
				 this->ProductNameGrill = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
				 this->ProductTypeGrill = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
				 this->PricingModelGrill = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
				 this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
				 this->panel4 = (gcnew System::Windows::Forms::Panel());
				 this->splitContainer2 = (gcnew System::Windows::Forms::SplitContainer());
				 this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
				 this->ProductColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
				 this->TypeColumn = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
				 this->ActivateColumn = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
				 this->ModelColumn = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
				 this->CancelTypeProdButton = (gcnew System::Windows::Forms::Button());
				 this->SaveCataloguebutton = (gcnew System::Windows::Forms::Button());
				 this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
				 this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
				 this->panel3 = (gcnew System::Windows::Forms::Panel());
				 this->dataG = (gcnew System::Windows::Forms::DataGridView());
				 this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
				 this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
				 this->ObjectQuantLibColumn = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
				 this->panel2 = (gcnew System::Windows::Forms::Panel());
				 this->ProductTypecomboBox = (gcnew System::Windows::Forms::ComboBox());
				 this->ProductTypeTextBox = (gcnew System::Windows::Forms::TextBox());
				 this->label2 = (gcnew System::Windows::Forms::Label());
				 this->Cancelbutton = (gcnew System::Windows::Forms::Button());
				 this->SaveMappingbutton = (gcnew System::Windows::Forms::Button());
				 this->Creatbutton = (gcnew System::Windows::Forms::Button());
				 this->tableLayoutPanel5 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->textBox1 = (gcnew System::Windows::Forms::TextBox());
				 this->tableLayoutPanel14 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->textBox2 = (gcnew System::Windows::Forms::TextBox());
				 this->panel1->SuspendLayout();
				 this->tableLayoutPanel1->SuspendLayout();
				 this->tabControl1->SuspendLayout();
				 this->tabPage3->SuspendLayout();
				 this->tableLayoutPanel2->SuspendLayout();
				 this->tableLayoutPanel4->SuspendLayout();
				 this->tableLayoutPanel3->SuspendLayout();
				 this->tableLayoutPanel6->SuspendLayout();
				 this->tableLayoutPanel9->SuspendLayout();
				 this->tableLayoutPanel12->SuspendLayout();
				 this->tableLayoutPanel15->SuspendLayout();
				 this->Valuation->SuspendLayout();
				 this->tableLayoutPanel10->SuspendLayout();
				 this->tableLayoutPanel16->SuspendLayout();
				 this->tableLayoutPanel11->SuspendLayout();
				 this->tableLayoutPanel8->SuspendLayout();
				 this->tableLayoutPanel7->SuspendLayout();
				 this->groupBox1->SuspendLayout();
				 this->groupBox2->SuspendLayout();
				 this->tableLayoutPanel13->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
				 this->tabPage1->SuspendLayout();
				 this->panel4->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->BeginInit();
				 this->splitContainer2->Panel1->SuspendLayout();
				 this->splitContainer2->Panel2->SuspendLayout();
				 this->splitContainer2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
				 this->tabPage2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
				 this->splitContainer1->Panel1->SuspendLayout();
				 this->splitContainer1->Panel2->SuspendLayout();
				 this->splitContainer1->SuspendLayout();
				 this->panel3->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataG))->BeginInit();
				 this->panel2->SuspendLayout();
				 this->tableLayoutPanel5->SuspendLayout();
				 this->tableLayoutPanel14->SuspendLayout();
				 this->SuspendLayout();
				 // 
				 // panel1
				 // 
				 this->panel1->BackColor = System::Drawing::SystemColors::ButtonFace;
				 this->panel1->Controls->Add(this->label8);
				 this->panel1->Dock = System::Windows::Forms::DockStyle::Bottom;
				 this->panel1->Location = System::Drawing::Point(0, 452);
				 this->panel1->Name = L"panel1";
				 this->panel1->Size = System::Drawing::Size(798, 15);
				 this->panel1->TabIndex = 31;
				 this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::panel1_Paint);
				 // 
				 // label8
				 // 
				 this->label8->AutoSize = true;
				 this->label8->BackColor = System::Drawing::SystemColors::ButtonFace;
				 this->label8->Dock = System::Windows::Forms::DockStyle::Right;
				 this->label8->Font = (gcnew System::Drawing::Font(L"Cambria", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label8->Location = System::Drawing::Point(440, 0);
				 this->label8->Name = L"label8";
				 this->label8->Size = System::Drawing::Size(358, 14);
				 this->label8->TabIndex = 31;
				 this->label8->Text = L"Fast Must Integration Efficiency Management Consulting � 2017";
				 // 
				 // tableLayoutPanel1
				 // 
				 this->tableLayoutPanel1->BackColor = System::Drawing::Color::White;
				 this->tableLayoutPanel1->ColumnCount = 1;
				 this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel1->Controls->Add(this->tabControl1, 0, 1);
				 this->tableLayoutPanel1->Controls->Add(this->tableLayoutPanel5, 0, 0);
				 this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
				 this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
				 this->tableLayoutPanel1->RowCount = 2;
				 this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 17.25664F)));
				 this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 82.74336F)));
				 this->tableLayoutPanel1->Size = System::Drawing::Size(798, 452);
				 this->tableLayoutPanel1->TabIndex = 32;
				 this->tableLayoutPanel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::tableLayoutPanel1_Paint_1);
				 // 
				 // tabControl1
				 // 
				 this->tabControl1->Controls->Add(this->tabPage3);
				 this->tabControl1->Controls->Add(this->tabPage1);
				 this->tabControl1->Controls->Add(this->tabPage2);
				 this->tabControl1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tabControl1->Location = System::Drawing::Point(3, 81);
				 this->tabControl1->Name = L"tabControl1";
				 this->tabControl1->RightToLeft = System::Windows::Forms::RightToLeft::No;
				 this->tabControl1->SelectedIndex = 0;
				 this->tabControl1->Size = System::Drawing::Size(792, 368);
				 this->tabControl1->TabIndex = 30;
				 // 
				 // tabPage3
				 // 
				 this->tabPage3->Controls->Add(this->tableLayoutPanel2);
				 this->tabPage3->Location = System::Drawing::Point(4, 22);
				 this->tabPage3->Name = L"tabPage3";
				 this->tabPage3->Padding = System::Windows::Forms::Padding(3);
				 this->tabPage3->Size = System::Drawing::Size(784, 342);
				 this->tabPage3->TabIndex = 2;
				 this->tabPage3->Text = L"Pricing";
				 this->tabPage3->UseVisualStyleBackColor = true;
				 // 
				 // tableLayoutPanel2
				 // 
				 this->tableLayoutPanel2->CellBorderStyle = System::Windows::Forms::TableLayoutPanelCellBorderStyle::OutsetDouble;
				 this->tableLayoutPanel2->ColumnCount = 1;
				 this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel2->Controls->Add(this->tableLayoutPanel4, 0, 0);
				 this->tableLayoutPanel2->Controls->Add(this->tableLayoutPanel3, 0, 1);
				 this->tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel2->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
				 this->tableLayoutPanel2->RowCount = 2;
				 this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 20.12012F)));
				 this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 79.87988F)));
				 this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
				 this->tableLayoutPanel2->Size = System::Drawing::Size(778, 336);
				 this->tableLayoutPanel2->TabIndex = 0;
				 // 
				 // tableLayoutPanel4
				 // 
				 this->tableLayoutPanel4->BackColor = System::Drawing::Color::White;
				 this->tableLayoutPanel4->CellBorderStyle = System::Windows::Forms::TableLayoutPanelCellBorderStyle::Single;
				 this->tableLayoutPanel4->ColumnCount = 3;
				 this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 7.216495F)));
				 this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 92.78351F)));
				 this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 158)));
				 this->tableLayoutPanel4->Controls->Add(this->LoadTextBox, 1, 0);
				 this->tableLayoutPanel4->Controls->Add(this->label1, 0, 0);
				 this->tableLayoutPanel4->Controls->Add(this->label5, 0, 1);
				 this->tableLayoutPanel4->Controls->Add(this->Loadbutton, 2, 0);
				 this->tableLayoutPanel4->Controls->Add(this->dateTimePicker1, 1, 1);
				 this->tableLayoutPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel4->Location = System::Drawing::Point(6, 6);
				 this->tableLayoutPanel4->Name = L"tableLayoutPanel4";
				 this->tableLayoutPanel4->RowCount = 2;
				 this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 53.96825F)));
				 this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 46.03175F)));
				 this->tableLayoutPanel4->Size = System::Drawing::Size(766, 59);
				 this->tableLayoutPanel4->TabIndex = 0;
				 // 
				 // LoadTextBox
				 // 
				 this->LoadTextBox->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->LoadTextBox->Enabled = false;
				 this->LoadTextBox->ForeColor = System::Drawing::SystemColors::InactiveCaption;
				 this->LoadTextBox->Location = System::Drawing::Point(48, 6);
				 this->LoadTextBox->Name = L"LoadTextBox";
				 this->LoadTextBox->Size = System::Drawing::Size(554, 20);
				 this->LoadTextBox->TabIndex = 60;
				 this->LoadTextBox->Text = L"Load a XML Must Product";
				 // 
				 // label1
				 // 
				 this->label1->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label1->AutoSize = true;
				 this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label1->Location = System::Drawing::Point(4, 9);
				 this->label1->Name = L"label1";
				 this->label1->Size = System::Drawing::Size(32, 13);
				 this->label1->TabIndex = 56;
				 this->label1->Text = L"Path:";
				 // 
				 // label5
				 // 
				 this->label5->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label5->AutoSize = true;
				 this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label5->Location = System::Drawing::Point(4, 38);
				 this->label5->Name = L"label5";
				 this->label5->Size = System::Drawing::Size(36, 13);
				 this->label5->TabIndex = 59;
				 this->label5->Text = L"Date :";
				 // 
				 // Loadbutton
				 // 
				 this->Loadbutton->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->Loadbutton->Location = System::Drawing::Point(609, 4);
				 this->Loadbutton->Name = L"Loadbutton";
				 this->Loadbutton->Size = System::Drawing::Size(65, 24);
				 this->Loadbutton->TabIndex = 62;
				 this->Loadbutton->Text = L"Load";
				 this->Loadbutton->UseVisualStyleBackColor = true;
				 this->Loadbutton->Click += gcnew System::EventHandler(this, &TablesForm::Loadbutton_Click_1);
				 // 
				 // dateTimePicker1
				 // 
				 this->dateTimePicker1->Location = System::Drawing::Point(48, 35);
				 this->dateTimePicker1->Name = L"dateTimePicker1";
				 this->dateTimePicker1->Size = System::Drawing::Size(155, 20);
				 this->dateTimePicker1->TabIndex = 63;
				 // 
				 // tableLayoutPanel3
				 // 
				 this->tableLayoutPanel3->ColumnCount = 1;
				 this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel3->Controls->Add(this->tableLayoutPanel6, 0, 0);
				 this->tableLayoutPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel3->Location = System::Drawing::Point(6, 74);
				 this->tableLayoutPanel3->Name = L"tableLayoutPanel3";
				 this->tableLayoutPanel3->RowCount = 1;
				 this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 46.07143F)));
				 this->tableLayoutPanel3->Size = System::Drawing::Size(766, 256);
				 this->tableLayoutPanel3->TabIndex = 1;
				 this->tableLayoutPanel3->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::tableLayoutPanel3_Paint_2);
				 // 
				 // tableLayoutPanel6
				 // 
				 this->tableLayoutPanel6->ColumnCount = 1;
				 this->tableLayoutPanel6->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel6->Controls->Add(this->tableLayoutPanel9, 0, 1);
				 this->tableLayoutPanel6->Controls->Add(this->tableLayoutPanel7, 0, 0);
				 this->tableLayoutPanel6->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel6->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel6->Name = L"tableLayoutPanel6";
				 this->tableLayoutPanel6->RowCount = 2;
				 this->tableLayoutPanel6->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel6->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 103)));
				 this->tableLayoutPanel6->Size = System::Drawing::Size(760, 250);
				 this->tableLayoutPanel6->TabIndex = 84;
				 this->tableLayoutPanel6->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::tableLayoutPanel6_Paint);
				 // 
				 // tableLayoutPanel9
				 // 
				 this->tableLayoutPanel9->BackColor = System::Drawing::Color::White;
				 this->tableLayoutPanel9->ColumnCount = 3;
				 this->tableLayoutPanel9->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 20.98569F)));
				 this->tableLayoutPanel9->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 79.01431F)));
				 this->tableLayoutPanel9->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 134)));
				 this->tableLayoutPanel9->Controls->Add(this->tableLayoutPanel12, 2, 0);
				 this->tableLayoutPanel9->Controls->Add(this->Valuation, 1, 0);
				 this->tableLayoutPanel9->Controls->Add(this->tableLayoutPanel8, 0, 0);
				 this->tableLayoutPanel9->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel9->Location = System::Drawing::Point(3, 150);
				 this->tableLayoutPanel9->Name = L"tableLayoutPanel9";
				 this->tableLayoutPanel9->RowCount = 1;
				 this->tableLayoutPanel9->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 49.13295F)));
				 this->tableLayoutPanel9->Size = System::Drawing::Size(754, 97);
				 this->tableLayoutPanel9->TabIndex = 84;
				 // 
				 // tableLayoutPanel12
				 // 
				 this->tableLayoutPanel12->ColumnCount = 1;
				 this->tableLayoutPanel12->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel12->Controls->Add(this->tableLayoutPanel15, 0, 1);
				 this->tableLayoutPanel12->Location = System::Drawing::Point(622, 3);
				 this->tableLayoutPanel12->Name = L"tableLayoutPanel12";
				 this->tableLayoutPanel12->RowCount = 2;
				 this->tableLayoutPanel12->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel12->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
					 55)));
				 this->tableLayoutPanel12->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
					 20)));
				 this->tableLayoutPanel12->Size = System::Drawing::Size(109, 91);
				 this->tableLayoutPanel12->TabIndex = 77;
				 // 
				 // tableLayoutPanel15
				 // 
				 this->tableLayoutPanel15->ColumnCount = 2;
				 this->tableLayoutPanel15->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel15->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 99)));
				 this->tableLayoutPanel15->Controls->Add(this->Closebutton, 1, 0);
				 this->tableLayoutPanel15->Location = System::Drawing::Point(3, 39);
				 this->tableLayoutPanel15->Name = L"tableLayoutPanel15";
				 this->tableLayoutPanel15->RowCount = 1;
				 this->tableLayoutPanel15->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel15->Size = System::Drawing::Size(103, 49);
				 this->tableLayoutPanel15->TabIndex = 75;
				 // 
				 // Closebutton
				 // 
				 this->Closebutton->Location = System::Drawing::Point(7, 3);
				 this->Closebutton->Name = L"Closebutton";
				 this->Closebutton->Size = System::Drawing::Size(93, 37);
				 this->Closebutton->TabIndex = 76;
				 this->Closebutton->Text = L"Close";
				 this->Closebutton->UseVisualStyleBackColor = true;
				 this->Closebutton->Click += gcnew System::EventHandler(this, &TablesForm::Closebutton_Click_1);
				 // 
				 // Valuation
				 // 
				 this->Valuation->Controls->Add(this->tableLayoutPanel10);
				 this->Valuation->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->Valuation->Location = System::Drawing::Point(133, 3);
				 this->Valuation->Name = L"Valuation";
				 this->Valuation->Size = System::Drawing::Size(483, 91);
				 this->Valuation->TabIndex = 78;
				 this->Valuation->TabStop = false;
				 this->Valuation->Text = L"Valuation";
				 // 
				 // tableLayoutPanel10
				 // 
				 this->tableLayoutPanel10->ColumnCount = 2;
				 this->tableLayoutPanel10->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 85.58559F)));
				 this->tableLayoutPanel10->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 149)));
				 this->tableLayoutPanel10->Controls->Add(this->tableLayoutPanel16, 0, 0);
				 this->tableLayoutPanel10->Controls->Add(this->tableLayoutPanel11, 1, 0);
				 this->tableLayoutPanel10->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel10->Location = System::Drawing::Point(3, 16);
				 this->tableLayoutPanel10->Name = L"tableLayoutPanel10";
				 this->tableLayoutPanel10->RowCount = 1;
				 this->tableLayoutPanel10->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel10->Size = System::Drawing::Size(477, 72);
				 this->tableLayoutPanel10->TabIndex = 80;
				 // 
				 // tableLayoutPanel16
				 // 
				 this->tableLayoutPanel16->ColumnCount = 4;
				 this->tableLayoutPanel16->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 25.45455F)));
				 this->tableLayoutPanel16->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 74.54546F)));
				 this->tableLayoutPanel16->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 49)));
				 this->tableLayoutPanel16->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 139)));
				 this->tableLayoutPanel16->Controls->Add(this->label3, 0, 0);
				 this->tableLayoutPanel16->Controls->Add(this->NPVTextBox, 1, 0);
				 this->tableLayoutPanel16->Controls->Add(this->DeltaTextBox, 1, 1);
				 this->tableLayoutPanel16->Controls->Add(this->label4, 0, 1);
				 this->tableLayoutPanel16->Controls->Add(this->label7, 2, 0);
				 this->tableLayoutPanel16->Controls->Add(this->label6, 2, 1);
				 this->tableLayoutPanel16->Controls->Add(this->GammaTextBox, 3, 0);
				 this->tableLayoutPanel16->Controls->Add(this->VegaTextBox, 3, 1);
				 this->tableLayoutPanel16->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel16->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel16->Name = L"tableLayoutPanel16";
				 this->tableLayoutPanel16->RowCount = 2;
				 this->tableLayoutPanel16->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 51.06383F)));
				 this->tableLayoutPanel16->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 48.93617F)));
				 this->tableLayoutPanel16->Size = System::Drawing::Size(322, 66);
				 this->tableLayoutPanel16->TabIndex = 86;
				 this->tableLayoutPanel16->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::tableLayoutPanel16_Paint);
				 // 
				 // label3
				 // 
				 this->label3->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label3->AutoSize = true;
				 this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label3->Location = System::Drawing::Point(3, 3);
				 this->label3->Name = L"label3";
				 this->label3->Size = System::Drawing::Size(22, 26);
				 this->label3->TabIndex = 57;
				 this->label3->Text = L"NPV";
				 // 
				 // NPVTextBox
				 // 
				 this->NPVTextBox->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->NPVTextBox->BackColor = System::Drawing::SystemColors::InactiveBorder;
				 this->NPVTextBox->ForeColor = System::Drawing::Color::LimeGreen;
				 this->NPVTextBox->Location = System::Drawing::Point(37, 6);
				 this->NPVTextBox->Name = L"NPVTextBox";
				 this->NPVTextBox->ReadOnly = true;
				 this->NPVTextBox->Size = System::Drawing::Size(93, 20);
				 this->NPVTextBox->TabIndex = 62;
				 // 
				 // DeltaTextBox
				 // 
				 this->DeltaTextBox->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->DeltaTextBox->BackColor = System::Drawing::SystemColors::InactiveBorder;
				 this->DeltaTextBox->Location = System::Drawing::Point(37, 39);
				 this->DeltaTextBox->Name = L"DeltaTextBox";
				 this->DeltaTextBox->ReadOnly = true;
				 this->DeltaTextBox->Size = System::Drawing::Size(93, 20);
				 this->DeltaTextBox->TabIndex = 63;
				 // 
				 // label4
				 // 
				 this->label4->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label4->AutoSize = true;
				 this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label4->Location = System::Drawing::Point(3, 36);
				 this->label4->Name = L"label4";
				 this->label4->Size = System::Drawing::Size(26, 26);
				 this->label4->TabIndex = 58;
				 this->label4->Text = L"Delta";
				 // 
				 // label7
				 // 
				 this->label7->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label7->AutoSize = true;
				 this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label7->Location = System::Drawing::Point(136, 10);
				 this->label7->Name = L"label7";
				 this->label7->Size = System::Drawing::Size(43, 13);
				 this->label7->TabIndex = 60;
				 this->label7->Text = L"Gamma";
				 // 
				 // label6
				 // 
				 this->label6->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->label6->AutoSize = true;
				 this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label6->Location = System::Drawing::Point(136, 43);
				 this->label6->Name = L"label6";
				 this->label6->Size = System::Drawing::Size(32, 13);
				 this->label6->TabIndex = 59;
				 this->label6->Text = L"Vega";
				 // 
				 // GammaTextBox
				 // 
				 this->GammaTextBox->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->GammaTextBox->BackColor = System::Drawing::SystemColors::InactiveBorder;
				 this->GammaTextBox->Location = System::Drawing::Point(185, 6);
				 this->GammaTextBox->Name = L"GammaTextBox";
				 this->GammaTextBox->ReadOnly = true;
				 this->GammaTextBox->Size = System::Drawing::Size(113, 20);
				 this->GammaTextBox->TabIndex = 64;
				 // 
				 // VegaTextBox
				 // 
				 this->VegaTextBox->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->VegaTextBox->BackColor = System::Drawing::SystemColors::InactiveBorder;
				 this->VegaTextBox->Location = System::Drawing::Point(185, 39);
				 this->VegaTextBox->Name = L"VegaTextBox";
				 this->VegaTextBox->ReadOnly = true;
				 this->VegaTextBox->Size = System::Drawing::Size(113, 20);
				 this->VegaTextBox->TabIndex = 65;
				 // 
				 // tableLayoutPanel11
				 // 
				 this->tableLayoutPanel11->ColumnCount = 1;
				 this->tableLayoutPanel11->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 23.38309F)));
				 this->tableLayoutPanel11->Controls->Add(this->NPVDetailButton, 0, 0);
				 this->tableLayoutPanel11->Controls->Add(this->RiskFactorButton, 0, 1);
				 this->tableLayoutPanel11->Location = System::Drawing::Point(331, 3);
				 this->tableLayoutPanel11->Name = L"tableLayoutPanel11";
				 this->tableLayoutPanel11->RowCount = 2;
				 this->tableLayoutPanel11->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel11->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
					 33)));
				 this->tableLayoutPanel11->Size = System::Drawing::Size(143, 66);
				 this->tableLayoutPanel11->TabIndex = 85;
				 // 
				 // NPVDetailButton
				 // 
				 this->NPVDetailButton->BackColor = System::Drawing::Color::WhiteSmoke;
				 this->NPVDetailButton->Cursor = System::Windows::Forms::Cursors::Default;
				 this->NPVDetailButton->Enabled = false;
				 this->NPVDetailButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold));
				 this->NPVDetailButton->Location = System::Drawing::Point(3, 3);
				 this->NPVDetailButton->Name = L"NPVDetailButton";
				 this->NPVDetailButton->Size = System::Drawing::Size(137, 27);
				 this->NPVDetailButton->TabIndex = 71;
				 this->NPVDetailButton->Text = L" NPV Details ";
				 this->NPVDetailButton->UseVisualStyleBackColor = true;
				 this->NPVDetailButton->Click += gcnew System::EventHandler(this, &TablesForm::NPVDetailButton_Click_2);
				 // 
				 // RiskFactorButton
				 // 
				 this->RiskFactorButton->BackColor = System::Drawing::Color::WhiteSmoke;
				 this->RiskFactorButton->Enabled = false;
				 this->RiskFactorButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->RiskFactorButton->Location = System::Drawing::Point(3, 36);
				 this->RiskFactorButton->Name = L"RiskFactorButton";
				 this->RiskFactorButton->Size = System::Drawing::Size(137, 27);
				 this->RiskFactorButton->TabIndex = 72;
				 this->RiskFactorButton->Text = L"Risk Factors";
				 this->RiskFactorButton->UseVisualStyleBackColor = true;
				 this->RiskFactorButton->Click += gcnew System::EventHandler(this, &TablesForm::RiskFactorButton_Click_1);
				 // 
				 // tableLayoutPanel8
				 // 
				 this->tableLayoutPanel8->ColumnCount = 1;
				 this->tableLayoutPanel8->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel8->Controls->Add(this->Pricebutton, 0, 1);
				 this->tableLayoutPanel8->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel8->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel8->Name = L"tableLayoutPanel8";
				 this->tableLayoutPanel8->RowCount = 3;
				 this->tableLayoutPanel8->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 36.25F)));
				 this->tableLayoutPanel8->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 63.75F)));
				 this->tableLayoutPanel8->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 10)));
				 this->tableLayoutPanel8->Size = System::Drawing::Size(124, 91);
				 this->tableLayoutPanel8->TabIndex = 79;
				 // 
				 // Pricebutton
				 // 
				 this->Pricebutton->AutoEllipsis = true;
				 this->Pricebutton->BackColor = System::Drawing::Color::WhiteSmoke;
				 this->Pricebutton->Enabled = false;
				 this->Pricebutton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->Pricebutton->ForeColor = System::Drawing::Color::YellowGreen;
				 this->Pricebutton->Location = System::Drawing::Point(3, 32);
				 this->Pricebutton->MaximumSize = System::Drawing::Size(119, 42);
				 this->Pricebutton->MinimumSize = System::Drawing::Size(119, 42);
				 this->Pricebutton->Name = L"Pricebutton";
				 this->Pricebutton->Size = System::Drawing::Size(119, 42);
				 this->Pricebutton->TabIndex = 63;
				 this->Pricebutton->Text = L"Price";
				 this->Pricebutton->UseVisualStyleBackColor = false;
				 this->Pricebutton->Click += gcnew System::EventHandler(this, &TablesForm::Pricebutton_Click);
				 // 
				 // tableLayoutPanel7
				 // 
				 this->tableLayoutPanel7->CellBorderStyle = System::Windows::Forms::TableLayoutPanelCellBorderStyle::Outset;
				 this->tableLayoutPanel7->ColumnCount = 2;
				 this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 55.45213F)));
				 this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 44.54787F)));
				 this->tableLayoutPanel7->Controls->Add(this->groupBox1, 1, 0);
				 this->tableLayoutPanel7->Controls->Add(this->groupBox2, 0, 0);
				 this->tableLayoutPanel7->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel7->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel7->Name = L"tableLayoutPanel7";
				 this->tableLayoutPanel7->RowCount = 1;
				 this->tableLayoutPanel7->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel7->Size = System::Drawing::Size(754, 141);
				 this->tableLayoutPanel7->TabIndex = 83;
				 // 
				 // groupBox1
				 // 
				 this->groupBox1->Controls->Add(this->richTextBox1);
				 this->groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->groupBox1->Location = System::Drawing::Point(421, 5);
				 this->groupBox1->Name = L"groupBox1";
				 this->groupBox1->Size = System::Drawing::Size(328, 131);
				 this->groupBox1->TabIndex = 91;
				 this->groupBox1->TabStop = false;
				 this->groupBox1->Text = L"Flows";
				 // 
				 // richTextBox1
				 // 
				 this->richTextBox1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->richTextBox1->Location = System::Drawing::Point(3, 16);
				 this->richTextBox1->Name = L"richTextBox1";
				 this->richTextBox1->Size = System::Drawing::Size(322, 112);
				 this->richTextBox1->TabIndex = 91;
				 this->richTextBox1->Text = L"";
				 // 
				 // groupBox2
				 // 
				 this->groupBox2->Controls->Add(this->tableLayoutPanel13);
				 this->groupBox2->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->groupBox2->Location = System::Drawing::Point(5, 5);
				 this->groupBox2->Name = L"groupBox2";
				 this->groupBox2->Size = System::Drawing::Size(408, 131);
				 this->groupBox2->TabIndex = 90;
				 this->groupBox2->TabStop = false;
				 this->groupBox2->Text = L"Fast Mapper";
				 // 
				 // tableLayoutPanel13
				 // 
				 this->tableLayoutPanel13->ColumnCount = 2;
				 this->tableLayoutPanel13->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel13->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 48)));
				 this->tableLayoutPanel13->Controls->Add(this->SaveButton, 1, 0);
				 this->tableLayoutPanel13->Controls->Add(this->dataGridView2, 0, 0);
				 this->tableLayoutPanel13->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel13->Location = System::Drawing::Point(3, 16);
				 this->tableLayoutPanel13->Name = L"tableLayoutPanel13";
				 this->tableLayoutPanel13->RowCount = 1;
				 this->tableLayoutPanel13->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 58.47458F)));
				 this->tableLayoutPanel13->Size = System::Drawing::Size(402, 112);
				 this->tableLayoutPanel13->TabIndex = 88;
				 // 
				 // SaveButton
				 // 
				 this->SaveButton->Enabled = false;
				 this->SaveButton->Location = System::Drawing::Point(357, 3);
				 this->SaveButton->Name = L"SaveButton";
				 this->SaveButton->Size = System::Drawing::Size(42, 43);
				 this->SaveButton->TabIndex = 76;
				 this->SaveButton->Text = L"Save";
				 this->SaveButton->UseVisualStyleBackColor = true;
				 this->SaveButton->Click += gcnew System::EventHandler(this, &TablesForm::button1_Click_1);
				 // 
				 // dataGridView2
				 // 
				 this->dataGridView2->AllowUserToResizeRows = false;
				 this->dataGridView2->BackgroundColor = System::Drawing::Color::White;
				 this->dataGridView2->CellBorderStyle = System::Windows::Forms::DataGridViewCellBorderStyle::None;
				 this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
				 this->dataGridView2->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
					 this->ProductNameGrill,
						 this->ProductTypeGrill, this->PricingModelGrill
				 });
				 this->dataGridView2->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->dataGridView2->Location = System::Drawing::Point(3, 3);
				 this->dataGridView2->Name = L"dataGridView2";
				 this->dataGridView2->Size = System::Drawing::Size(348, 106);
				 this->dataGridView2->TabIndex = 88;
				 this->dataGridView2->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &TablesForm::dataGridView2_CellContentClick_1);
				 // 
				 // ProductNameGrill
				 // 
				 this->ProductNameGrill->Frozen = true;
				 this->ProductNameGrill->HeaderText = L"Product Name";
				 this->ProductNameGrill->Name = L"ProductNameGrill";
				 // 
				 // ProductTypeGrill
				 // 
				 this->ProductTypeGrill->Frozen = true;
				 this->ProductTypeGrill->HeaderText = L"Product Type";
				 this->ProductTypeGrill->Name = L"ProductTypeGrill";
				 this->ProductTypeGrill->Resizable = System::Windows::Forms::DataGridViewTriState::True;
				 this->ProductTypeGrill->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
				 // 
				 // PricingModelGrill
				 // 
				 this->PricingModelGrill->HeaderText = L"Pricing Model";
				 this->PricingModelGrill->Name = L"PricingModelGrill";
				 // 
				 // tabPage1
				 // 
				 this->tabPage1->BackColor = System::Drawing::Color::GhostWhite;
				 this->tabPage1->Controls->Add(this->panel4);
				 this->tabPage1->Location = System::Drawing::Point(4, 22);
				 this->tabPage1->Name = L"tabPage1";
				 this->tabPage1->Padding = System::Windows::Forms::Padding(3);
				 this->tabPage1->Size = System::Drawing::Size(784, 342);
				 this->tabPage1->TabIndex = 0;
				 this->tabPage1->Text = L"Product Type /Pricing Model";
				 // 
				 // panel4
				 // 
				 this->panel4->Controls->Add(this->splitContainer2);
				 this->panel4->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->panel4->Location = System::Drawing::Point(3, 3);
				 this->panel4->Name = L"panel4";
				 this->panel4->Size = System::Drawing::Size(778, 336);
				 this->panel4->TabIndex = 4;
				 // 
				 // splitContainer2
				 // 
				 this->splitContainer2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
					 | System::Windows::Forms::AnchorStyles::Left)
					 | System::Windows::Forms::AnchorStyles::Right));
				 this->splitContainer2->Location = System::Drawing::Point(3, 18);
				 this->splitContainer2->Name = L"splitContainer2";
				 this->splitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
				 // 
				 // splitContainer2.Panel1
				 // 
				 this->splitContainer2->Panel1->Controls->Add(this->dataGridView1);
				 this->splitContainer2->Panel1->RightToLeft = System::Windows::Forms::RightToLeft::No;
				 // 
				 // splitContainer2.Panel2
				 // 
				 this->splitContainer2->Panel2->Controls->Add(this->CancelTypeProdButton);
				 this->splitContainer2->Panel2->Controls->Add(this->SaveCataloguebutton);
				 this->splitContainer2->Panel2->RightToLeft = System::Windows::Forms::RightToLeft::No;
				 this->splitContainer2->Size = System::Drawing::Size(772, 318);
				 this->splitContainer2->SplitterDistance = 264;
				 this->splitContainer2->TabIndex = 3;
				 // 
				 // dataGridView1
				 // 
				 this->dataGridView1->BackgroundColor = System::Drawing::Color::White;
				 this->dataGridView1->BorderStyle = System::Windows::Forms::BorderStyle::None;
				 this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
				 this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
					 this->ProductColumn,
						 this->TypeColumn, this->ActivateColumn, this->ModelColumn
				 });
				 this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->dataGridView1->Location = System::Drawing::Point(0, 0);
				 this->dataGridView1->Name = L"dataGridView1";
				 this->dataGridView1->Size = System::Drawing::Size(772, 264);
				 this->dataGridView1->TabIndex = 0;
				 // 
				 // ProductColumn
				 // 
				 this->ProductColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->ProductColumn->HeaderText = L"Product";
				 this->ProductColumn->Name = L"ProductColumn";
				 // 
				 // TypeColumn
				 // 
				 this->TypeColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->TypeColumn->HeaderText = L"Type";
				 this->TypeColumn->Name = L"TypeColumn";
				 this->TypeColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
				 this->TypeColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
				 // 
				 // ActivateColumn
				 // 
				 this->ActivateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->ActivateColumn->HeaderText = L"Activated";
				 this->ActivateColumn->Name = L"ActivateColumn";
				 this->ActivateColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
				 this->ActivateColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
				 // 
				 // ModelColumn
				 // 
				 this->ModelColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->ModelColumn->HeaderText = L"Pricing Model";
				 this->ModelColumn->Name = L"ModelColumn";
				 this->ModelColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
				 this->ModelColumn->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
				 // 
				 // CancelTypeProdButton
				 // 
				 this->CancelTypeProdButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
				 this->CancelTypeProdButton->Location = System::Drawing::Point(652, 9);
				 this->CancelTypeProdButton->Name = L"CancelTypeProdButton";
				 this->CancelTypeProdButton->Size = System::Drawing::Size(117, 37);
				 this->CancelTypeProdButton->TabIndex = 2;
				 this->CancelTypeProdButton->Text = L"Cancel";
				 this->CancelTypeProdButton->UseVisualStyleBackColor = true;
				 this->CancelTypeProdButton->Click += gcnew System::EventHandler(this, &TablesForm::CancelTypeProdButton_Click_1);
				 // 
				 // SaveCataloguebutton
				 // 
				 this->SaveCataloguebutton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
				 this->SaveCataloguebutton->Location = System::Drawing::Point(3, 10);
				 this->SaveCataloguebutton->Name = L"SaveCataloguebutton";
				 this->SaveCataloguebutton->Size = System::Drawing::Size(117, 37);
				 this->SaveCataloguebutton->TabIndex = 1;
				 this->SaveCataloguebutton->Text = L"Save";
				 this->SaveCataloguebutton->UseVisualStyleBackColor = true;
				 this->SaveCataloguebutton->Click += gcnew System::EventHandler(this, &TablesForm::SaveCataloguebutton_Click_1);
				 // 
				 // tabPage2
				 // 
				 this->tabPage2->BackColor = System::Drawing::Color::GhostWhite;
				 this->tabPage2->Controls->Add(this->splitContainer1);
				 this->tabPage2->Location = System::Drawing::Point(4, 22);
				 this->tabPage2->Name = L"tabPage2";
				 this->tabPage2->Padding = System::Windows::Forms::Padding(3);
				 this->tabPage2->Size = System::Drawing::Size(784, 342);
				 this->tabPage2->TabIndex = 1;
				 this->tabPage2->Text = L"Mapping";
				 // 
				 // splitContainer1
				 // 
				 this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->splitContainer1->Location = System::Drawing::Point(3, 3);
				 this->splitContainer1->Name = L"splitContainer1";
				 this->splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
				 // 
				 // splitContainer1.Panel1
				 // 
				 this->splitContainer1->Panel1->Controls->Add(this->panel3);
				 this->splitContainer1->Panel1->Controls->Add(this->panel2);
				 this->splitContainer1->Panel1->RightToLeft = System::Windows::Forms::RightToLeft::No;
				 // 
				 // splitContainer1.Panel2
				 // 
				 this->splitContainer1->Panel2->Controls->Add(this->Cancelbutton);
				 this->splitContainer1->Panel2->Controls->Add(this->SaveMappingbutton);
				 this->splitContainer1->Panel2->Controls->Add(this->Creatbutton);
				 this->splitContainer1->Panel2->RightToLeft = System::Windows::Forms::RightToLeft::No;
				 this->splitContainer1->Panel2->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::splitContainer1_Panel2_Paint);
				 this->splitContainer1->Size = System::Drawing::Size(778, 336);
				 this->splitContainer1->SplitterDistance = 283;
				 this->splitContainer1->TabIndex = 29;
				 // 
				 // panel3
				 // 
				 this->panel3->Controls->Add(this->dataG);
				 this->panel3->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->panel3->Location = System::Drawing::Point(0, 44);
				 this->panel3->Name = L"panel3";
				 this->panel3->Size = System::Drawing::Size(778, 239);
				 this->panel3->TabIndex = 30;
				 // 
				 // dataG
				 // 
				 this->dataG->AllowUserToDeleteRows = false;
				 this->dataG->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
					 | System::Windows::Forms::AnchorStyles::Left)
					 | System::Windows::Forms::AnchorStyles::Right));
				 this->dataG->BackgroundColor = System::Drawing::Color::White;
				 this->dataG->BorderStyle = System::Windows::Forms::BorderStyle::None;
				 this->dataG->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
				 this->dataG->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
					 this->Column4, this->Column5,
						 this->ObjectQuantLibColumn
				 });
				 this->dataG->GridColor = System::Drawing::SystemColors::ActiveCaption;
				 this->dataG->Location = System::Drawing::Point(5, 6);
				 this->dataG->Name = L"dataG";
				 this->dataG->Size = System::Drawing::Size(774, 300);
				 this->dataG->TabIndex = 22;
				 // 
				 // Column4
				 // 
				 this->Column4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->Column4->HeaderText = L"MUST Component";
				 this->Column4->Name = L"Column4";
				 this->Column4->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
				 // 
				 // Column5
				 // 
				 this->Column5->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				 this->Column5->HeaderText = L"Must Component Type";
				 this->Column5->Name = L"Column5";
				 this->Column5->ReadOnly = true;
				 // 
				 // ObjectQuantLibColumn
				 // 
				 this->ObjectQuantLibColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
				// this->ObjectQuantLibColumn->HeaderText = L"QuantLib Objet";
				 this->ObjectQuantLibColumn->HeaderText = L"ExtLib Objet";
				 this->ObjectQuantLibColumn->Items->AddRange(gcnew cli::array< System::Object^  >(13) {
					 L"FixedLegQuantLib", L"FloatingLegQuantLib",
						 L"OptionQuantLib", L"IndexQuantLib", L"NominalQuantLib", L"startDateQuantLib", L"endDateQuantLib", L"startDateOption", L"endDateOption",
						 L"FixedRateQuantLib", L"StrikeQuantLib", L"maturityOptionQuantLib", L"CapOrFloor"
				 });
				 this->ObjectQuantLibColumn->Name = L"ObjectQuantLibColumn";
				 this->ObjectQuantLibColumn->Resizable = System::Windows::Forms::DataGridViewTriState::True;
				 // 
				 // panel2
				 // 
				 this->panel2->Controls->Add(this->ProductTypecomboBox);
				 this->panel2->Controls->Add(this->ProductTypeTextBox);
				 this->panel2->Controls->Add(this->label2);
				 this->panel2->Dock = System::Windows::Forms::DockStyle::Top;
				 this->panel2->Location = System::Drawing::Point(0, 0);
				 this->panel2->Name = L"panel2";
				 this->panel2->Size = System::Drawing::Size(778, 44);
				 this->panel2->TabIndex = 29;
				 // 
				 // ProductTypecomboBox
				 // 
				 this->ProductTypecomboBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
				 this->ProductTypecomboBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
				 this->ProductTypecomboBox->FormattingEnabled = true;
				 this->ProductTypecomboBox->Location = System::Drawing::Point(589, 5);
				 this->ProductTypecomboBox->Name = L"ProductTypecomboBox";
				 this->ProductTypecomboBox->Size = System::Drawing::Size(164, 21);
				 this->ProductTypecomboBox->TabIndex = 20;
				 this->ProductTypecomboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &TablesForm::ProductTypecomboBox_SelectedIndexChanged_1);
				 // 
				 // ProductTypeTextBox
				 // 
				 this->ProductTypeTextBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
				 this->ProductTypeTextBox->Location = System::Drawing::Point(589, 5);
				 this->ProductTypeTextBox->Name = L"ProductTypeTextBox";
				 this->ProductTypeTextBox->Size = System::Drawing::Size(164, 20);
				 this->ProductTypeTextBox->TabIndex = 28;
				 this->ProductTypeTextBox->Visible = false;
				 // 
				 // label2
				 // 
				 this->label2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
				 this->label2->AutoSize = true;
				 this->label2->Location = System::Drawing::Point(478, 13);
				 this->label2->Name = L"label2";
				 this->label2->Size = System::Drawing::Size(71, 13);
				 this->label2->TabIndex = 21;
				 this->label2->Text = L"Product Type";
				 // 
				 // Cancelbutton
				 // 
				 this->Cancelbutton->Anchor = System::Windows::Forms::AnchorStyles::Right;
				 this->Cancelbutton->Location = System::Drawing::Point(663, 11);
				 this->Cancelbutton->Name = L"Cancelbutton";
				 this->Cancelbutton->Size = System::Drawing::Size(115, 34);
				 this->Cancelbutton->TabIndex = 25;
				 this->Cancelbutton->Text = L"Cancel";
				 this->Cancelbutton->UseVisualStyleBackColor = true;
				 this->Cancelbutton->Click += gcnew System::EventHandler(this, &TablesForm::Cancelbutton_Click_1);
				 // 
				 // SaveMappingbutton
				 // 
				 this->SaveMappingbutton->Anchor = System::Windows::Forms::AnchorStyles::None;
				 this->SaveMappingbutton->Location = System::Drawing::Point(350, 11);
				 this->SaveMappingbutton->Name = L"SaveMappingbutton";
				 this->SaveMappingbutton->Size = System::Drawing::Size(122, 34);
				 this->SaveMappingbutton->TabIndex = 24;
				 this->SaveMappingbutton->Text = L"Save";
				 this->SaveMappingbutton->UseVisualStyleBackColor = true;
				 this->SaveMappingbutton->Click += gcnew System::EventHandler(this, &TablesForm::SaveMappingbutton_Click);
				 // 
				 // Creatbutton
				 // 
				 this->Creatbutton->Location = System::Drawing::Point(5, 13);
				 this->Creatbutton->Name = L"Creatbutton";
				 this->Creatbutton->Size = System::Drawing::Size(106, 34);
				 this->Creatbutton->TabIndex = 26;
				 this->Creatbutton->Text = L"Create";
				 this->Creatbutton->UseVisualStyleBackColor = true;
				 this->Creatbutton->Click += gcnew System::EventHandler(this, &TablesForm::Creatbutton_Click_1);
				 // 
				 // tableLayoutPanel5
				 // 
				 this->tableLayoutPanel5->ColumnCount = 3;
				 this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 50)));
				 this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 352)));
				 this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 186)));
				 this->tableLayoutPanel5->Controls->Add(this->textBox1, 1, 0);
				 this->tableLayoutPanel5->Controls->Add(this->tableLayoutPanel14, 1, 1);
				 this->tableLayoutPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel5->Location = System::Drawing::Point(3, 3);
				 this->tableLayoutPanel5->Name = L"tableLayoutPanel5";
				 this->tableLayoutPanel5->RowCount = 2;
				 this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
				 this->tableLayoutPanel5->Size = System::Drawing::Size(792, 72);
				 this->tableLayoutPanel5->TabIndex = 3;
				 this->tableLayoutPanel5->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &TablesForm::tableLayoutPanel5_Paint_1);
				 // 
				 // textBox1
				 // 
				 this->textBox1->BorderStyle = System::Windows::Forms::BorderStyle::None;
				 this->textBox1->Font = (gcnew System::Drawing::Font(L"Century Gothic", 27.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->textBox1->ForeColor = System::Drawing::Color::Black;
				 this->textBox1->Location = System::Drawing::Point(257, 3);
				 this->textBox1->Name = L"textBox1";
				 this->textBox1->Size = System::Drawing::Size(231, 46);
				 this->textBox1->TabIndex = 0;
				 this->textBox1->Text = L"EFFICIENCY";
				 // 
				 // tableLayoutPanel14
				 // 
				 this->tableLayoutPanel14->ColumnCount = 3;
				 this->tableLayoutPanel14->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 13.09904F)));
				 this->tableLayoutPanel14->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 86.90096F)));
				 this->tableLayoutPanel14->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
					 100)));
				 this->tableLayoutPanel14->Controls->Add(this->textBox2, 1, 0);
				 this->tableLayoutPanel14->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel14->Location = System::Drawing::Point(257, 46);
				 this->tableLayoutPanel14->Name = L"tableLayoutPanel14";
				 this->tableLayoutPanel14->RowCount = 1;
				 this->tableLayoutPanel14->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
				 this->tableLayoutPanel14->Size = System::Drawing::Size(346, 23);
				 this->tableLayoutPanel14->TabIndex = 2;
				 // 
				 // textBox2
				 // 
				 this->textBox2->BorderStyle = System::Windows::Forms::BorderStyle::None;
				 this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.25F));
				 this->textBox2->ForeColor = System::Drawing::Color::SaddleBrown;
				 this->textBox2->Location = System::Drawing::Point(35, 3);
				 this->textBox2->Name = L"textBox2";
				 this->textBox2->Size = System::Drawing::Size(187, 10);
				 this->textBox2->TabIndex = 1;
				 this->textBox2->Text = L"MANAGEMENT CONSULTING";
				 // 
				 // TablesForm
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->BackColor = System::Drawing::Color::Gainsboro;
				 this->ClientSize = System::Drawing::Size(798, 467);
				 this->Controls->Add(this->tableLayoutPanel1);
				 this->Controls->Add(this->panel1);
				 this->MinimumSize = System::Drawing::Size(639, 506);
				 this->Name = L"TablesForm";
				 this->Load += gcnew System::EventHandler(this, &TablesForm::TablesForm_Load);
				 this->panel1->ResumeLayout(false);
				 this->panel1->PerformLayout();
				 this->tableLayoutPanel1->ResumeLayout(false);
				 this->tabControl1->ResumeLayout(false);
				 this->tabPage3->ResumeLayout(false);
				 this->tableLayoutPanel2->ResumeLayout(false);
				 this->tableLayoutPanel4->ResumeLayout(false);
				 this->tableLayoutPanel4->PerformLayout();
				 this->tableLayoutPanel3->ResumeLayout(false);
				 this->tableLayoutPanel6->ResumeLayout(false);
				 this->tableLayoutPanel9->ResumeLayout(false);
				 this->tableLayoutPanel12->ResumeLayout(false);
				 this->tableLayoutPanel15->ResumeLayout(false);
				 this->Valuation->ResumeLayout(false);
				 this->tableLayoutPanel10->ResumeLayout(false);
				 this->tableLayoutPanel16->ResumeLayout(false);
				 this->tableLayoutPanel16->PerformLayout();
				 this->tableLayoutPanel11->ResumeLayout(false);
				 this->tableLayoutPanel8->ResumeLayout(false);
				 this->tableLayoutPanel7->ResumeLayout(false);
				 this->groupBox1->ResumeLayout(false);
				 this->groupBox2->ResumeLayout(false);
				 this->tableLayoutPanel13->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
				 this->tabPage1->ResumeLayout(false);
				 this->panel4->ResumeLayout(false);
				 this->splitContainer2->Panel1->ResumeLayout(false);
				 this->splitContainer2->Panel2->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer2))->EndInit();
				 this->splitContainer2->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
				 this->tabPage2->ResumeLayout(false);
				 this->splitContainer1->Panel1->ResumeLayout(false);
				 this->splitContainer1->Panel2->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
				 this->splitContainer1->ResumeLayout(false);
				 this->panel3->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataG))->EndInit();
				 this->panel2->ResumeLayout(false);
				 this->panel2->PerformLayout();
				 this->tableLayoutPanel5->ResumeLayout(false);
				 this->tableLayoutPanel5->PerformLayout();
				 this->tableLayoutPanel14->ResumeLayout(false);
				 this->tableLayoutPanel14->PerformLayout();
				 this->ResumeLayout(false);

			 }
			 /*
			 Fichier de la Form(fen�tre) de la table de mapping
			 Cette form est appel�e via le menu de la premi�re interface
			 */
#pragma endregion

	private: System::Void Cancelbutton_Click_1(System::Object^  sender, System::EventArgs^  e) {
		////////
		ObjectQuantLibColumn->ReadOnly = true;
		ProductTypecomboBox->Visible = true;
		ProductTypeTextBox->Visible = false;

		/////
		if (ProductTypecomboBox->SelectedIndex != -1)

		{
			char* typeProd_string = (char*)Marshal::StringToHGlobalAnsi(ProductTypecomboBox->SelectedItem->ToString()).ToPointer();
			EfficiencyProduct::EfficiencyTypeProduct typePrduct = EfficiencyProduct::stringToTypeProduct(typeProd_string);

			string path = "";

			for (size_t indice = 0; indice < catalogue->VecCatalogue.size(); indice++)
			{
				if (catalogue->VecCatalogue[indice][0] != "" && catalogue->VecCatalogue[indice][1] != "")
				{
					if (ProductTypecomboBox->SelectedItem->ToString() == gcnew String(catalogue->VecCatalogue[indice][1].c_str())) { // si c'est le m�me type de produit
						path = catalogue->VecCatalogue[indice][4];

						break;
					}
				}
			}
			if (path != "") {
				try
				{
					mapping = new Mapping(path, typePrduct);
					affiche_dataMapping();
				}
				catch (char* const& err)
				{
					MessageBox::Show(gcnew String(err), "Erreur du chargement",
						MessageBoxButtons::OK, MessageBoxIcon::Error);

				}
			}


		}
		else
		{
			dataG->Rows->Clear();
		}
	}
			 //bouton enregistrer les modifications de la table de mapping
	private: System::Void SaveMappingbutton_Click(System::Object^  sender, System::EventArgs^  e) {
		// si l'utilisateur veut afficher la table de mapping
		if (ProductTypecomboBox->Visible == true)

		{
			saveMappingModifiedProduct();

		}
		// si on veut cr�er un nouveau produit
		if (ProductTypeTextBox->Visible == true)
		{
			saveMappingNewProduct();
		}

	}
	private: System::Void saveMappingModifiedProduct() {

		//si un des �l�ments de la comboBox est selectionn�
		if (ProductTypecomboBox->SelectedIndex != -1)
		{
			char* typeProd_string = (char*)Marshal::StringToHGlobalAnsi(ProductTypecomboBox->SelectedItem->ToString()).ToPointer();

			EfficiencyProduct::EfficiencyTypeProduct typePrduct = EfficiencyProduct::stringToTypeProduct(typeProd_string);

			string path = "";
			for (size_t indice = 0; indice < catalogue->VecCatalogue.size(); indice++)
			{
				if (catalogue->VecCatalogue[indice][0] != "" && catalogue->VecCatalogue[indice][1] != "")
				{
					if (ProductTypecomboBox->SelectedItem->ToString() == gcnew String(catalogue->VecCatalogue[indice][1].c_str())) { // si c'est le m�me type de produit
						path = catalogue->VecCatalogue[indice][4];
						break;
					}
				}
			}
			if (path != "") {
				try
				{
					mapping = new Mapping(path, typePrduct);
					System::String ^ cell1;
					System::String ^ cell2;

					char* newCell1;
					char* newCell2;

					// mettre � jour le XML de param�trage par les �l�ments de la datagrid
					for (int row = 0; row < dataG->RowCount - 1; row++)
					{

						cell1 = dataG->Rows[row]->Cells[0]->Value->ToString();
						cell2 = dataG->Rows[row]->Cells[2]->Value->ToString();
						newCell1 = (char*)Marshal::StringToHGlobalAnsi(cell1).ToPointer();
						newCell2 = (char*)Marshal::StringToHGlobalAnsi(cell2).ToPointer();
						if (strcmp(newCell1, "") != 0 && strcmp(newCell2, "") != 0)
						{
							mapping->updateXML(newCell2, newCell1);
						}
					}
				}
				catch (char* const& err)
				{
					MessageBox::Show(gcnew String(err), "Erreur du chargement",
						MessageBoxButtons::OK, MessageBoxIcon::Error);

				}
			}

		}
		else // si la comboBox est vide
		{
			MessageBox::Show(
				"Veuillez choisir le type de votre produit",
				"Type du produit invalide", MessageBoxButtons::OK,
				MessageBoxIcon::Warning);
		}
	}

	private: System::Void saveMappingNewProduct() {
		// si l'utilisateur saisit un type de produit
		if (ProductTypeTextBox->Text != "")
		{
			char* prodName;
			bool isFull; // isFull = true : si la cellule est pleine
			Mapping* newMapping = new Mapping(); //constructeur sans argument
			vector<vector<string>> vectorMapping;// vecteur qui contient les elements de la dataGrid
			if (ProductTypeTextBox->Text != "" && dataG->Rows->Count>2)
			{

				//tester si le 1er �l�ment de la datagrid existe
				if (dataG->Rows[0]->Cells[0]->Value != nullptr)  isFull = true;
				else isFull = false;

				//parcourir la dataGrid
				// tester si tous les elements de la datagrid ne sont pas vides 
				//si seulement un est vide alors isFull = false
				for (int indice = 0; indice < dataG->Rows->Count - 1; indice++)
				{
					if (dataG->Rows[indice]->Cells[0]->Value == nullptr || dataG->Rows[indice]->Cells[2]->Value == nullptr)
					{

						isFull = isFull && false;
					}
				}
				//si aucun element n'est vide
				if (isFull)
				{
					prodName = (char*)Marshal::StringToHGlobalAnsi(ProductTypeTextBox->Text).ToPointer();
					string prodNameS(prodName);

					// cr�ation de la table
					vectorMapping = CreatTableVector();
					newMapping->creat_Table(prodNameS, vectorMapping);

					//update du catalogue

					newMapping->updateCatalogue(prodName);
					// v�rifier si le fichier xml de param�trage est cr�e
					string pathFile = "..//product file//" + prodNameS + ".xml";
					TiXmlDocument doc_param(pathFile.c_str());

					if (doc_param.LoadFile())
					{
						//affichade d'un message : fichier enregistr�
						MessageBox::Show(
							"Fichier XML de param�trage de la nouvelle table de mapping est enregistr� avec succ�s !",
							"Fichier enregistr�", MessageBoxButtons::OK,
							MessageBoxIcon::None);

						TypeColumn->Items->AddRange(gcnew String(prodName));
						ProductTypecomboBox->Items->Add(gcnew String(prodName));
					}

				}

			}
		}

		else // si le textBox est vide
			MessageBox::Show(
				"Veuillez saisir le nom de votre produit",
				"Nom du produit invalide", MessageBoxButtons::OK,
				MessageBoxIcon::Warning);
	}

			 vector < vector < string>> CreatTableVector()
			 {
				 vector<string> ligne;
				 vector<vector<string>> table;
				 char* colonne0;
				 char* colonne2;
				 for (int indice = 0; indice < dataG->Rows->Count - 1; indice++)
				 {
					 ligne.clear();

					 // 1ere colonne de la dataGrid
					 colonne0 = (char*)Marshal::StringToHGlobalAnsi(dataG->Rows[indice]->Cells[0]->Value->ToString()).ToPointer();
					 string c0(colonne0);

					 // 3eme colonne de la dataGrid
					 colonne2 = (char*)Marshal::StringToHGlobalAnsi(dataG->Rows[indice]->Cells[2]->Value->ToString()).ToPointer();
					 string c2(colonne2);

					 string c1;

					 // 2eme colonne de la dataGrid
					 // affectation de chaque objet QuantLib � un type Must
					 if (c2 == "FixedLegQuantLib" || c2 == "FloatingLegQuantLib")
					 {
						 c1 = "COMPONENT_CASHFLOW";
					 }

					 if (c2 == "IndexQuantLib")
					 {
						 c1 = "COMPONENT_INDEX";
					 }


					 if (c2 == "NominalQuantLib")
					 {
						 c1 = "COMPONENT_PRINCIPAL";
					 }

					 if (c2 == "startDateQuantLib" || c2 == "endDateQuantLib")
					 {
						 c1 = "FULLDATE";
					 }

					 if (c2 == "FixedRateQuantLib")
					 {
						 c1 = "RATE";
					 }


					 // affectation des donn�es au vecteur ligne
					 ligne.push_back(c0);
					 ligne.push_back(c1);
					 ligne.push_back(c2);

					 //ajout du vecteur ligne au vecteur table
					 table.push_back(ligne);

				 }
				 return table;
			 }

	private: System::Void Creatbutton_Click_1(System::Object^  sender, System::EventArgs^  e) {
		ObjectQuantLibColumn->ReadOnly = false;
		dataG->Rows->Clear();
		ProductTypecomboBox->Visible = false;
		ProductTypeTextBox->Visible = true;
		ProductTypeTextBox->Clear();
	}

	private: System::Void ProductTypecomboBox_SelectedIndexChanged_1(System::Object^  sender, System::EventArgs^  e) {
		if (ProductTypecomboBox->SelectedIndex != -1)
		{



			char* typeProd_string = (char*)Marshal::StringToHGlobalAnsi(ProductTypecomboBox->SelectedItem->ToString()).ToPointer();
			EfficiencyProduct::EfficiencyTypeProduct typePrduct = EfficiencyProduct::stringToTypeProduct(typeProd_string);
			string path = "";

			for (size_t indice = 0; indice < catalogue->VecCatalogue.size(); indice++)
			{
				if (catalogue->VecCatalogue[indice][0] != "" && catalogue->VecCatalogue[indice][1] != "")
				{
					if (ProductTypecomboBox->SelectedItem->ToString() == gcnew String(catalogue->VecCatalogue[indice][1].c_str())) { // si c'est le m�me type de produit
						path = catalogue->VecCatalogue[indice][4];

						break;
					}
				}
			}
			if (path != "") {
				try
				{
					mapping = new Mapping(path, typePrduct);
					affiche_dataMapping();
				}
				catch (char* const& err)
				{
					MessageBox::Show(gcnew String(err), "Erreur du chargement",
						MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
			}

		}
	}
			 void affiche_dataMapping() {


				 String^ typeProd_string = gcnew String(mapping->typeProd.c_str());

				 if (ProductTypecomboBox->SelectedItem->ToString() == typeProd_string)
				 {
					 //mapping : objet de la classe mapping

					 dataG->Rows->Clear();
					 std::size_t taille = mapping->map.size();
					 dataG->Rows->Add(taille); // la taille de la dataGridView  = taille de la matrice map //pour faire le test

											   //===============================================================
											   //				AFFICHAGE DANS LA TABLE DataGridView
											   //===============================================================

					 for (std::size_t i = 0; i <taille; i++)
					 {
						 dataG->Rows[i]->Cells[0]->Value = gcnew String(std::get<0>(mapping->map[i]).c_str());
						 dataG->Rows[i]->Cells[1]->Value = gcnew String(std::get<1>(mapping->map[i]).c_str());
						 dataG->Rows[i]->Cells[2]->Value = gcnew String(std::get<2>(mapping->map[i]).c_str());

						 //ObjectQuantLibColumn
					 }//fin de la boucle d'affichage
					  //==================  fin de l'affichage ==================

				 }

				 else
				 {
					 dataG->Rows->Clear();
				 }


			 }

			 //void fillTableMappingVector(){
			 //	vector<string> ligne;
			 //		vector<vector<string>> table;
			 //		char* colonne0;
			 //		char* colonne2;
			 //		for (int indice = 0; indice < dataG->Rows->Count - 1; indice++)
			 //		{
			 //			ligne.clear();
			 //	
			 //			// 1ere colonne de la dataGrid
			 //			colonne0 = (char*)Marshal::StringToHGlobalAnsi(dataG->Rows[indice]->Cells[0]->Value->ToString()).ToPointer();
			 //			string c0(colonne0);
			 //	
			 //			// 3eme colonne de la dataGrid
			 //			colonne2 = (char*)Marshal::StringToHGlobalAnsi(dataG->Rows[indice]->Cells[2]->Value->ToString()).ToPointer();
			 //			string c2(colonne2);
			 //	
			 //			string c1;
			 //	
			 //			// 2eme colonne de la dataGrid
			 //			// affectation de chaque objet QuantLib � un type Must
			 //			if (c2 == "FixedLegQuantLib" || c2 == "FloatingLegQuantLib")
			 //			{
			 //				c1 = "COMPONENT_CASHFLOW";
			 //			}
			 //	
			 //			if (c2 == "IndexQuantLib")
			 //			{
			 //				c1 = "COMPONENT_INDEX";
			 //			}
			 //	
			 //	
			 //			if (c2 == "NominalQuantLib")
			 //			{
			 //				c1 = "COMPONENT_PRINCIPAL";
			 //			}
			 //	
			 //			if (c2 == "startDateQuantLib" || c2 == "endDateQuantLib")
			 //			{
			 //				c1 = "FULLDATE";
			 //			}
			 //	
			 //			if (c2 == "FixedRateQuantLib")
			 //			{
			 //				c1 = "RATE";
			 //			}
			 //	
			 //	
			 //			// affectation des donn�es au vecteur ligne
			 //			ligne.push_back(c0);
			 //			ligne.push_back(c1);
			 //			ligne.push_back(c2);
			 //	
			 //			//ajout du vecteur ligne au vecteur table
			 //			table.push_back(ligne);
			 //	
			 //		}
			 //		//return table;
			 //
			 //}
			 // chargement de la form

	private: System::Void fillDataGridViewCatalogue() {
		try {
			for (size_t indice = 0; indice < catalogue->VecCatalogue.size(); indice++) {
				if (catalogue->VecCatalogue[indice][0] != "" && catalogue->VecCatalogue[indice][1] != "") {
					//remplir les colonnes de la dataGrid avec les elements du vecteur VecCatalogue de l'objet catalogue
					dataGridView1->Rows[indice]->Cells[0]->Value = gcnew String(catalogue->VecCatalogue[indice][0].c_str());
					dataGridView1->Rows[indice]->Cells[1]->Value = gcnew String(catalogue->VecCatalogue[indice][1].c_str());
					dataGridView1->Rows[indice]->Cells[2]->Value = gcnew String(catalogue->VecCatalogue[indice][2].c_str());
					dataGridView1->Rows[indice]->Cells[3]->Value = gcnew String(catalogue->VecCatalogue[indice][3].c_str());
				}
			}
		}
		catch (char* const& err) {
			MessageBox::Show(gcnew String(err), "Erreur du chargement",
				MessageBoxButtons::OK, MessageBoxIcon::Error);
		}

	}
	private: System::Void TablesForm_Load(System::Object^  sender, System::EventArgs^  e) {
		//objet de la classe Catalogue
		if (catalogue->VecCatalogue.size() != 0)
		{
			//initier la taille de la datagridView
			dataGridView1->Rows->Add(catalogue->VecCatalogue.size());

			vector<string> elementsComboBox;
			//boucle sur les elements du vecteur qui contient les noms et les types de produits 
			// et les chemins  des XML de param�trage

			for (size_t indice = 0; indice < catalogue->VecCatalogueModel.size(); indice++)
			{
				//remplir la comboBox de la colonnes 4 par les modeles
				ModelColumn->Items->AddRange(gcnew String(catalogue->VecCatalogueModel[indice].c_str()));

			}
			for (size_t indice = 0; indice < catalogue->VecCatalogueType.size(); indice++)
			{
				//remplir un autre vecteur avce les types de produits
				elementsComboBox.push_back(catalogue->VecCatalogueType[indice][0]);
			}

			//supprimer les doublons dans le vecteur elementsComboBox
			elementsComboBox = catalogue->SuppDoublons(elementsComboBox);

			for (size_t indice = 0; indice < elementsComboBox.size(); indice++)
			{
				//remplir les comboBox de la datagrid et la comboBox de la table de mapping
				ProductTypecomboBox->Items->Add(gcnew String(elementsComboBox[indice].c_str()));
				TypeColumn->Items->AddRange(gcnew String(elementsComboBox[indice].c_str()));

			}

			fillDataGridViewCatalogue();

		}

	}
	private: System::Void SaveCataloguebutton_Click_1(System::Object^  sender, System::EventArgs^  e) {
		//Catalogue* catalogue = new Catalogue();
		try {
			catalogue->deleteCatalogue();

			for (int indice = 0; indice < dataGridView1->Rows->Count - 1; indice++)
			{
				if (dataGridView1->Rows[indice]->Cells[0]->Value != nullptr
					&& dataGridView1->Rows[indice]->Cells[1]->Value != nullptr
					&& dataGridView1->Rows[indice]->Cells[3]->Value != nullptr)
				{
					char* name = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[0]->Value->ToString()).ToPointer();
					char* type = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[1]->Value->ToString()).ToPointer();
					char* activate;
					if (dataGridView1->Rows[indice]->Cells[2]->Value == nullptr)
						activate = "False";
					else
						activate = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[2]->Value->ToString()).ToPointer();

					char* modele = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[3]->Value->ToString()).ToPointer();

					if (!catalogue->isInCatalogue(string(type), string(name)))
					{
						if (!catalogue->doublonsActifs(string(name), string(activate), indice))
						{
							catalogue->updateCatalogue(type, name, activate, modele);
							catalogueIsChanged = true;
						}
						else
						{
							MessageBox::Show(
								"Un produit du m�me nom est d�j� activ�",
								"Produit existant", MessageBoxButtons::OK,
								MessageBoxIcon::Warning);

							dataGridView1->Rows[indice]->Cells[2]->Value = "False";
						}

					}
					else
					{
						MessageBox::Show(
							"Un produit du m�me type est enregistr� sous le m�me nom",
							"Produit existant", MessageBoxButtons::OK,
							MessageBoxIcon::Warning);

						dataGridView1->Rows[indice]->Cells[0]->Value = nullptr;
					}
				}
				else
				{
					/*MessageBox::Show(
						"Veuillez entrer tous les champs !",
						"Donn�es manquantes", MessageBoxButtons::OK,
						MessageBoxIcon::Warning);*/

				}
			}
		}
		catch (char* const& err) {
			MessageBox::Show(gcnew String(err), "Erreur du chargement",
				MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
	}


private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) //buttonsave fast mapper
{
	/*
#pragma region OLDMethode


	Catalogue* catalogueclone;
	//catalogueclone->deleteCatalogue();
	for (int indice = 0; indice < catalogue->VecCatalogue.size() ; indice++)
	{
		for (int i = 0; i < dataGridView2->Rows->Count-1; i++)
		{
			//Data from Catalogue
			string type = catalogue->VecCatalogue[indice][1];
			char* type_ = (char*)Marshal::StringToHGlobalAnsi(gcnew String(type.c_str())).ToPointer();

			string activate = catalogue->VecCatalogue[indice][2];
			char* activate_ = (char*)Marshal::StringToHGlobalAnsi(gcnew String(activate.c_str())).ToPointer();

			string nameproduct = catalogue->VecCatalogue[indice][0];
			char* nameproduct_ = (char*)Marshal::StringToHGlobalAnsi(gcnew String(nameproduct.c_str())).ToPointer();

			string mynameproduct = (char*)Marshal::StringToHGlobalAnsi(dataGridView2->Rows[i]->Cells[0]->Value->ToString()).ToPointer();
			char* mynameproduct_ = (char*)Marshal::StringToHGlobalAnsi(dataGridView2->Rows[i]->Cells[0]->Value->ToString()).ToPointer();

			if (catalogue->VecCatalogue[indice][0] == mynameproduct)
			{
				//Model from dataGrill
				char* modele = (char*)Marshal::StringToHGlobalAnsi(dataGridView2->Rows[i]->Cells[2]->Value->ToString()).ToPointer();
				//modification of catalogue
				catalogueclone->updateCatalogue(type_, mynameproduct_, activate_, modele);
				catalogueIsChanged = true;
			}
			else
			{
				string modele = catalogue->VecCatalogue[indice][3];
				char* modele_ = (char*)Marshal::StringToHGlobalAnsi(gcnew String(modele.c_str())).ToPointer();
				catalogueclone->updateCatalogue(type_, nameproduct_, activate_, modele_);
				catalogueIsChanged = true;
			}
		}
		
	}

	catalogue = catalogueclone;
#pragma endregion
*/

	fillDataGridViewCatalogue();
	int size = dataGridView1->Rows->Count;
	for (int indice = 0; indice < size - 2; indice++)
	{
		string NAmeProductCatalogue = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[0]->Value->ToString()).ToPointer();
		for (int i = 0; i < dataGridView2->Rows->Count - 1; i++)
		{
			string NAmeProduct = (char*)Marshal::StringToHGlobalAnsi(dataGridView2->Rows[i]->Cells[0]->Value->ToString()).ToPointer();
			if (dataGridView2->Rows[i]->Cells[0]->Value->ToString() == dataGridView1->Rows[indice]->Cells[0]->Value->ToString())
			{
				dataGridView1->Rows[indice]->Cells[3]->Value = dataGridView2->Rows[i]->Cells[2]->Value;
			}
		}

	}
	SaveCataloguebutton_Click_1(sender, e);

}

	private: System::Void  Closebutton_Click_1(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}

	private: System::Void dataG_DataError(System::Object^ sender, DataGridViewDataErrorEventArgs^ anError)
	{
		anError->ThrowException = false;


	}


	private: System::Void CancelTypeProdButton_Click_1(System::Object^  sender, System::EventArgs^  e) {
		/* � compl�ter*/
		catalogueIsChanged = false;
	}
	private: System::Void tableLayoutPanel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
	}
private: System::Void label3_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void tableLayoutPanel3_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
	private: System::Void Pricebutton_Click(System::Object^  sender, System::EventArgs^  e) {
		//Effa�age des r�sultats : tableaux des npv et des grecs
		tableauNPV->clear();
		tableauGrecs->clear();

		if (this->formTable != nullptr)
		{
			if (formTable->catalogueIsChanged) {
				EffLibProducts->setModelsProductsTypes();
				EffLibProducts->AllProducts.clear();
				EffLibProducts->makeProducts();
			}
		}
		/*pricer tous les trade qui sont dans "EffLibProducts" */
		EffLibProducts->setModelsProductsTypes();
		EffLibProducts->AllProducts.clear();
		EffLibProducts->makeProducts();

		if (EffLibProducts->AllProducts[0] != NULL) {
			double npv = 0;
			double npvFixedLeg = 0;
			double npvFloatingLeg = 0;
			double delta, vega, gamma, theta;

			double npvSomme = 0;
			double deltaSomme, vegaSomme, gammaSomme, thetaSomme;
			string type;
			string id;
			for (std::size_t numProd = 0; numProd < EffLibProducts->AllProducts.size(); numProd++)
			{
				npv = (EffLibProducts->AllProducts[numProd])->price();
				npvFixedLeg = (EffLibProducts->AllProducts[numProd])->npvFixedLeg;
				npvFloatingLeg = (EffLibProducts->AllProducts[numProd])->npvFloatingLeg;
				delta = (EffLibProducts->AllProducts[numProd])->delta;
				vega = (EffLibProducts->AllProducts[numProd])->vega;
				gamma = (EffLibProducts->AllProducts[numProd])->gamma;
				theta = (EffLibProducts->AllProducts[numProd])->theta;

				DeuxChiff(&npv);
				DeuxChiff(&npvFixedLeg);
				DeuxChiff(&npvFloatingLeg);
				DeuxChiff(&npvFloatingLeg);
				DeuxChiff(&delta);
				DeuxChiff(&vega);
				DeuxChiff(&gamma);
				DeuxChiff(&theta);

				type = EffLibProducts->portefeuille.AllTrades->operator[](numProd)->typeProduct;
				id = EffLibProducts->portefeuille.AllTrades->operator[](numProd)->tradeID;

				tableauNPV->push_back(std::make_tuple(id, type, npv, npvFixedLeg, npvFloatingLeg));
				tableauGrecs->push_back(std::make_tuple(id, type, delta, vega, gamma, theta));
				npvSomme += npv;
				deltaSomme += delta;
				gammaSomme += gamma;
				vegaSomme += vega;
				thetaSomme += theta;
			}
			NPVTextBox->Text = npvSomme.ToString();
			DeltaTextBox->Text = deltaSomme.ToString();
			GammaTextBox->Text = gammaSomme.ToString();
			VegaTextBox->Text = vegaSomme.ToString();

		}
	}

	private: System::Void Loadbutton_Click_1(System::Object^  sender, System::EventArgs^  e) {

		// Effacer les r�sultats du pricing pr�c�dent
		LoadTextBox->Text = "";
		NPVTextBox->Text = "";
		DeltaTextBox->Text = "";
		GammaTextBox->Text = "";
		VegaTextBox->Text = "";
		dataGridView2->Rows->Clear();
		PricingModelGrill->Items->Clear();
		


		// ouverture de la fen�tre pour choisir le fichier
		OpenFileDialog ^ openFileDialog1 = gcnew OpenFileDialog;
		openFileDialog1->ShowDialog();
		String ^ FilePath = openFileDialog1->FileName;
		LoadTextBox->Text = FilePath;
		int day = dateTimePicker1->Value.Day, month = dateTimePicker1->Value.Month, year = dateTimePicker1->Value.Year;
		QuantLib::Date pricingDate = QuantLib::Date(QuantLib::Day(day),Month(month),Year(year));
		//DateTextBox->Text =

		// si un chemin est indiqu�
		if (LoadTextBox->Text != "") {
			char* pathC = (char*)Marshal::StringToHGlobalAnsi(LoadTextBox->Text).ToPointer();
			string ret(pathC); // convert char* to string
			string extension = pathC;
			extension.erase(0, extension.length() - 3);

			try {
				if (boost::iequals(extension, "xml")) {
					TiXmlDocument doc(pathC);

					if (!doc.LoadFile()) {
						char* err = "Erreur du chargement du fichier XML";
						throw err;
					}
					else // si le fichier est charg�
					{
						// cr�ation de la fen�tre de la table de mapping

						//DateTextBox->Enabled = true;
						//DateTextBox->Text = toda

						DeltaTextBox->Enabled = true;
						NPVTextBox->Enabled = true;
						GammaTextBox->Enabled = true;
						VegaTextBox->Enabled = true;
						Pricebutton->Enabled = true;
						NPVDetailButton->Enabled = true;
						RiskFactorButton->Enabled = true;
						TiXmlHandle hdl(&doc);
						//ProductName->Text =
						try {
							/*Construction of all products*/
							EffLibProducts = new EfficiencyLibProducts(pricingDate, doc);
							EffLibProducts->setModelsProductsTypes();
							EffLibProducts->makeProducts();


						}
						catch (string e) {

							MessageBox::Show(gcnew String(e.c_str()), "Erreur",
								MessageBoxButtons::OK, MessageBoxIcon::Error); //kb
						}
			
					}
				}
				else {
					char* err = "Erreur du chargement du fichier XML";
					throw err;
				}
			}

			catch (char* const& err) {
				MessageBox::Show(gcnew String(err), "Erreur du chargement",
					MessageBoxButtons::OK, MessageBoxIcon::Error);

				NPVTextBox->Enabled = false;
				Pricebutton->Enabled = false;
				NPVDetailButton->Enabled = false;
				RiskFactorButton->Enabled = false;
				//DateTextBox->Enabled = false;
				GammaTextBox->Enabled = false;
				VegaTextBox->Enabled = false;
			}
		}

		//Fast Mapper 

#pragma region"Initilization"
		//Initialization:
		dataGridView2->Rows->Add(EffLibProducts->portefeuille.AllTrades->size());
		//vector<string> TypeelementsComboBox;
		//
		//for (size_t indice = 0; indice < catalogue->VecCatalogueType.size(); indice++)
		//{
		//	//remplir un autre vecteur avce les types de produits
		//	TypeelementsComboBox.push_back(catalogue->VecCatalogueType[indice][0]);
		//}

		////supprimer les doublons dans le vecteur elementsComboBox
		//TypeelementsComboBox = catalogue->SuppDoublons(TypeelementsComboBox);
		//for (size_t indice = 0; indice < TypeelementsComboBox.size(); indice++)
		//{
		//	//remplir les comboBox de la datagrid et la comboBox de la table de mapping
		//	ProductTypeGrill->Items->AddRange(gcnew String(TypeelementsComboBox[indice].c_str()));

		//}
		ProductTypeGrill->ReadOnly = true;
		ProductNameGrill->ReadOnly = true;
		SaveButton->Enabled = false;
#pragma endregion

		//XML Reading
		for (int iTrade = 0; iTrade < EffLibProducts->portefeuille.AllTrades->size(); iTrade++)
		{
			string ProductName = EffLibProducts->portefeuille.AllTrades->operator[](iTrade)->nameProduct;
			dataGridView2->Rows[iTrade]->Cells[0]->Value = gcnew String(ProductName.c_str());
			if (catalogue->SearchType(ProductName) != "")
			{
				SaveButton->Enabled = true;
				for (size_t indice = 0; indice < catalogue->VecCatalogueModel.size(); indice++)
				{
					if (isModel(catalogue->SearchType(ProductName), catalogue->VecCatalogueModel[indice]))
						PricingModelGrill->Items->AddRange(gcnew String(catalogue->VecCatalogueModel[indice].c_str()));
				}
				string Type = catalogue->SearchType(ProductName);
				string modelProduct_ = catalogue->SearchModel(ProductName);
				string typeProduct_ = catalogue->SearchType(ProductName);
				dataGridView2->Rows[iTrade]->Cells[1]->Value = gcnew String(typeProduct_.c_str());
				dataGridView2->Rows[iTrade]->Cells[2]->Value = gcnew String(modelProduct_.c_str());
				
			}
			else
			{
				PricingModelGrill->ReadOnly = true;
		
				MessageBox::Show(gcnew String(ProductName.c_str()), ": Add Product on Catalogue",
					MessageBoxButtons::OK, MessageBoxIcon::Error);


			}
		}
	}

	private: bool isModel(string typeProduct, string Model)
	{
		if (typeProduct == "SWAP")
		{
			if (Model == "NUMERIX_MODEL" || Model == "QUANTLIB_FLOW" || Model == "QUANTLIB_BLACKFT" || Model == "QUANTLIB_LMM") return 1;
			else return 0;
		}
		if (typeProduct == "CAPFLOOR")
		{
			if (Model == "NUMERIX_MODEL" || Model == "QUANTLIB_LMM" || Model == "QUANTLIB_BLACKFT" || Model == "EFFICIENCY_LMM") return 1;
			else return 0;
		}
		if (typeProduct == "SWAPTION")
		{
			if (Model == "NUMERIX_MODEL" || Model == "QUANTLIB_LMM" || Model == "QUANTLIB_BLACKFT" || Model == "EFFICIENCY_LMM") return 1;
			else return 0;
		}
		return 0;

	}

	private: System::Void RiskFactorButton_Click(System::Object^  sender, System::EventArgs^  e) {
		GrecsForm ^ grecsFrom = gcnew GrecsForm();
		grecsFrom->tableauGrecs = tableauGrecs;
		grecsFrom->Show();
	}
			 void DeuxChiff(double *my_double) /* test attend l'adresse d'un entier... */
			 {
				 if (*my_double >= 0)
					 *my_double = (floor(*my_double * 100)) / 100;
				 else
					 *my_double = (floor(*my_double * 100 + 1)) / 100;
				 return;
			 }

private: System::Void NPVDetailButton_Click(System::Object^  sender, System::EventArgs^  e) {
	DetailsForms ^ detailsForm = gcnew DetailsForms();
	detailsForm->tableauNPV = tableauNPV;
	// afficher l'�cran "D�tails"
	detailsForm->Show();
}
private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}

private: System::Void label8_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label9_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void tableLayoutPanel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void tableLayoutPanel5_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	////char* modele = (char*)Marshal::StringToHGlobalAnsi(dataGridView1->Rows[indice]->Cells[3]->Value->ToString()).ToPointer();
	//for (size_t indice = 0; indice < catalogue->VecCatalogue.size(); indice++) {
	//	if (catalogue->VecCatalogue[indice][0] != "" && catalogue->VecCatalogue[indice][1] != "") {
	//		//remplir les colonnes de la dataGrid avec les elements du vecteur VecCatalogue de l'objet catalogue
	//		string caloguetype = String(catalogue->VecCatalogue[indice][1].c_str());

	//		if(caloguetype == (comboBox1->SelectedValue->ToString())
	//		
	//	}
	//}
}
private: System::Void RiskFactorButton_Click_1(System::Object^  sender, System::EventArgs^  e) {
	GrecsForm ^ grecsFrom = gcnew GrecsForm();
	grecsFrom->tableauGrecs = tableauGrecs;
	grecsFrom->Show();
}
private: System::Void NPVDetailButton_Click_2(System::Object^  sender, System::EventArgs^  e) {
	DetailsForms ^ detailsForm = gcnew DetailsForms();
	detailsForm->tableauNPV = tableauNPV;
	// afficher l'�cran "D�tails"
	detailsForm->Show();
}
private: System::Void NPVTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void tableLayoutPanel1_Paint_1(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void tableLayoutPanel3_Paint_1(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void tableLayoutPanel3_Paint_2(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void monthCalendar1_DateChanged(System::Object^  sender, System::Windows::Forms::DateRangeEventArgs^  e) {
}




private: System::Void tableLayoutPanel6_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}

private: System::Void tableLayoutPanel10_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void splitContainer1_Panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void dataGridView2_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{

}






private: System::Void groupBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
}

private: System::Void tableLayoutPanel5_Paint_1(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void tableLayoutPanel16_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}


private: System::Void dataGridView2_CellContentClick_1(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void ProductTypecomboBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
}
};
}
