#pragma once
#include <ql/quantlib.hpp>
#include "CurveDataMod.h"
#include "CapFloorEfficiencyEngine.h"
#include "SwaptionEfficiencyEngine.h"
#include "DllHeader.h"

using namespace std;
using namespace QuantLib;
using namespace boost;

class EFFICIEBCYLIBDLL_API LMMEfficiencyEngine :public SwaptionEfficiencyEngine, public CapFloorEfficiencyEngine
{
public:
	std::vector <QuantLib::Date > date2;
	std::vector<Rate> fwdvect;
	Matrix VOLMATR1;
	//Constuctor
	LMMEfficiencyEngine(QuantLib::Date startDate, QuantLib::Date endDate, Frequency fixedLegFrequency)
	{
		date2 = vectorDates(startDate, endDate, fixedLegFrequency);
		CurveData2 Data;
		CurveData2 temp;
		temp.sampleMktData2(Data);
		/*temp.buildCurve2(Data);*/
		std::vector<Rate>  Fwd = Data.Calcul_Fwd(date2);
		int n = date2.size();
		std::vector<Rate> fwdvect2(n);
		for (int i = 0; i < n; i++){
			fwdvect2[i] = Fwd[i] / 100;
		}
		fwdvect = fwdvect2;
		VOLMATR1 = matRebonato(0.02, 0.1, 0.1, 0.01, date2);
	}
	//forward Rate
	Matrix LMMmatforward() override{
		
		return matforwardLMM(fwdvect, VOLMATR1, date2);
	}
	Matrix LMMSwaptionmatforward() override{

		return matforwardLMM(fwdvect, VOLMATR1, date2);
	}

private:
	std::vector <QuantLib::Date > vectorDates(QuantLib::Date startDate, QuantLib::Date endDate, Frequency frequency){
		std::vector <QuantLib::Date > result;
		int i = 0;
		QuantLib::Date starDatetemp = startDate;
		result.push_back(starDatetemp);
		while (result[i] < endDate){
			i++;
			starDatetemp.operator+=(Period(frequency));
			result.push_back(starDatetemp);
		}
		return result;
	}
	Matrix matRebonato(float a, float b, float c, float d, std::vector<QuantLib::Date> &dates){
		int n = dates.size();
		Matrix M(n, n);

		int i = 0;
		int j = 0;
		std::vector<Time> D(n);
		std::vector<float> sigma(n);
		float t = 0;
		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
		}

		for (j = 0; j < n; j++){

			for (i = 0; i < j; i++){
				M[i][j] = 0;
			}
			for (i = j; i < n; i++){
				t = D[j];
				sigma = Rebonato(a, b, c, d, dates, t);

				M[i][j] = sigma[i];
			}
		}
		return M;
	}
	Matrix matforwardLMM(std::vector<Rate> &forwardRate, Matrix &volMat, std::vector<QuantLib::Date> &dates){

		int n = forwardRate.size();
		Matrix matF(n, n);
		std::vector<float> mu(n);
		std::vector<float> muvec(n);
		std::vector<float> volMatvec(n);
		std::vector<float> volMatvec2(n);
		std::vector<float> MU(n);
		std::vector<float> MU2(n);
		std::vector<float> MU3(n);
		std::vector<Rate> FR(n);
		std::vector<Time> D(n);
		std::vector<float> delta(n);
		//float z = 0.7;

		std::vector<float> z(n);

		for (int i = 0; i < n; i++){
			z[i] = randomloinormal();
		}


		int j = 0;
		int i = 0;

		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
			//std::cout << "d" << i << "" << D[i] << std::endl;
		}
		for (j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
			//std::cout << "delta " << delta[j] << std::endl;
		}
		delta[n - 1] = delta[n - 2];
		//std::cout << delta[n - 1] << std::endl;


		FR = forwardRate;


		for (i = 0; i < n; i++){

			for (j = 0; j < i; j++){
				matF[j][i] = 0;
			}

			for (j = i; j < n; j++){


				mu = drift(FR, volMat, dates, i);
				muvec[j] = mu[j] * D[i];

				volMatvec[j] = volMat[j][i];
				volMatvec2[j] = (volMatvec[j] * volMatvec[j]) / 2;

				MU[j] = (muvec[j] - volMatvec2[j])* delta[i];
				MU2[j] = std::sqrt(delta[i])*volMatvec[j] * z[i];
				MU3[j] = exp(MU[j] + MU2[j]);

				FR[j] = FR[j] * MU3[j];//L'equation du LMM

				matF[j][i] = FR[j];


			}
		}


		return matF;

	}
	std::vector<float> Rebonato(float a, float b, float c, float d, std::vector<QuantLib::Date> &dates, float t){
		int n = dates.size();
		std::vector<float> sigma(n);
		int i = 0;
		std::vector<Time> D(n);
		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
		}
		//sigma[0] = c;
		for (i = 0; i < n; i++){

			sigma[i] = (a*(D[i] - t) + d) * exp(-b*(D[i] - t)) + c;
		}


		return sigma;
	}
	double randomloinormal(){
		double x1 = randomUniform2(0, 1);
		double x2 = randomUniform2(0, 1);
		double Pi = 3.14159265358979323846;

		double x;
		x = std::cos(2 * Pi*x1)*std::sqrt(-2 * std::log10(x2));
		return x;
	}
	double randomUniform()
	{
		return rand() / (double)RAND_MAX;

	}
	double randomUniform2(double a, double b)
	{
		return (b - a)*randomUniform() + a;
	}
	std::vector<float> drift(std::vector<Rate> &forwardRate, Matrix &volMat, std::vector<QuantLib::Date> &dates, int indicetemps){

		int n = forwardRate.size();
		Matrix M(n, n);
		std::vector<float> R(n);
		int i = 0;
		int k = 0;
		for (i = 0; i < n; i++){
			M[i][indicetemps] = volMat[i][indicetemps] * sommedrift(forwardRate, volMat, dates, k, i, indicetemps);
			k = indicetemps + 1;

			R[i] = M[i][indicetemps];
		}

		return R;
	}
	float sommedrift(std::vector<Rate> &forwardRate, Matrix &volMat, std::vector<QuantLib::Date> &dates, int initial, int final, int indice){

		float R = 0;
		int n = forwardRate.size();
		std::vector<Time> D(n);
		int i = 0;
		std::vector<float> delta(n);
		Matrix matforwardR(n, n);
		//Remarque : indice =0 car on connait les forwards rates � l'instant initial t=0 seulement 

		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
		}

		for (int j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
		}
		delta[n - 1] = delta[n - 2];


		for (i = initial; i < final; i++){

			matforwardR[i][indice] = forwardRate[i];
			R = R + ((delta[i] * matforwardR[i][indice] * volMat[i][indice]) / (1 + delta[i] * forwardRate[i]));
		}

		return R;
	}

	//Attributs
	//std::vector <Date > date2;
	//std::vector<Rate> fwdvect;
	//Matrix VOLMATR1;
};