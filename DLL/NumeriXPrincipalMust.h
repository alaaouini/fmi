#pragma once
#include "tinyxml.h"
#include <iostream>


class NumeriXPrincipalMust
{
public:
	NumeriXPrincipalMust();
	~NumeriXPrincipalMust();

	void setComponent(TiXmlDocument doc, string namePrincipal, string tradeId);

	//*** M�thode d'amortissement

	//void AmortNotional(QuantLib::Date startDate, QuantLib::Date endDate, int periodicite, string formule);

	//attributs
	std::vector<double> nominals;
	double nominal;
	string principalCcy;
	string tauxAmort;
	float amortRate;

	string amortYorN;
	string freq;
	//Frequency freqQ;

	string startDate;
	//QuantLib::Date startDateQ;

	string endDate;
	//QuantLib::Date endDateQ;
	string formuleAmort;

};

