#pragma once
#include "tinyxml.h"
#include <iostream>
#include "ComponentMust.h"
#include "MatriceMust.h"

class NumerixIndexMust : public ComponentMust
{
public:
	NumerixIndexMust();
	~NumerixIndexMust();

	void setComponent(TiXmlDocument doc, string nameIndex, string tradeId);

	boost::shared_ptr<IborIndex> ConstructIndex(Handle<QuantLib::YieldTermStructure> forwardingTermStructure);
	//attributs

	//string convertIndexName(string mustNameString);

	//QuantLib::Currency currencyConvert(string currencyName);


	//// si on a un spread constant =>remplir la matrice de spread par une seule ligne
	//void SetMatrixSpread(QuantLib::Date startDate, QuantLib::Date endDate, vector<Spread> spreads);

public:
	string IndexCcy;
	string IndexFreq;
	string IndexBasis;
	string IndexTerm;
	string IndexIndex;
	int IndexTermQ;
	float spread;

	string spreadString; // formule du spread
	vector<float> spreads; // vacteur contenant les spreads
	std::vector<std::tuple<QuantLib::Date, QuantLib::Date, Spread>> matrixSpread; // matrice avec les dates et les spreads

	string numerixIndexFrequency;
private:
	string eff_numerix_convert_frequency(char freq_char) const;


};

