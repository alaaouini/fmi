#pragma once
#include <ql/quantlib.hpp>
#include "MustCapFloor.h"
#include "MustSwaption.h"
#include "MustVanilleSwap.h"



class EFFICIEBCYLIBDLL_API RiskParameter 
{

public:
	RiskParameter(){}
public: 
		Real delta = 0;
		Real gamma = 0;
		Real vega = 0;
		Real theta = 0;
		QuantLib::Date today;
};