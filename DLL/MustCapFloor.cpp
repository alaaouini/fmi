#pragma once
#include <ql/quantlib.hpp>
#include "MustCapFloor.h"

#include "CurveDataNumerix.h"
#include "NumerixUtilities.h"
#include "NumeriX/Core/Basis.h"
#include "NumeriX/Pro/Structuring/Event.h"
#include "NumeriX/Pro/Models/HW1F.h" 


MustCapFloor::MustCapFloor(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdldoc, string trId) : EfficiencyLibProduct(valuationDate, typeProd, paths, modelProd, hdldoc, trId) {
	modelProduct = modelProd;
	//cVol = new CapFloorVolatility();
	//capletsVolatilies = (cVol.buildFlatCurveVol(cVol));

}
void MustCapFloor::setComponentsQuantLib() {
	this->setComponentsForCapFloor();
	//index
	MustCapFloor::myIndex = ComponentIndexMust.ConstructIndex(forwardingTermStructure);

	////FloatingLeg
	SwapFloatingLeg floating_leg;
	floating_leg.setComponent(MustCapFloor::myIndex, floatingLegObj.basisQ, floatingLegObj.freqQ);


	cout << " MustCapFloor::setComponentsQuantLib() startDateObj.dateQ: " << startDateObj.dateQ << endl;
	cout << " MustCapFloor::setComponentsQuantLib() endDateObj.dateQ: " << endDateObj.dateQ << endl;
	cout << " MustCapFloor::setComponentsQuantLib() floatingLegObj.freqQ: " << floatingLegObj.freqQ << endl;

	Rates = floating_leg.vectorAdaptFreq(fixedRateObj.matrixRate, startDateObj.dateQ, endDateObj.dateQ, floatingLegObj.freqQ);


	if (ComponentIndexMust.spreads.size()<1) //condition to check the Spread
	{
		MustCapFloor::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
	}
	else
	{
		MustCapFloor::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal, ComponentIndexMust.matrixSpread);
	}
}

void MustCapFloor::makeCapFloor(Handle<QuantLib::YieldTermStructure> forwardingTermStructure) {

	setComponentsQuantLib();

	switch (modelProduct) {
	case EfficiencyProduct::EfficiencyModelProduct::EFFICIENCY_LMM:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_BLACKFT:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_HULLWHITE:
	{
		//	capFloorType.
		if (capFloorType.valeur == "CAP")
			MustCapFloor::capfloor = boost::shared_ptr<QuantLib::CapFloor>(new Cap(floatingLeg, Rates));
		else
			MustCapFloor::capfloor = boost::shared_ptr<QuantLib::CapFloor>(new Floor(floatingLeg, Rates));
	}
	break;

	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_BLACKFT:
	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_HW:
	{

		cout << " MustCapFloor::setComponentsQuantLib Rates.size():" << Rates.size() << endl;
		numerixRates.clear();
		for (int i = 0; i < Rates.size(); i++)
		{
			numerixRates.push_back((double)Rates[i]);

			cout << " MustCapFloor::setComponentsQuantLib Rates[" << i << "]=" << Rates[i] << endl;
		}
	}
	break;
	}







}

Real MustCapFloor::price() {
	std::cout << "--->MustCapFloor::price startDateObj.dateQ:" << startDateObj.dateQ << endl;
	std::cout << "--->MustCapFloor::price endDateObj.dateQ:" << endDateObj.dateQ << endl;
	std::cout << "--->MustCapFloor::price floatingLegObj.freqQ:" << floatingLegObj.freqQ << endl;
	std::cout << "--->MustCapFloor::price today:" << today << endl;
	switch (modelProduct) {
	case EfficiencyProduct::EfficiencyModelProduct::EFFICIENCY_LMM:
	{
		LMMEfficiencyEngine* lmmEngine = new  LMMEfficiencyEngine(startDateObj.dateQ, endDateObj.dateQ, floatingLegObj.freqQ);
		//TO DO
		//lmmEngine->lmmEngine(startDateObj.dateQ, endDateObj.dateQ, floatingLegObj.freqQ);
		//TO DO
		CapFloorEfficiencyPricer myPricer;
		myPricer.SetEngine(lmmEngine);

		return myPricer.Price(Rates[0], floatingLegObj.freqQ, startDateObj.dateQ, endDateObj.dateQ, startDateObj.dateQ);

		//Real pricefinal = 0.0;
		//int nbdesimulation = 1000;
		//Real npv;
		//for (int i = 0; i < nbdesimulation; i++){
		//	Matrix matForward = lmmEngine->LMMmatforward();
		//	npv = CalculPrixRelatifCap(Rates[0], matForward, lmmEngine.date2, startDateObj.dateQ, endDateObj.dateQ, startDateObj.dateQ);
		//	pricefinal = pricefinal + npv / nbdesimulation;
		//}
		//return pricefinal;
		/*	return 0.0;*/
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
	{
		boost::shared_ptr<AnalyticCapFloorEngine> LmEngine = setEngineQuantLibLMM();

		capfloor->setPricingEngine(LmEngine);
		Real npv = capfloor->NPV();
		vega = capfloor->result<Real>("vega");

		return npv;
		//return 0;
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_BLACKFT:
	{
		//TO DO

		boost::shared_ptr<BlackCapFloorEngine> strippedVolEngine = setEngineQuantLibBalckFT();
		capfloor->setPricingEngine(strippedVolEngine);
		Real npv = capfloor->NPV();
		Real shift = 0.001;
		/* delta = computeDelta(shift);
		vega = computeVega();
		gamma = computeGamma(shift);
		*/
		return npv;
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_HULLWHITE:
	{
		//TO DO
		return 0;
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_BLACKFT:
	{
		std::cout.unsetf(std::ios::floatfield);
		std::cout.precision(10);

		boost::shared_ptr<NumeriX::Pro::CapFloorAnalytic> capFloorAnalytic = MustCapFloor::setEngineNumerixBlackFT();

		delta = MustCapFloor::NumeriXPrincipalMust.nominal*capFloorAnalytic->getDelta();
		gamma = MustCapFloor::NumeriXPrincipalMust.nominal*capFloorAnalytic->getGamma();
		vega = MustCapFloor::NumeriXPrincipalMust.nominal*capFloorAnalytic->getVega();
		double npv = MustCapFloor::NumeriXPrincipalMust.nominal*capFloorAnalytic->getPrice();

		cout << " MustCapFloor::price() npv:=" << npv << endl;

		return npv;
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_HW:
	{
		std::cout.unsetf(std::ios::floatfield);
		std::cout.precision(10);


		this->setComponentsForNumerixCapFloor();
		this->numerixCapFloor = setEngineNumerixHW();
		double npv = numerixCapFloor->NPV();

		cout << "MustCapFloor::price() npv:=" << npv << endl;
		return npv;
	}
	break;
	default://just to do something
	{
		return 0;
	}
	break;
	}
}
//Real MustCapFloor::price2(LMMEfficiencyEngine engine)
//{
//	/*Real pricefinal = 0.0;
//	int nbdesimulation = 1000;
//	Real npv;
//	for (int i = 0; i < nbdesimulation; i++){
//		Matrix matForward = engine.LMMmatforward();
//		npv = CalculPrixRelatifCap(Rates[0], matForward, engine.date2, startDateObj.dateQ, endDateObj.dateQ, startDateObj.dateQ);
//		pricefinal = pricefinal + npv / nbdesimulation;
//	}
//	return pricefinal;*/
//	return 0.0;
//}

boost::shared_ptr<BlackCapFloorEngine> MustCapFloor::setEngineQuantLibBalckFT() {

	CurveData cData;
	// Fill out with some sample market data
	cData.sampleMktData(cData);

	// Build a curve linked to this market data
	boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));

	// Link the curve to the term structure
	this->forwardingTermStructure.linkTo(ocurve);
	//Date today(6, October, 2014);
	Settings::instance().evaluationDate() = today;

	//Date todaysDate = Date(8, Apr, 2015);
	//capletsVolatilies = buildCurveOptionletAtmVol(cVol);
	//capletsVolatilies = buildOptionletSurfaceVol(cVol);
	//capletsVolatilies = buildOptionletCurveVol(cVol);

	cVol.termStructure.linkTo(ocurve);
	capletsVolatilies = (cVol.buildFlatCurveVol(cVol));

	//capVol->enableExtrapolation();
	return boost::shared_ptr<BlackCapFloorEngine>(new
		BlackCapFloorEngine(forwardingTermStructure, capletsVolatilies));
}
boost::shared_ptr<AnalyticCapFloorEngine> MustCapFloor::setEngineQuantLibLMM() {
	CurveData cData;
	// Fill out with some sample market data
	cData.sampleMktData(cData);
	//Date today(10, October, 2014);
	//Settings::instance().evaluationDate() = today;
	// Build a curve linked to this market data
	boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));

	// Link the curve to the term structure
	boost::shared_ptr<IborIndex> index = boost::shared_ptr<IborIndex>(new Euribor6M(forwardingTermStructure));
	QuantLib::Date todaysDate =
		myIndex->fixingCalendar().adjust(today);
	Settings::instance().evaluationDate() = todaysDate;

	this->forwardingTermStructure.linkTo(ocurve);

	const Size size = floatingLeg.size();
	boost::shared_ptr<LiborForwardModelProcess> process(
		new LiborForwardModelProcess(size, myIndex));

	//// set-up the model
	///*	const Real a = 0.02;
	//const Real b = 0.12;
	//const Real c = 0.1;
	//const Real d = 0.01;*/

	const Real a = 0.025;
	const Real b = 0.12;
	const Real c = 0.1;
	const Real d = 0.01;

	boost::shared_ptr<LmVolatilityModel> volaModel(
		new LmLinearExponentialVolatilityModel(process->fixingTimes(), a, b, c, d));

	boost::shared_ptr<LmCorrelationModel> corrModel(
		new LmLinearExponentialCorrelationModel(size, 0.1, 0.1));

	boost::shared_ptr<LiborForwardModel> liborModel(
		new LiborForwardModel(process, volaModel, corrModel));

	return	boost::shared_ptr<AnalyticCapFloorEngine>(
		new AnalyticCapFloorEngine(liborModel, forwardingTermStructure));
}
void MustCapFloor::setEngineQuantLibHullWhite() {}

boost::shared_ptr<NumeriX::Pro::CapFloorAnalytic> MustCapFloor::setEngineNumerixBlackFT()
{
 	std::cout.unsetf(std::ios::floatfield);
	std::cout.precision(10);


	this->setComponentsForNumerixCapFloor();

	double nominal = MustCapFloor::NumeriXPrincipalMust.nominal;

	NumeriX::Pro::Date startDate = NumeriX::Pro::Date(MustCapFloor::NumerixstartDateObj.dateReference);

	NumeriX::Pro::Date endDate = NumeriX::Pro::Date(MustCapFloor::NumerixmaturityObj.dateReference);

	NumeriX::Pro::CapFloor::Volatility sigma(0.16);

	loadGlobalConventions(nxApp);
	Conventions convention = "EUR_Swap_AnnMoney";
	cout << " MustCapFloor::price() nominal:" << nominal << endl;
	cout << " MustCapFloor::price() startDate:" << MustCapFloor::NumerixstartDateObj.dateString << endl;
	cout << " MustCapFloor::price() endDate:" << MustCapFloor::NumerixmaturityObj.dateString << endl;


	NumeriX::Pro::Date todayNumerix = NumeriX::Pro::Date(today.serialNumber());


	cout << " MustCapFloor::price() todayNumerix:" << today << endl;

	Tenor startTenor = getTenorDurationInDays(todayNumerix, startDate);
	Tenor endTenor = getTenorDurationInDays(todayNumerix, endDate);

	cout << " MustCapFloor::price() startTenor:" << startTenor << endl;
	cout << " MustCapFloor::price() endTenor:" << endTenor << endl;

	NumeriX::Pro::CapFloor::OptionalArgsCustom2 optargs2;
	NumeriX::Pro::Basis basis(MustCapFloor::NumerixfloatingLeg.CashFlowBasis);
	optargs2.setBasis(basis);
	string numerixFloatingLegFreq = MustCapFloor::NumerixfloatingLeg.freqQ;
	optargs2.setFrequency(numerixFloatingLegFreq);
	cout << " MustCapFloor::price() numerixFloatingLegFreq:" << numerixFloatingLegFreq << endl;
	cout << " MustCapFloor::price() basis:" << MustCapFloor::NumerixfloatingLeg.CashFlowBasis << endl;
	NumeriX::Pro::CapFloor::OptionType flavor;
	if (capFloorType.valeur == "CAP")
		flavor = NumeriX::Pro::CapFloor::CALL;
	else
		flavor = NumeriX::Pro::CapFloor::PUT;


	for (int i = 0; i < numerixRates.size(); i++)
	{
		cout << " MustCapFloor::price() numerixRates[" << i << "]=" << numerixRates[i] << endl;
	}
	cout << " MustCapFloor::price() volatility:" << 0.16 << endl;

	NumeriX::Pro::CapFloor numerixCapFloorInstrument(nxApp, currency, flavor, startTenor, endTenor, numerixRates, sigma, convention, optargs2);

	

	CurveDataNumerix curveDataNumerix(nxApp);

	YieldCurve yieldCurve = curveDataNumerix.buildCurve(curveDataNumerix, todayNumerix);


	const boost::shared_ptr<NumeriX::Pro::CapFloorAnalytic> result = boost::shared_ptr<NumeriX::Pro::CapFloorAnalytic>(new NumeriX::Pro::CapFloorAnalytic(nxApp, numerixCapFloorInstrument, yieldCurve, todayNumerix/*, sigma*/));
	/*const NumeriX::Pro::CapFloorAnalytic::PaymentLog* payment = result->getPaymentLog();

	const std::vector<NumeriX::Pro::Date> accrualStartDates = payment->getAccrualStart();
	const std::vector<NumeriX::Pro::Date> accrualEndDates = payment->getAccrualEnd();
	const std::vector<NumeriX::Pro::Date> fixingDates = payment->getFixingDate();

	for (int i = 0; i < accrualStartDates.size(); i++)
	{
		NumeriX::Pro::Date d = accrualStartDates[i];
		cout << "MustCapFloor::setEngineNumerixBlackFT() accrualStartDates[" << i << "]=" << d << endl;
	}

	for (int i = 0; i < accrualEndDates.size(); i++)
	{
		NumeriX::Pro::Date d = accrualEndDates[i];
		cout << "MustCapFloor::setEngineNumerixBlackFT() accrualEndDates[" << i << "]=" << d << endl;
	}

	for (int i = 0; i < fixingDates.size(); i++)
	{
		NumeriX::Pro::Date d = fixingDates[i];
		cout << "MustCapFloor::setEngineNumerixBlackFT() fixingDates[" << i << "]=" << d << endl;
	}*/

	return result;
}



boost::shared_ptr<NumerixCapFloor> MustCapFloor::setEngineNumerixHW()
{
	boost::shared_ptr<NumerixCapFloor> result;
	string numerixFloatingLegFreq = MustCapFloor::NumerixfloatingLeg.freqQ;
	cout << " MustCapFloor::price() numerixFloatingLegFreq:" << numerixFloatingLegFreq << endl;
	double nominal = MustCapFloor::NumeriXPrincipalMust.nominal;

	cout << " MustCapFloor::price() numerixFloatingLegFreq:" << numerixFloatingLegFreq << endl;
	cout << " MustCapFloor::price() nominal:" << nominal << endl;

	cout << " MustCapFloor::price() startDate:" << MustCapFloor::NumerixstartDateObj.dateString << endl;
	cout << " MustCapFloor::price() endDate:" << MustCapFloor::NumerixmaturityObj.dateString << endl;

	NumeriX::Pro::Date todayNumerix = NumeriX::Pro::Date(today.serialNumber());

	cout << " MustCapFloor::price() todayNumerix:" << today << endl;

	NumeriX::Pro::Date startDate = NumeriX::Pro::Date(MustCapFloor::NumerixstartDateObj.dateReference);

	NumeriX::Pro::Date endDate = NumeriX::Pro::Date(MustCapFloor::NumerixmaturityObj.dateReference);
	

	string cashFlowBasis = MustCapFloor::NumerixfloatingLeg.CashFlowBasis;

	cout << " MustCapFloor::price() cashFlowBasis:" << cashFlowBasis << endl;

	Tenor startTenor = getTenorDurationInDays(todayNumerix, startDate);
	Tenor endTenor = getTenorDurationInDays(todayNumerix, endDate);
	bool isCap = true;
	if (capFloorType.valeur != "CAP")
		isCap = false;

	cout << " MustCapFloor::price() isCap:" << isCap << endl;

	result = boost::shared_ptr<NumerixCapFloor>(new NumerixCapFloor(nxApp, nominal, numerixFloatingLegFreq, isCap, todayNumerix, startDate, endDate, numerixRates, cashFlowBasis));

	CurveDataNumerix curveDataNumerix(nxApp);
	double meanReversion = 0.020275;
	double hwVolatility = 0.0001;

	cout << " MustCapFloor::price() meanReversion:" << meanReversion << endl;
	cout << " MustCapFloor::price() hwVolatility:" << hwVolatility << endl;

	YieldCurve yieldCurve = curveDataNumerix.buildCurve(curveDataNumerix, todayNumerix);

	HW1F numerixModel(nxApp, currency, yieldCurve, meanReversion, hwVolatility);


	result->setEngine(numerixModel);
	return result;

}


Real MustCapFloor::computeDelta(Real shift)
{

	switch (modelProduct)
	{
	case EfficiencyProduct::EfficiencyModelProduct::EFFICIENCY_LMM:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_BLACKFT:
	case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_HULLWHITE:
	{
		CurveData cData1;
		// Fill out with some sample market data
		cData1.sampleMktData(cData1);
		cData1.bumpCurve(cData1, shift);
		// Build a curve linked to this market data
		boost::shared_ptr<YieldTermStructure> ocurve = (cData1.buildCurve(cData1));
		this->forwardingTermStructure.linkTo(ocurve);
		Real npv1 = this->capfloor->NPV();

		CurveData cData2;
		cData2.sampleMktData(cData2);
		cData2.bumpCurve(cData2, -shift);
		// Build a curve linked to this market data
		ocurve = (cData2.buildCurve(cData2));
		this->forwardingTermStructure.linkTo(ocurve);
		Real npv2 = this->capfloor->NPV();
		return (npv1 - npv2) / (2 * shift);

	}
	break;

	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_BLACKFT:
	{
		return delta;
	}
	break;
	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_HW:
	{
		NumeriX::Pro::Date todayNumerix = NumeriX::Pro::Date(today.serialNumber());

		double meanReversion = 0.020275;
		double hwVolatility = 0.0001;


		CurveDataNumerix curveDataNumerix1(nxApp);
		curveDataNumerix1.bumpCurve(curveDataNumerix1, shift);

		YieldCurve yieldCurve1 = curveDataNumerix1.buildCurve(curveDataNumerix1, todayNumerix);


		HW1F numerixModel1(nxApp, currency, yieldCurve1, meanReversion, hwVolatility);


		numerixCapFloor->setEngine(numerixModel1);
		double npv1 = numerixCapFloor->NPV();


		CurveDataNumerix curveDataNumerix2(nxApp);
		curveDataNumerix2.bumpCurve(curveDataNumerix2, (-1)*shift);
		YieldCurve yieldCurve2 = curveDataNumerix2.buildCurve(curveDataNumerix2, todayNumerix);

		HW1F numerixModel2(nxApp, currency, yieldCurve2, meanReversion, hwVolatility);


		numerixCapFloor->setEngine(numerixModel2);
		double npv2 = numerixCapFloor->NPV();

		return (npv1 - npv2) / (2 * shift);
	}
	break;
	}
	return -1;
}
Real MustCapFloor::computeGamma(Real shift) {

	switch (modelProduct)
	{
	case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_BLACKFT:
	{
		return delta;
	}
	break;
	default:
	{
		Real delta1 = computeDelta(shift);
		Real delta2 = computeDelta((-1)*shift);
		return (delta1 - delta2) / (2 * shift);
	}
	break;
	}
	return -1;

}
Real MustCapFloor::computeVega() { return capfloor->result<Real>("vega"); }
Real MustCapFloor::computeTheta() { return 0; }