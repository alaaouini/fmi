#include "NumerixCashFlowMust.h"



NumerixCashFlowMust::NumerixCashFlowMust()
{
}


NumerixCashFlowMust::~NumerixCashFlowMust()
{
}



void NumerixCashFlowMust::setComponent(TiXmlDocument doc, string nameLeg, string tradeId)
{
	cout << "NumerixCashFlowMust::setComponent: nameLeg" << nameLeg << endl;
	cout << "NumerixCashFlowMust::setComponent: tradeId" << tradeId << endl;
	TiXmlHandle docMUSTHandle(&doc);
	TiXmlElement *Component;
	TiXmlElement *Trade = docMUSTHandle.FirstChild("BODY").FirstChild("TRADELIST").FirstChild("MUST_TR").ToElement();

	while (Trade)
	{
		if (strcmp(Trade->FirstChild("TradeId")->ToElement()->GetText(), tradeId.c_str()) == 0)
		{
			Component = Trade->FirstChild("MPTradeData")->FirstChild("MPTRDATA")->FirstChild("MPTrDataXML")->FirstChild("MPTRDATAXML")->FirstChild("STRUCTURED_INSTRUMENT")->
				FirstChild("COMPONENT_LIST")->FirstChild("COMPONENT_CASHFLOW")->ToElement();
			while (Component)
			{
				if (strcmp(Component->Value(), "COMPONENT_CASHFLOW") == 0)
				{
					if (strcmp(Component->FirstChild("NAME")->ToElement()->GetText(), nameLeg.c_str()) == 0)
					{
						CashFlowCcy = Component->FirstChild("PAYOFF_CCY")->FirstChild("CCY")->ToElement()->GetText();

						CashFlowFreq = Component->FirstChild("PAY_SCHED")->FirstChild("BASIC_SCHED")->FirstChild("SCHED_DEF")->FirstChild("FREQ")->ToElement()->GetText();
						
						cout << "NumerixCashFlowMust::setComponent CashFlowFreq:" << CashFlowFreq << endl;
						
						freqQ = this->eff_numerix_convert_frequency(CashFlowFreq.c_str()[0]);

						CashFlowBasis = Component->FirstChild("BASIS_DEF")->FirstChild("BASIS")->ToElement()->GetText();
						//basisQ = this->eff_convert_basis(CashFlowBasis);


					}
				}
				Component = Component->NextSiblingElement();
			}
		}
		Trade = Trade->NextSiblingElement();
	}


}


string NumerixCashFlowMust::eff_numerix_convert_frequency(char freq_char) const
{
	//cette fonction fait la conversion 
	//de la frequence de type string lue de XML de MUST
	string result;

	switch (freq_char)
	{
	case 'A': result = "1Y";
		break;
	case 'S': result = "6M";
		break;
	case 'Q': result = "3M";
		break;
	case 'M':result = "1M";
		break;
	case 'W':result = "1W";
		break;
	default:
		result = "";
	}

	return result; // retourne frequency de type Numerix
}