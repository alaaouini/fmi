#pragma once
//#include"Engine.h"
#include <ql/quantlib.hpp>
#include "DllHeader.h"

using namespace std;
using namespace QuantLib;
using namespace boost;

class EFFICIEBCYLIBDLL_API CapFloorEfficiencyEngine
{
public:
	virtual ~CapFloorEfficiencyEngine()
	{

	}

	virtual Matrix LMMmatforward()=0;

};

