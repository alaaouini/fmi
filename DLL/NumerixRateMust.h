#pragma once
#include "ComponentMust.h"
#include "tinyxml.h"
#include <string>
#include "MatriceMust.h"

public class NumerixRateMust: public ComponentMust
{
public:
	NumerixRateMust();
	~NumerixRateMust();

	void setComponent(TiXmlDocument doc, string nameRate, string tradeId);

	//Attributs
	string rateString;


	vector<float>  rates;
	float rate;
	vector<std::tuple<QuantLib::Date, QuantLib::Date, Rate>> matrixRate;


	//m�thode
	bool check_matrix(string formule);


	// si on a un taux constant =>remplir la matrice de taux par une seule ligne
	//void SetMatrixRate(QuantLib::Date startDate, QuantLib::Date endDate, vector<Rate> rates);
};

