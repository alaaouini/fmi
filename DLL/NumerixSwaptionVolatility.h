#pragma once

#include "NumeriX/Pro/Application.h"
#include "NumeriX/Pro/ApplicationMatrix.h"
#include "NumeriX/Core/Currency.h"
#include "NumeriX/Core/Date.h"
#include "NumeriX/Pro/MarketInformation/SwaptionVolSurface.h"

#include <vector>
using namespace std;


class NumerixSwaptionVolatility {
public: 
	NumerixSwaptionVolatility(NumeriX::Pro::Application& app, NumeriX::Core::Date& nowDate_,NumeriX::Core::Currency& currency_):
		nxApp(app), nowDate(nowDate_), currency(currency_)
    {
		NumeriX::Pro::SwaptionVolSurface::OptionalArgsCal opt;
		NumeriX::Pro::ApplicationMatrix mat(newApplicationMatrix(createOptionTenors(), createSwapTenors(), createEURSwaptionVols()));
		swaption_vols_s = NumeriX::Pro::SwaptionVolSurface(nxApp, nowDate, currency, mat, NumeriX::Pro::SwaptionVolSurface::CUBIC_SPLINE, opt.setNoticePeriod("2BD").setFixingCalendar("EUTA"));
    }

	NumeriX::Pro::SwaptionVolSurface getSwaptionVolSurface()
	{
		return swaption_vols_s;
	}

private:
	NumeriX::Pro::Application nxApp;
	NumeriX::Core::Date nowDate;
	NumeriX::Core::Currency currency;
	NumeriX::Pro::SwaptionVolSurface swaption_vols_s;
	vector<string> createOptionTenors()
	{
		const char *data[] = { "1M","2M","3M","6M","1Y","2Y","3Y","5Y","7Y","10Y","15Y","20Y","30Y" };
		vector<string> dataVector;
		int length = sizeof(data) / sizeof(*data);
		for (int i = 0; i<length; i++)
			dataVector.push_back(data[i]);

		return dataVector;
	}

	vector<string> createSwapTenors()
	{
		const char *data[] = { "1M","3M","6M","1Y","2Y","5Y","7Y","10Y","15Y","20Y","30Y" };
		vector<string> dataVector;
		int length = sizeof(data) / sizeof(*data);
		for (int i = 0; i<length; i++)
			dataVector.push_back(data[i]);

		return dataVector;
	}

	vector< vector<double> > createEURSwaptionVols()
	{
		vector< vector<double> > EURSwaptionVols;
		{
			double row[] = {
				0.19765,0.183525,0.1671,
				0.1557,0.14587,0.138,
				0.1305,0.12447,0.11857,
				0.11325,0.10695 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.19275, 0.1803125, 0.164875,
				0.15361, 0.14433, 0.1365,
				0.12966, 0.123787, 0.118275,
				0.113225, 0.106825 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.18785, 0.1771, 0.16265,
				0.1515, 0.1428, 0.135,
				0.12883, 0.1231, 0.11797,
				0.1132, 0.1067 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.183575, 0.169525, 0.1571,
				0.145925, 0.13765, 0.13133,
				0.12587, 0.1207, 0.11635,
				0.112075, 0.105675 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.179375, 0.1645, 0.1525875,
				0.1428, 0.13497, 0.12917,
				0.1241, 0.1195, 0.115337,
				0.1113625, 0.1054125 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.175175, 0.159475, 0.148075,
				0.139675, 0.13222, 0.127,
				0.12232, 0.1183, 0.114325,
				0.11065, 0.10515 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.1553, 0.1433750, 0.1353250,
				0.1295, 0.12455, 0.12067,
				0.1167, 0.113675, 0.110825,
				0.10805,0.102975 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.145925, 0.134925, 0.127975,
				0.12277, 0.1191, 0.116,
				0.1127, 0.1107, 0.10815,
				0.10605, 0.1003975 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.137725, 0.12815, 0.12225,
				0.11765, 0.11442, 0.1123,
				0.10975, 0.1079, 0.105825,
				0.104075, 0.09844 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.131375, 0.122875, 0.1177,
				0.113325, 0.11067, 0.10866,
				0.10647, 0.10503, 0.103425,
				0.10195, 0.09689 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.12613, 0.11861, 0.11371,
				0.10955, 0.10695, 0.10517,
				0.10336, 0.109431, 0.1004925,
				0.0992288, 0.0944825 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.1209, 0.11435, 0.109725,
				0.10577, 0.103225, 0.101667,
				0.1025, 0.0988575, 0.09756,
				0.0965075, 0.092075 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		{
			double row[] = {
				0.11662, 0.11025, 0.10589,
				0.10222, 0.089, 0.098766,
				0.09876, 0.0960429, 0.0950127,
				0.0941221, 0.0900605 };
			EURSwaptionVols.push_back(vector<double>(row, row + sizeof(row) / sizeof(*row)));
		}
		return EURSwaptionVols;
	}


	NumeriX::Pro::ApplicationMatrix newApplicationMatrix(const vector<string> &row_names, const vector<string> &col_names, const vector< vector<double> > &values)
	{
		NumeriX::Pro::ApplicationMatrix mat;
		size_t n_rows = row_names.size();
		size_t n_cols = col_names.size();
		size_t i;
		for (i = 0; i < n_rows; i++)
			mat.addRow(row_names[i]);
		for (i = 0; i < n_cols; i++)
			mat.addColumn(col_names[i]);
		if (values.size() != n_rows)
			throw runtime_error("Different number of rows and headings");
		for (i = 0; i < n_rows; i++)
		{
			if (values[i].size() != n_cols)
				throw runtime_error("Different number of columns and headings");
			for (size_t j = 0; j < n_cols; j++)
				mat.set(i, j, values[i][j]);
		}
		return mat;
	}


};