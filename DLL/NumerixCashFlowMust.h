#pragma once
#include "ComponentMust.h"
#include "tinyxml.h"
#include <string>


class NumerixCashFlowMust
{
public:
	NumerixCashFlowMust();
	~NumerixCashFlowMust();
	void setComponent(TiXmlDocument doc, string nameLeg, string tradeId);

	string eff_numerix_convert_frequency(char freq_char) const;

	//attributs
	string CashFlowCcy;
	string CashFlowFreq;
	string CashFlowBasis;
	string freqQ; // type Numerix
	//DayCounter basisQ; // type QuantLib
};

