#pragma once
#include "ComponentMust.h"
#include "tinyxml.h"
#include <string>


using namespace std;
using namespace QuantLib;

class NumerixDateMust : public ComponentMust
{
public:
	NumerixDateMust();
	~NumerixDateMust();

	void setComponent(TiXmlDocument doc, string nameDate, string tradeId);

	//attributs
	string dateString;
	int dateReference;
};

