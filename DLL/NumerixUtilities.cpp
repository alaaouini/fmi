#include "NumerixUtilities.h"
#include <sstream>
using namespace std;
Tenor getTenorDurationInDays(NumeriX::Pro::Date& startDate, NumeriX::Pro::Date& endDate)
{
	int d1 = startDate.getDays();
	int d2 = endDate.getDays();
	int difference = d2 - d1;
	ostringstream differenceStringStream;
	differenceStringStream << difference;
	string differenceStrInDays = differenceStringStream.str() + "d";
	Tenor result(differenceStrInDays);
	return result;
}