#pragma once

//#include <ql/quantlib.hpp>
/*#include <tuple*/

#include "NumeriX/Pro/Application.h"
#include "NumeriX/Pro/ApplicationException.h"

#include "NumeriX/Pro/Basis.h"
#include "NumeriX/Pro/MarketInformation/Calendar.h"
#include "NumeriX/Pro/Instruments/CashDeposit.h"
#include "NumeriX/Pro/Structuring/ComponentsInstrument.h"
#include "NumeriX/Pro/Models/DeterministicIR.h"
#include "NumeriX/Pro/MarketInformation/Fixings.h"
#include "NumeriX/Pro/Structuring/FloatingIndex.h"
#include "NumeriX/Pro/Models/HW1F.h"
#include "NumeriX/Pro/Instruments/InstrumentCollection.h"
#include "NumeriX/Pro/Structuring/InterestSchedule.h"
#include "NumeriX/Pro/Instruments/IRFuture.h"
#include "NumeriX/Pro/Kernel/KernelPricer.h"
#include "NumeriX/Pro/Structuring/Script.h"
#include "NumeriX/Pro/Instruments/Swaption.h"
#include "NumeriX/Pro/Instruments/VanillaIRSwap.h"
#include "NumeriX/Pro/MarketInformation/YieldCurve.h"


#include <iostream>

#include "CurveDataNumerix.h"
using namespace NumeriX::Pro;
using namespace std;


class NumerixSwap: public NumerixMarketData
{
public:
	double spread;
	double notional;
	double strike;
	NumeriX::Pro::Basis fixedBasis;
	NumeriX::Pro::Basis floatingBasis;
	
			
	NumerixSwap(NumeriX::Pro::Application& app, double Spread, double Notional,std::string numerixIndexFrequency ,std::string numerixFixedFrequency, std::string numerixFloatingFrequency,double Strike, std::string IndexBasis_,std::string FixedBasis_, std::string FloatingBasis_, int inttodayDate, int intstartDate, int intmaturity);
	
	void setEngine(CurveDataNumerix& curveData);
	double NPV();
	double NPV_FixedLeg();
	double NPV_FloatingLeg();
	~NumerixSwap();
	NumerixSwap();

private:
	NumeriX::Pro::Basis indexBasis;
	std::string indexFrequency;

	std::string fixedFrequency;
	std::string floatingFrequency;
	NumeriX::Pro::Date todayDate;
	NumeriX::Pro::Date startDate;
	NumeriX::Pro::Date maturity;
	DeterministicIR model;
	ComponentsInstrument componentsInstrument;
	NumeriX::Pro::Application nxApp;
	
	vector < std:: string > createScript();
	double npv;
	double npv_fixed_leg;
	double npv_floating_leg;
};

