#pragma once
#include "SwaptionEfficiencyEngine.h"
#include <ql/quantlib.hpp>
#include "DllHeader.h"

using namespace std;
using namespace QuantLib;
using namespace boost;

class EFFICIEBCYLIBDLL_API SwaptionEfficiencyPricer
{
public:
	SwaptionEfficiencyPricer();
	SwaptionEfficiencyEngine* myEngine;
	void SetEngine(SwaptionEfficiencyEngine* MyEngine){ myEngine = MyEngine; }

	Real Price(float strike, Frequency Frequency, QuantLib::Date debut, QuantLib::Date fin,QuantLib::Date datedecalcul){
		//Matrix M;
		std::vector <QuantLib::Date >  dates = vectorDates(debut, fin, Frequency);
		int nbdesimulation = 1000;
		float price;
		Real pricefinal = 0;

		int n = dates.size();
		int i = 0;
		int j = 0;
		int initial = 0;
		int finale = 0;
		int datecalcul = 0;


		for (i = 0; i < n; i++){

			if (dates[i] == debut) {
				initial = i;

			}

			if (dates[i] == fin) {
				finale = i;

			}

			if (dates[i] == datedecalcul){

				datecalcul = i;

			}
		}
		
		for (int i = 0; i < nbdesimulation; i++){
			Matrix matForward = myEngine->LMMSwaptionmatforward();
			/*Matrix matForward1 = matforwardLMM(forwardRate, volMat, dates);*/
			price = CalculPrixRelatifSwaptionEuroPayeuse2(strike, matForward, dates, debut, fin, datedecalcul);
			pricefinal = pricefinal + price / nbdesimulation*calculdeA(datecalcul, initial, finale, matForward, dates);

		}

		


		return pricefinal;
		//fwdvect,VOLMATR1, strike, date, start, end, start, nbsimulation
	}

private:
	float CalculPrixRelatifSwaptionEuroPayeuse2(float strike, Matrix &matForward, std::vector<QuantLib::Date> &dates, QuantLib::Date debut, QuantLib::Date fin, QuantLib::Date datedecalcul){
		int n = dates.size();
		int i = 0;
		int j = 0;
		int initial = 0;
		int finale = 0;
		int M = 0;
		int datecalcul = 0;
		float strikeprice = 0;

		for (i = 0; i < n; i++){

			if (dates[i] == debut) {
				initial = i;

			}

			if (dates[i] == fin) {
				finale = i;

			}

			if (dates[i] == datedecalcul){

				datecalcul = i;

			}
		}

		if (SR2(initial, initial, finale, matForward, dates) > strike)

			strikeprice = float(SR2(initial, initial, finale, matForward, dates) - strike);//la relation de swaptions euro payeuse

		else

			strikeprice = 0;

		return strikeprice;
	}

	float calculdeA(int indicetemps, int S, int N, Matrix matForward, std::vector<QuantLib::Date> dates){

		Matrix ZC = PrixZCForward(matForward, dates);
		int n = dates.size();
		std::vector<Time> D(n);
		std::vector<Time> delta(n);
		int i = 0;
		int j = 0;
		float A = 0;

		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
		}
		for (j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
		}
		delta[n - 1] = delta[n - 2];

		DayCounter basis = Actual360();
		for (i = S; i < N; i++){

			A = A + float(delta[i + 1] * ZC[i + 1][indicetemps]);
		}


		return A;

	}
	float SR2(int indicetemps, int S, int N, Matrix matForward, std::vector<QuantLib::Date> dates){


		Matrix ZC = PrixZCForward(matForward, dates);
		float SR = 0;

		float A = calculdeA(indicetemps, S, N, matForward, dates);
		for (int i = S; i < N; i++){
			SR = SR + 0.5*ZC[i + 1][S] * matForward[i][S];
		}
		SR = SR / A;
		return SR;
	}
	Matrix PrixZCForward(Matrix &matForward, std::vector<QuantLib::Date> &dates){
		int n = dates.size();
		std::vector<Time> delta(n);
		std::vector<Time> D(n);
		Matrix ZC(n, n);
		int i, j;
		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;

		}
		for (int j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
		}
		delta[n - 1] = delta[n - 2];
		for (j = 0; j < n; j++){
			for (i = 0; i < j; i++){
				ZC[i][j] = 0;
			}

			ZC[j][j] = 1;
			for (i = j + 1; i < n; i++){
				ZC[i][j] = 1 / float(1 + delta[j] * matForward[i][j]);
			}
		}
		return ZC;
	}
	std::vector <QuantLib::Date > vectorDates(QuantLib::Date startDate, QuantLib::Date endDate, Frequency frequency){
		std::vector <QuantLib::Date > result;
		int i = 0;
		QuantLib::Date starDatetemp = startDate;
		result.push_back(starDatetemp);
		while (result[i] < endDate){
			i++;
			starDatetemp.operator+=(Period(frequency));
			result.push_back(starDatetemp);
		}
		return result;
	}
};
