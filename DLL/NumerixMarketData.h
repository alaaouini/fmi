
#pragma once

#include "NumeriX/Pro/Application.h"
#include "NumeriX/Core/Currency.h"
#include "NumeriX/Pro/Date.h"
#include "NumeriX/Core/Exception.h"
#include <vector>


using namespace NumeriX::Pro;
using namespace std;


class NumerixMarketData {
public:
	NumerixMarketData() :currency("EUR") {}
	NumeriX::Core::Currency currency;

	
	void loadGlobalConventions(NumeriX::Pro::Application& nxApp)
	{
		// These should really be loaded from XML, but
		// their included here to make this example self contained.

		ApplicationData data;

		// EUR_ShortRates
		data.addValue("Name", "EUR_ShortRates");
		data.addValue("Currency", "EUR");
		data.addValue("Notional", "");
		data.addValue("StartTenor", "");
		data.addValue("Spot Lag", "");
		data.addValue("FixCalendar", "EUTA");//EUTA
		data.addValue("FixConvention", "F");
		data.addValue("PayCalendar", "EUTA");//EUTA
		data.addValue("PayConvention", "F");
		data.addValue("AccrualConvention", "");
		data.addValue("AccrualCalendar", "");
		data.addValue("Frequency", "");
		data.addValue("Tenor", "");
		data.addValue("Basis", "ACT/360");


		// EUR_LIBOR
		data.addValue("Name", "EUR_LIBOR");//EURIBOR
		data.addValue("Currency", "EUR");
		data.addValue("Notional", "");
		data.addValue("StartTenor", "2BD");
		data.addValue("Spot Lag", "2BD");
		data.addValue("FixCalendar", "EUTA");//EUTA   Target
		data.addValue("FixConvention", "F");
		data.addValue("PayCalendar", "EUTA");//EUTA
		data.addValue("PayConvention", "MF");
		data.addValue("AccrualConvention", "MF");
		data.addValue("AccrualCalendar", "EUTA");//EUTA
		data.addValue("Frequency", "");
		data.addValue("Tenor", "");
		data.addValue("Basis", "ACT/360");



		// EUR_Future3M
		/*data.addValue("Name", "EUR_Future3M");
		data.addValue("Currency", "EUR");
		data.addValue("Notional", 1000000.00);
		data.addValue("StartTenor", "");
		data.addValue("Spot Lag", "2BD");
		data.addValue("FixCalendar", "EUTA");//EUTA
		data.addValue("FixConvention", "F");
		data.addValue("PayCalendar", "EUTA");//EUTA
		data.addValue("PayConvention", "MF");
		data.addValue("AccrualConvention", "MF");
		data.addValue("AccrualCalendar", "EUTA");//EUTA
		data.addValue("Frequency", "");
		data.addValue("Tenor", "3M");
		data.addValue("Basis", "ACT/360");*/

		// EUR_Swap_AnnMoney
		data.addValue("Name", "EUR_Swap_AnnMoney");
		data.addValue("Currency", "EUR");
		data.addValue("Notional", "");//data.addValue("Notional", "1000000");
		data.addValue("StartTenor", "0BD");  //2BD //0BD
		data.addValue("Spot Lag", "0BD"); //2BD
		data.addValue("FixCalendar", "EUTA");//EUTA
		data.addValue("FixConvention", "F");//""
		data.addValue("PayCalendar", "EUTA");//EUTA
		data.addValue("PayConvention", "MF");
		data.addValue("AccrualConvention", "MF");
		data.addValue("AccrualCalendar", "EUTA");//EUTA
		data.addValue("Frequency", "6M");
		data.addValue("Tenor", "");//""
		data.addValue("Basis", "Act/360");//30E/360




		ApplicationWarning warning;
		nxApp.setGlobalConventions(data, warning);

		if (warning.isFatal())
			throw NumeriX::Core::Exception("Global Convention Error");
	}
};




