#pragma once
//#include"Engine.h"
#include <ql/quantlib.hpp>
#include "DllHeader.h"

using namespace std;
using namespace QuantLib;
using namespace boost;

class EFFICIEBCYLIBDLL_API SwaptionEfficiencyEngine
{
public:
	virtual ~SwaptionEfficiencyEngine()
	{	}

	virtual Matrix LMMSwaptionmatforward() = 0;

};
