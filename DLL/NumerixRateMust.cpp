#include "NumerixRateMust.h"



NumerixRateMust::NumerixRateMust()
{
}


NumerixRateMust::~NumerixRateMust()
{
}



void NumerixRateMust::setComponent(TiXmlDocument doc, string nameRate, string tradeId)
{

	//rates = new vector<Rate> ();
	//matrixRate = new vector<std::tuple<Date, Date, Rate>>();
	int test = 0;
	TiXmlHandle docMUSTHandle(&doc);
	TiXmlElement *Component;
	TiXmlElement *ComponentMatrix;
	TiXmlElement *Trade = docMUSTHandle.FirstChild("BODY").FirstChild("TRADELIST").FirstChild("MUST_TR").ToElement();

	while (Trade)
	{
		if (strcmp(Trade->FirstChild("TradeId")->ToElement()->GetText(), tradeId.c_str()) == 0)
		{
			Component = Trade->FirstChild("MPTradeData")->FirstChild("MPTRDATA")->FirstChild("MPTrDataXML")->FirstChild("MPTRDATAXML")->FirstChild("STRUCTURED_INSTRUMENT")->FirstChild("VARIABLE_LIST")->FirstChild("VARIABLE")->ToElement();

			while (Component)
			{
				// si on a un seul taux 
				if (strcmp(Component->Value(), "VARIABLE") == 0 && strcmp(Component->FirstChild("TYPE")->ToElement()->GetText(), "RATE") == 0)
				{
					if (strcmp(Component->FirstChild("NAME")->ToElement()->GetText(), nameRate.c_str()) == 0)
					{
						if (Component->FirstChild("FORMULA")->FirstChild("FORMULA_STRING")->ToElement()->GetText())
						{
							rateString = Component->FirstChild("FORMULA")->FirstChild("FORMULA_STRING")->ToElement()->GetText();

							if (this->check_matrix(rateString))
							{
								ComponentMatrix = Trade->FirstChild("MPTradeData")->FirstChild("MPTRDATA")->FirstChild("MPTrDataXML")->FirstChild("MPTRDATAXML")->FirstChild("STRUCTURED_INSTRUMENT")->FirstChild("VARIABLE_LIST")->FirstChild("VARIABLE")->ToElement();
								while (ComponentMatrix)
								{
									if (strcmp(ComponentMatrix->Value(), "VARIABLE") == 0 && strcmp(ComponentMatrix->FirstChild("TYPE")->ToElement()->GetText(), "MATRIX") == 0)
									{
										if (strcmp(ComponentMatrix->FirstChild("NAME")->ToElement()->GetText(), "MatrixRates") == 0)
										{
											MatriceMust* matrixOfRates = new MatriceMust();
											matrixOfRates->setComponent(doc, "MatrixRates", tradeId);
											matrixRate = this->eff_matice_rate(matrixOfRates->matrice);
											
											//rate = this->eff_convert_float(matrixOfRates->matrice[0][2]);
											rate = this->eff_convert_double(matrixOfRates->matrice[0][2]);
											rates.push_back(rate);
											test++;
										}
									}
									ComponentMatrix = ComponentMatrix->NextSiblingElement();
								}
								test++;
							}
							else
							{
								//rate = this->eff_convert_float(rateString);
								rate = this->eff_convert_double(rateString);
								rates.push_back(rate);
								test++;

							}

						}

					}
				}

				Component = Component->NextSiblingElement();
			}


		}
		Trade = Trade->NextSiblingElement();
	}


	if (test == 0)
	{
		throw string("Erreur de la variable de type Rate");
	}
}

//m�thode
bool NumerixRateMust::check_matrix(string formule)
{
	int founder = formule.find("Lookup");
	if (founder != string::npos) return true; // si Lookup existe, alors c'est une matrice
	else return false; // sinon c'est une valeur constante

}


// si on a un taux constant =>remplir la matrice de taux par une seule ligne
//void NumerixRateMust::SetMatrixRate(QuantLib::Date startDate, QuantLib::Date endDate, vector<Rate> rates)
//{
//	if (this->matrixRate.size() == 0)
//	{
//		vector<tuple<QuantLib::Date, QuantLib::Date, Rate>> matrix{ std::make_tuple(startDate, endDate, rates[0]) };
//		matrixRate = (matrix);
//	}
//
//}
