#include "NumerixSwap.h"




NumerixSwap::NumerixSwap(NumeriX::Pro::Application& app, double Spread, double Notional, std::string numerixIndexFrequency, std::string numerixFixedFrequency, std::string numerixFloatingFrequency, double Strike, std::string IndexBasis_, std::string FixedBasis_, std::string FloatingBasis_, int inttodayDate, int intstartDate, int intmaturity) :
	nxApp(app)
{
	spread = Spread;
	notional = Notional;

	indexFrequency = numerixIndexFrequency;
	fixedFrequency = numerixFixedFrequency;
	floatingFrequency = numerixFloatingFrequency;

	strike = Strike;

	indexBasis = Basis(IndexBasis_);

	fixedBasis = Basis(FixedBasis_);
	floatingBasis = Basis(FloatingBasis_);

	
	

	maturity = Date(intmaturity);
	todayDate = Date(inttodayDate);
	startDate = Date(intstartDate);
	
	
	
	


}

NumerixSwap::NumerixSwap()
{
}

NumerixSwap::~NumerixSwap()
{
}



double NumerixSwap :: NPV()
{
	return npv;
}


double NumerixSwap::NPV_FixedLeg()
{
	return npv_fixed_leg;
}

double NumerixSwap::NPV_FloatingLeg()
{
	return npv_floating_leg;
}

void NumerixSwap ::setEngine(CurveDataNumerix& curveData)
{
	vector<Index> componentsInstrumentIndices;
	vector<Event> componentsInstrumentEvents;
	vector<ComponentsInstrument::Data> componentsInstrumentDataNameValue;
	Script script;


	//yieldCurve-----------------------------------------------------------------------------------------------
	YieldCurve yieldCurve = curveData.buildCurve(curveData, todayDate);

	//Calendar-------------------------------------------------------------------------------------------------------------------------------
	//NumeriX::Pro::Calendar calendar(nxApp, "calendar", loadCalendarDates());
	//Holidays holidaysCalendar("calendar");


	//FloatingIndex------------------------------------------------------------------------------------------------------------------------------
	Tenor floatingIndexTenor = indexFrequency;
	string floatingIndexName = "FundLIBOR";
	Holidays floatingIndexAccrualCalendar = "EUTA";
	RollConvention floatingIndexAccrualConvention = RollConvention::MF;
	Holidays floatingIndexFixingCalendar = "EUTA";
	Tenor floatingIndexSpotLag("2bd");
	FloatingIndex::OptionalArgsTenor floatingIndexOptargs;
	Basis basis = indexBasis;
	floatingIndexOptargs.setBasis(basis);
	floatingIndexOptargs.setFixingConvention(RollConvention::P);
	

	FloatingIndex floatingIndex(nxApp, floatingIndexName, floatingIndexTenor, currency,
		floatingIndexAccrualCalendar, floatingIndexAccrualConvention, floatingIndexFixingCalendar,
		floatingIndexSpotLag, floatingIndexOptargs);



	//Schedule 1 & 2 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	string interestSchedule1Name = "FundDates";
	Basis interestSchedule1Basis = floatingBasis;
	Date interestSchedule1StartDate = startDate ;
	Date interestSchedule1EndDate = maturity;
	Tenor interestSchedule1Frequency = floatingFrequency;
	InterestSchedule::OptionalArgsNonIMM interestSchedule1Optargs;
	Holidays interestSchedule1FixingCalendar = "EUTA";
	interestSchedule1Optargs.setFixingCalendar(interestSchedule1FixingCalendar);
	Tenor interestSchedule1FixingLag("2bd");
	interestSchedule1Optargs.setFixingLag(interestSchedule1FixingLag);
	bool interestSchedule1FrontPay = false;
	interestSchedule1Optargs.setFrontPay(interestSchedule1FrontPay);
	Holidays interestSchedule1PayCalendar = "EUTA";
	interestSchedule1Optargs.setPayCalendar(interestSchedule1PayCalendar);
	RollConvention interestSchedule1PayConvention = RollConvention::MF;
	interestSchedule1Optargs.setPayConvention(interestSchedule1PayConvention);
	Holidays interestSchedule1AccrualCalendar = "EUTA";
	interestSchedule1Optargs.setAccrualCalendar(interestSchedule1AccrualCalendar);
	RollConvention interestSchedule1AccrualConvention = RollConvention::MF;
	interestSchedule1Optargs.setAccrualConvention(interestSchedule1AccrualConvention);
	InterestSchedule interestSchedule1(nxApp, interestSchedule1Name,
		interestSchedule1Basis, interestSchedule1StartDate,
		interestSchedule1EndDate, interestSchedule1Frequency,
		interestSchedule1Optargs);
	//2 --------------------------
	InterestSchedule::OptionalArgsNonIMM interestSchedule2Optargs;
	string interestSchedule2Name = "CouponDates";
	Basis interestSchedule2Basis = fixedBasis;
	Date interestSchedule2StartDate = startDate;
	Date interestSchedule2EndDate = maturity;
	Tenor interestSchedule2Frequency = fixedFrequency;
	Holidays interestSchedule2FixingCalendar = "EUTA";   //("NONE");
	interestSchedule2Optargs.setFixingCalendar(interestSchedule2FixingCalendar);
	Tenor interestSchedule2FixingLag("2bd");
	interestSchedule2Optargs.setFixingLag(interestSchedule2FixingLag);
	bool interestSchedule2FrontPay = false;
	interestSchedule2Optargs.setFrontPay(interestSchedule2FrontPay);
	Holidays interestSchedule2PayCalendar = "EUTA";
	interestSchedule2Optargs.setPayCalendar(interestSchedule2PayCalendar);
	RollConvention interestSchedule2PayConvention = RollConvention::MF;
	interestSchedule2Optargs.setPayConvention(interestSchedule2PayConvention);
	Holidays interestSchedule2AccrualCalendar = "EUTA"; //("NONE");
	interestSchedule2Optargs.setAccrualCalendar(interestSchedule2AccrualCalendar);
	RollConvention interestSchedule2AccrualConvention = RollConvention::MF; // NONE;
	interestSchedule2Optargs.setAccrualConvention(interestSchedule2AccrualConvention);
	InterestSchedule interestSchedule2(nxApp, interestSchedule2Name,
		interestSchedule2Basis, interestSchedule2StartDate,
		interestSchedule2EndDate, interestSchedule2Frequency,
		interestSchedule2Optargs);




	//componentsInstrument------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	componentsInstrumentIndices.push_back(floatingIndex);

	componentsInstrumentEvents.push_back(interestSchedule1);
	componentsInstrumentEvents.push_back(interestSchedule2);

	cout << "NumerixSwap::price spread:" << spread << endl;
	cout << "NumerixSwap::price fixed:" << strike << endl;
	cout << "NumerixSwap::price Notional:" << notional << endl;

	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Spread", spread));
	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Fixed", strike));
	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Notional", notional));

	script = Script(nxApp, createScript(), Script::BACKWARD);

	componentsInstrument= ComponentsInstrument(nxApp,
		componentsInstrumentIndices,
		componentsInstrumentEvents,
		componentsInstrumentDataNameValue,
		script);


	//Model -----------------------------------------------------------------------------------------
	model = DeterministicIR(nxApp, currency, yieldCurve);


	string kernelPricer1Method("BackwardAnalytic");
	MethodParameters methodParameters1Quality(0);

	cout << "NumerixSwap ::setEngine quality" << 0 << endl;
	KernelPricer kernelPricer1(nxApp,
		componentsInstrument,
		model,
		kernelPricer1Method,
		methodParameters1Quality,
		todayDate);
	npv = kernelPricer1.getPV("Swap");
	npv_fixed_leg = kernelPricer1.getPV("CouponLeg");
	npv_floating_leg = kernelPricer1.getPV("FundingLeg"); 

	/*
	std::cout.unsetf(std::ios::floatfield);         
	std::cout.precision(10);


	double couponLegValue = 0;
	const std::string couponLogName = "CouponLog";
	const NumeriX::Pro::KernelPricer::PaymentLog*  couponLog = kernelPricer1.getPaymentLog(couponLogName);

	const std::string couponLogName1 = "CouponLog1";
	const NumeriX::Pro::KernelPricer::PaymentLog*  couponLog1 = kernelPricer1.getPaymentLog(couponLogName1);

	const std::string couponLogName2 = "CouponLog2";
	const NumeriX::Pro::KernelPricer::PaymentLog*  couponLog2 = kernelPricer1.getPaymentLog(couponLogName2);

	double fundLegValue = 0;
	const std::string fundLogName = "FundLog";
	const NumeriX::Pro::KernelPricer::PaymentLog*  fundLog = kernelPricer1.getPaymentLog(fundLogName);

	const std::string fundLogName1 = "FundLog1";
	const NumeriX::Pro::KernelPricer::PaymentLog*  fundLog1 = kernelPricer1.getPaymentLog(fundLogName1);

	const std::string fundLogName2 = "FundLog2";
	const NumeriX::Pro::KernelPricer::PaymentLog*  fundLog2 = kernelPricer1.getPaymentLog(fundLogName2);


	for (int i = 0; i < couponLog->size(); i++)
	{
		Date date = couponLog->getDate(i);
		string dateString = date.toString();
		cout << " NumerixSwap::setEngine couponLog dateString: " << dateString << endl;

		double pv = couponLog->getPV(date);
		cout << " NumerixSwap::setEngine couponLog pv: " << pv << endl;
		couponLegValue += pv;


		double actual = couponLog->getActual(date);
		cout << " NumerixSwap::setEngine couponLog actual: " << actual << endl;

		cout << "---------------------------------" << endl;

	}

	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << " NumerixSwap::setEngine couponLegValue:"  << couponLegValue << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	



	for (int i = 0; i < fundLog->size(); i++)
	{
		Date date = fundLog->getDate(i);
		string dateString = date.toString();
		cout << " NumerixSwap::setEngine fundLog dateString: "  << dateString << endl;

		double pv = fundLog->getPV(date);
		cout << " NumerixSwap::setEngine fundLog pv: "  <<  pv << endl;
		fundLegValue += pv;

		double actual = fundLog->getActual(date);
		cout << " NumerixSwap::setEngine fundLog actual: "  <<  actual << endl;

		cout << "---------------------------------" << endl;

	}

	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << " NumerixSwap::setEngine fundLegValue:" << fundLegValue << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << " NumerixSwap::setEngine NPV:" << (fundLegValue- couponLegValue) << endl;
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	for (int i = 0; i < fundLog->size(); i++)
	{
		Date couponLogDate = couponLog->getDate(i);
		string couponLogDateString = couponLogDate.toString();
		double couponLogActual = couponLog->getActual(couponLogDate);
		double couponLogPv = couponLog->getPV(couponLogDate);

		Date fundLogDate = fundLog->getDate(i);
		string fundLogDateString = fundLogDate.toString();
		double fundLogActual = fundLog->getActual(fundLogDate);
		double fundLogPv = fundLog->getPV(fundLogDate);

		cout << couponLogDateString << "   "  << couponLogActual << " "  << couponLogPv << " " << fundLogDateString << " " << fundLogActual << " " << fundLogPv << endl;

		double couponLogActual1 = couponLog1->getActual(couponLogDate);
		double couponLogActual2 = couponLog2->getActual(couponLogDate);

		double fundLogActual1 = fundLog1->getActual(fundLogDate);
		double fundLogActual2 = fundLog2->getActual(fundLogDate);

		cout << "####################" << "couponLogActual1 " << couponLogActual1 << " couponLogActual2 " << couponLogActual2 << " * = " << couponLogActual1*couponLogActual2*1000000.0 << endl;
		cout << "####################" << "fundLogActual1 " << fundLogActual1 << " fundLogActual2 " << fundLogActual2  << " * = " << fundLogActual1*fundLogActual2*1000000.0 << endl;
	}*/

	
}

/*script.push_back("PRODUCTS");
script.push_back("DISCOUNTING CouponLeg, FundingLeg, Swap");
script.push_back("TEMPORARY StructuredCoupon, FloatCoupon");
script.push_back("PAYMENTLOG CouponLog, FundLog, CouponLog1, CouponLog2, FundLog1, FundLog2");
script.push_back("END PRODUCTS");

script.push_back("PAYOFFSCRIPT");
// Calculate the value of the Coupon Leg
script.push_back("IF ISACTIVE(CouponDates) THEN");
script.push_back("StructuredCoupon = Fixed * CouponDatesDCF * Notional");
script.push_back("LOGPAYMENT(StructuredCoupon, CouponDates, THISPAY, CouponLog)");
script.push_back("LOGPAYMENT(Fixed, CouponDates, THISPAY, CouponLog1)");
script.push_back("LOGPAYMENT(CouponDatesDCF, CouponDates, THISPAY, CouponLog2)");
script.push_back("CouponLeg += CASH(StructuredCoupon, CouponDates, THISPAY)");
script.push_back("END IF");
//1
//2
// Calculate the value of the Funding Leg
script.push_back("IF ISACTIVE(FundDates) THEN");
script.push_back("FloatCoupon = (FundLIBOR + Spread) * FundDatesDCF * Notional");
script.push_back("LOGPAYMENT(FloatCoupon, FundDates, THISPAY, FundLog)");
script.push_back("LOGPAYMENT(FundLIBOR, FundDates, THISPAY, FundLog1)");
script.push_back("LOGPAYMENT(FundDatesDCF, FundDates, THISPAY, FundLog2)");
script.push_back("FundingLeg += CASH(FloatCoupon, FundDates, THISPAY)");
script.push_back("END IF");
// Calculate the value of the Swap
script.push_back("Swap = FundingLeg - CouponLeg");
script.push_back("END PAYOFFSCRIPT");*/


vector<string> NumerixSwap::createScript() {

	vector<string> script;
	script.push_back("PRODUCTS");
	script.push_back("DISCOUNTING CouponLeg, FundingLeg, Swap");
	script.push_back("TEMPORARY StructuredCoupon, FloatCoupon");
	script.push_back("PAYMENTLOG CouponLog, FundLog");
	script.push_back("END PRODUCTS");
	script.push_back("");
	script.push_back("PAYOFFSCRIPT");
	script.push_back("// Calculate the value of the Coupon Leg");
	script.push_back("IF ISACTIVE(CouponDates) THEN");
	script.push_back(" StructuredCoupon = Fixed * CouponDatesDCF * Notional");
	script.push_back(" LOGPAYMENT(StructuredCoupon, CouponDates, THISPAY, CouponLog)");
	script.push_back(" CouponLeg += CASH(StructuredCoupon, CouponDates, THISPAY)");
	script.push_back("END IF");
	script.push_back("//Calculate the value of the Funding Leg");
	script.push_back("IF ISACTIVE(FundDates) THEN");
	script.push_back("  FloatCoupon = (FundLIBOR + Spread) * FundDatesDCF * Notional");
	script.push_back("  LOGPAYMENT(FloatCoupon, FundDates, THISPAY, FundLog)");
	script.push_back("  FundingLeg += CASH(FloatCoupon, FundDates, THISPAY)");
	script.push_back("END IF");
	script.push_back("Swap = FundingLeg - CouponLeg");
	script.push_back("END PAYOFFSCRIPT");

	return script;
}


