#pragma once

#include "NumeriX/Pro/Date.h"
using namespace NumeriX::Pro;

Tenor getTenorDurationInDays(NumeriX::Pro::Date& startDate, NumeriX::Pro::Date& endDate);