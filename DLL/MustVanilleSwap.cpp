#pragma once
#include <ql/quantlib.hpp>
#include "MustVanilleSwap.h"


using namespace std;
using namespace QuantLib;
using namespace boost;


MustVanilleSwap::MustVanilleSwap(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdldoc, string trId) : 
	EfficiencyLibProduct(valuationDate, typeProd, paths, modelProd, hdldoc, trId){
	this->modelProduct = modelProd;
}

void MustVanilleSwap::setComponentsQuantLib(Handle<QuantLib::YieldTermStructure> forwardingTermStructure){
	//this->forwardingTermStructure = forwardingTermStructure ;
	this->setComponentsForSwap();

	//index
	MustVanilleSwap::myIndex = ComponentIndexMust.ConstructIndex(forwardingTermStructure);

	//FixedLeg
	SwapFixedLeg fixed_Leg;
	fixed_Leg.setComponent(fixedRateObj.matrixRate, fixedLegObj.basisQ, floatingLegObj.freqQ);

	////FloatingLeg
	SwapFloatingLeg floating_leg;
	floating_leg.setComponent(MustVanilleSwap::myIndex, floatingLegObj.basisQ, floatingLegObj.freqQ);

	cout << " MustVanilleSwap::setComponentsQuantLib() fixedLeg floatingLegObj.freqQ: " << floatingLegObj.freqQ << endl;
	cout << " MustVanilleSwap::setComponentsQuantLib() floatingLeg floatingLegObj.freqQ: " << floatingLegObj.freqQ << endl;


	if (ComponentIndexMust.spreads.size()<1) //condition to check the Sread
	{
		MustVanilleSwap::fixedLeg = fixed_Leg.constructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
		MustVanilleSwap::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
	}
	else
	{
		MustVanilleSwap::fixedLeg = fixed_Leg.constructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal);
		MustVanilleSwap::floatingLeg = floating_leg.ConstructLeg(startDateObj.dateQ, endDateObj.dateQ, ComponentPrincipalMust.nominal, ComponentIndexMust.matrixSpread);
	}
}

 void MustVanilleSwap::makeSwap(Handle<QuantLib::YieldTermStructure> forwardingTermStructure){
	setComponentsQuantLib(forwardingTermStructure);
	this->swap = boost::shared_ptr<Swap>(new Swap(fixedLeg, floatingLeg));
}

 Real MustVanilleSwap::price(){
	 switch (modelProduct) {
	 case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
	 {
		 CurveData cData;
		 // Fill out with some sample market data
		 cData.sampleMktData(cData);

		 // Build a curve linked to this market data
		 boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));
		 /*string a = MustVanilleSwap::convert_Date(swap->maturityDate());*/
		 // Link the curve to the term structure
		 this->forwardingTermStructure.linkTo(ocurve);

		 QuantLib::Settings::instance().evaluationDate() = today;
		 MustVanilleSwap::swap->setPricingEngine(boost::shared_ptr<PricingEngine>(
			 new DiscountingSwapEngine(this->forwardingTermStructure)));
		 Real npv = this->swap->NPV();
		 npvFixedLeg = this->swap->legNPV(0);
		 npvFloatingLeg = this->swap->legNPV(1);
		 Real shift = 0.0001;
		 delta = computeDelta(shift);
		 vega = computeVega();
		 gamma = computeGamma(shift);
		 return  npv; 
}
	 break;
	 case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_MODEL:
	 {
		 this->setComponentsForNumerixSwap();
		 double spredTest;
		 
		 spredTest = MustVanilleSwap::NumeriXIndexMust.spread;
		
		

		 std::cout.unsetf(std::ios::floatfield);
		 std::cout.precision(10);

		
		 cout << "4 MustVanilleSwap::price spredTest:" << spredTest << endl;

		 double a = MustVanilleSwap::NumeriXPrincipalMust.nominal;
		 int ttest = MustVanilleSwap::NumerixmaturityObj.dateReference;

		 cout << "  MustVanilleSwap::price() MustVanilleSwap::NumeriXIndexMust.spread " << spredTest << endl;
		 cout << "  MustVanilleSwap::price() MustVanilleSwap::NumeriXPrincipalMust.nominal " << MustVanilleSwap::NumeriXPrincipalMust.nominal << endl;
		 cout << "  MustVanilleSwap::price() MustVanilleSwap::numerixFixedRate.rate " << MustVanilleSwap::numerixFixedRate.rate << endl;
		 
		 cout << "  MustVanilleSwap::price() today " << today << endl;
		 cout << "  MustVanilleSwap::price() MustVanilleSwap::NumerixstartDateObj.dateReference " << NumeriX::Pro::Date(MustVanilleSwap::NumerixstartDateObj.dateReference) << endl;
		 cout << "  MustVanilleSwap::price() MustVanilleSwap::NumerixendmaturityObj.dateReference " << NumeriX::Pro::Date(MustVanilleSwap::NumerixmaturityObj.dateReference) << endl;

		 
		 string numerixIndexFrequency = MustVanilleSwap::NumeriXIndexMust.numerixIndexFrequency;
		 string numerixFloatingLegFreq = MustVanilleSwap::NumerixfloatingLeg.freqQ;
		
		 string numerixFixedLegFreq = MustVanilleSwap::NumerixfixedLeg.freqQ;
		
		 cout << "  MustVanilleSwap::price() numerixIndexFrequency " << numerixIndexFrequency << endl;
		 cout << "  MustVanilleSwap::price() numerixFloatingLegFreq " << numerixFloatingLegFreq << endl;
		 cout << "  MustVanilleSwap::price() numerixFixedLegFreq " << numerixFixedLegFreq << endl;

		 string  numerixIndexCashFlowBasis = MustVanilleSwap::NumeriXIndexMust.IndexBasis;
		 string  numerixfixedLegCashFlowBasis = MustVanilleSwap::NumerixfixedLeg.CashFlowBasis;
		 string  NumerixfloatingLegCashFlowBasis = MustVanilleSwap::NumerixfloatingLeg.CashFlowBasis;

		 //numerixfixedLegCashFlowBasis = "ACT/ACT";
		 //NumerixfloatingLegCashFlowBasis = "ACT/ACT";

		 cout << "  MustVanilleSwap::price() numerixIndexCashFlowBasis " << numerixIndexCashFlowBasis << endl;
		 cout << "  MustVanilleSwap::price() numerixfixedLegCashFlowBasis " << numerixfixedLegCashFlowBasis << endl;
		 cout << "  MustVanilleSwap::price() NumerixfloatingLegCashFlowBasis " << NumerixfloatingLegCashFlowBasis << endl;

		 this->numerixswap = boost::shared_ptr<NumerixSwap>(new NumerixSwap(nxApp,spredTest, MustVanilleSwap::NumeriXPrincipalMust.nominal, numerixIndexFrequency, MustVanilleSwap::NumerixfixedLeg.freqQ, MustVanilleSwap::NumerixfloatingLeg.freqQ, MustVanilleSwap::numerixFixedRate.rate, numerixIndexCashFlowBasis, numerixfixedLegCashFlowBasis, NumerixfloatingLegCashFlowBasis, today.serialNumber(), MustVanilleSwap::NumerixstartDateObj.dateReference, MustVanilleSwap::NumerixmaturityObj.dateReference));
		 CurveDataNumerix curveDataNumerix(nxApp);
		 numerixswap->setEngine(curveDataNumerix);
		// string CashFlowCcy;
		 //string CashFlowFreq;
		 //string CashFlowBasis;
		 double npv = numerixswap->NPV();
		 npvFixedLeg = numerixswap->NPV_FixedLeg();
		 npvFloatingLeg = numerixswap->NPV_FloatingLeg();
		 Real shift = 0.0001;
		 
		 delta = computeDelta(shift);
		 vega = computeVega();
		 gamma = computeGamma(shift);
		 cout << " MustVanilleSwap::price() npv:=" << npv << endl;
		 return npv;
	 }

	 break;
	 case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_BLACKFT:
	 {

		 return 1;
	 }
	 break;
	 default://just to do something
	 {
		 return 0;
	 }
	 break;
	 }
 }

 Real MustVanilleSwap::computeDelta(Real shift){
	 switch (modelProduct) {
	 case EfficiencyProduct::EfficiencyModelProduct::QUANTLIB_LMM:
	 {
		 CurveData cData;
		 // Fill out with some sample market data
		 cData.sampleMktData(cData);

		 cData.bumpCurve(cData, shift);
		 // Build a curve linked to this market data
		 boost::shared_ptr<YieldTermStructure> ocurve = (cData.buildCurve(cData));
		 this->forwardingTermStructure.linkTo(ocurve);
		 /*
		 Settings::instance().evaluationDate() = today;
		 MustVanilleSwap::swap->setPricingEngine(boost::shared_ptr<PricingEngine>(
		 new DiscountingSwapEngine(this->forwardingTermStructure)));
		 */
		 Real npv1 = this->swap->NPV();

		 // Fill out with some sample market data
		 cData.sampleMktData(cData);
		 cData.bumpCurve(cData, -shift);
		 // Build a curve linked to this market data
		 ocurve = (cData.buildCurve(cData));
		 this->forwardingTermStructure.linkTo(ocurve);
		 Real npv2 = this->swap->NPV();
		 cout << "MustVanilleSwap::computeDelta QUANTLIB_LMM npv1" << npv1 << endl;
		 cout << "MustVanilleSwap::computeDelta QUANTLIB_LMM npv2" << npv2 << endl;
		 Real a = (npv1 - npv2);
		 Real b = (2 * shift);
		 Real c = a / b;
		 cout << "MustVanilleSwap::computeDelta QUANTLIB_LMM delta" << c << endl;
		 return c;
	 }
	 break;
	 case EfficiencyProduct::EfficiencyModelProduct::NUMERIX_MODEL:
	 {
		 CurveDataNumerix curveDataNumerix1(nxApp);

		 curveDataNumerix1.bumpCurve(curveDataNumerix1, shift);

		 numerixswap->setEngine(curveDataNumerix1);
		 double npv1 = numerixswap->NPV();

		 CurveDataNumerix curveDataNumerix2(nxApp);

		 curveDataNumerix2.bumpCurve(curveDataNumerix2, -shift);

		 numerixswap->setEngine(curveDataNumerix2);
		 double npv2 = numerixswap->NPV();

		 cout << "MustVanilleSwap::computeDelta NUMERIX_MODEL npv1" << npv1 << endl;
		 cout << "MustVanilleSwap::computeDelta NUMERIX_MODEL npv2" << npv2 << endl;

		 double a = (npv1 - npv2);
		 double b = (2 * shift);
		 double c = a / b;
		 cout << "MustVanilleSwap::computeDelta NUMERIX_MODEL delta" << c << endl;
		 return c;
	 }
	 break;
	 default://just to do something
	 {
		 return 0;
	 }
	 break;
	 }
 }
 Real MustVanilleSwap::computeGamma(Real shift){
		 Real delta1 = computeDelta(shift);
		 Real delta2 = computeDelta((-1)* shift);
		 cout << "MustVanilleSwap::computeGamma Gamma=" << (delta1 - delta2) / (2 * shift) << endl;
		 return (delta1 - delta2) / (2 * shift);
	
 }
 Real MustVanilleSwap::computeVega(){ return 0; }
 Real MustVanilleSwap::computeTheta(){ return 0; }