#pragma once

#include "NumeriX/Core/Currency.h"
#include "NumeriX/Pro/Date.h"
#include "NumeriX/Pro/Models/Model.h"
#include "CurveDataNumerix.h"
#include <vector>
using namespace std;
using namespace NumeriX::Core;


#include "CurveDataNumerix.h"
using namespace NumeriX::Pro;
using namespace std;

class NumerixCapFloor {

public: NumerixCapFloor(NumeriX::Pro::Application& app, double notional, string numerixFloatingLegFreq, bool isCap,NumeriX::Pro::Date& todayDate_, NumeriX::Pro::Date& startDate_, NumeriX::Pro::Date& endDate_, vector<double>& strikes_,string& cashFlowBasis_);
		void setEngine(Model& model);
		double NPV();

private: 
	     string cashFlowBasis;
	     string floatingLegFreq;
	     vector<string> createScript();
	     NumeriX::Pro::Application nxApp;
		 double notional;
		 bool isCap;
		 NumeriX::Pro::Date todayDate;
	     NumeriX::Pro::Date startDate;
		 NumeriX::Pro::Date endDate;
	     vector<double> strikes;
		 double npv;
		 

};
