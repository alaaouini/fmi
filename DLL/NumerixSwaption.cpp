#pragma once

#include "NumerixSwaption.h"
#include "NumeriX/Pro/Structuring/SingleEvent.h"

#include <iostream>
using namespace std;

NumerixSwaption::NumerixSwaption(NumeriX::Pro::Application& app,
	bool isEuropean_, double spread_, double notional_,
	string& numerixIndexFrequency_,
	string& numerixFixedFrequency_, string& numerixFloatingFrequency_,
	double fixedrate_, string& IndexBasis_, string& FixedBasis_, string& floatingBasis_,
	NumeriX::Core::Date& todayDate_,
	NumeriX::Core::Date& startDate_, NumeriX::Core::Date& maturityDate_) :
	nxApp(app)
{
	isEuropean = isEuropean_;
	spread = spread_;
	notional = notional_;
	numerixIndexFrequency = numerixIndexFrequency_;
	numerixFixedFrequency = numerixFixedFrequency_;
	numerixFloatingFrequency = numerixFloatingFrequency_;
	fixedrate = fixedrate_;
	IndexBasis = IndexBasis_;
	FixedBasis = FixedBasis_;
	floatingBasis = floatingBasis_;
	todayDate = todayDate_;
	startDate = startDate_;
	maturityDate = maturityDate_;
}



void NumerixSwaption::setEngine(Model& model)
{
	//FloatingIndex------------------------------------------------------------------------------------------------------------------------------
	string floatingIndexName = "FundIndex";
	NumeriX::Core::Tenor floatingIndexTenor = numerixIndexFrequency;
	NumeriX::Core::Holidays floatingIndexAccrualCalendar = "EUTA";
	NumeriX::Core::RollConvention floatingIndexAccrualConvention = NumeriX::Core::RollConvention::MF;
	NumeriX::Core::Holidays floatingIndexFixingCalendar = "EUTA";
	NumeriX::Core::Tenor floatingIndexSpotLag = "2bd";
	NumeriX::Pro::FloatingIndex::OptionalArgsTenor floatingIndexOptargs;
	NumeriX::Pro::Basis basis = IndexBasis;
	floatingIndexOptargs.setBasis(basis);
	floatingIndexOptargs.setFixingConvention(NumeriX::Core::RollConvention::P);
	NumeriX::Pro::FloatingIndex floatingIndex(nxApp, floatingIndexName, floatingIndexTenor, currency,
		floatingIndexAccrualCalendar, floatingIndexAccrualConvention, floatingIndexFixingCalendar,
		floatingIndexSpotLag, floatingIndexOptargs);
	//interest schedule1
	string interestSchedule1Name = "FundDates";
	Basis interestSchedule1Basis = floatingBasis;
	Date interestSchedule1StartDate = startDate;
	Date interestSchedule1EndDate = maturityDate;
	Tenor interestSchedule1Frequency = numerixFloatingFrequency;
	InterestSchedule::OptionalArgsNonIMM interestSchedule1Optargs;
	Holidays interestSchedule1FixingCalendar = "EUTA";
	interestSchedule1Optargs.setFixingCalendar(interestSchedule1FixingCalendar);
	Tenor interestSchedule1FixingLag("2bd");
	interestSchedule1Optargs.setFixingLag(interestSchedule1FixingLag);
	bool interestSchedule1FrontPay = false;
	interestSchedule1Optargs.setFrontPay(interestSchedule1FrontPay);
	Holidays interestSchedule1PayCalendar = "EUTA";
	interestSchedule1Optargs.setPayCalendar(interestSchedule1PayCalendar);
	RollConvention interestSchedule1PayConvention = RollConvention::MF;
	interestSchedule1Optargs.setPayConvention(interestSchedule1PayConvention);
	Holidays interestSchedule1AccrualCalendar = "EUTA";
	interestSchedule1Optargs.setAccrualCalendar(interestSchedule1AccrualCalendar);
	RollConvention interestSchedule1AccrualConvention = RollConvention::MF;
	interestSchedule1Optargs.setAccrualConvention(interestSchedule1AccrualConvention);
	InterestSchedule interestSchedule1(nxApp, interestSchedule1Name,
		interestSchedule1Basis, interestSchedule1StartDate,
		interestSchedule1EndDate, interestSchedule1Frequency,
		interestSchedule1Optargs);
	//interest schedule 2 --------------------------
	InterestSchedule::OptionalArgsNonIMM interestSchedule2Optargs;
	string interestSchedule2Name = "CouponDates";
	Basis interestSchedule2Basis = FixedBasis;
	Date interestSchedule2StartDate = startDate;
	Date interestSchedule2EndDate = maturityDate;
	Tenor interestSchedule2Frequency = numerixFixedFrequency;
	Holidays interestSchedule2FixingCalendar = "EUTA";
	interestSchedule2Optargs.setFixingCalendar(interestSchedule2FixingCalendar);
	Tenor interestSchedule2FixingLag("0d");
	interestSchedule2Optargs.setFixingLag(interestSchedule2FixingLag);
	bool interestSchedule2FrontPay = false;
	interestSchedule2Optargs.setFrontPay(interestSchedule2FrontPay);
	Holidays interestSchedule2PayCalendar = "EUTA";
	interestSchedule2Optargs.setPayCalendar(interestSchedule2PayCalendar);
	RollConvention interestSchedule2PayConvention = RollConvention::MF;
	interestSchedule2Optargs.setPayConvention(interestSchedule2PayConvention);
	Holidays interestSchedule2AccrualCalendar = "EUTA";
	interestSchedule2Optargs.setAccrualCalendar(interestSchedule2AccrualCalendar);
	RollConvention interestSchedule2AccrualConvention = RollConvention::NONE;
	interestSchedule2Optargs.setAccrualConvention(interestSchedule2AccrualConvention);

	InterestSchedule interestSchedule2(nxApp, interestSchedule2Name,
		interestSchedule2Basis, interestSchedule2StartDate,
		interestSchedule2EndDate, interestSchedule2Frequency,
		interestSchedule2Optargs);

	//Option schedule
	const OptionsSchedule::OptionalArgs optargs;
	//Script
	NumeriX::Pro::Script script(nxApp, createScript(), NumeriX::Pro::Script::BACKWARD);
	vector<Index> componentsInstrumentIndices;
	componentsInstrumentIndices.push_back(floatingIndex);
	vector<Event> componentsInstrumentEvents;
	componentsInstrumentEvents.push_back(interestSchedule1);
	componentsInstrumentEvents.push_back(interestSchedule2);
	if (!isEuropean)
	{
		//OptionSchedule
		string optionsScheduleName = "CallDates";
		Tenor optionsScheduleFrequency = numerixFloatingFrequency;
		OptionsSchedule optionsSchedule(nxApp, optionsScheduleName, startDate, maturityDate,
			optionsScheduleFrequency, optargs);
		componentsInstrumentEvents.push_back(optionsSchedule);
	}
	else
	{
		string expiryDateName = "ExpiryDate";
		SingleEvent expiryEvent(nxApp, expiryDateName, maturityDate);
		componentsInstrumentEvents.push_back(expiryEvent);
	}

	vector<ComponentsInstrument::Data> componentsInstrumentDataNameValue;
	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Spread", spread));
	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Fixed", fixedrate));
	componentsInstrumentDataNameValue.push_back(ComponentsInstrument::Data("Notional", notional));



	//Component instrument
	NumeriX::Pro::ComponentsInstrument componentsInstrument(nxApp,
		componentsInstrumentIndices,
		componentsInstrumentEvents,
		componentsInstrumentDataNameValue,
		script);



	//Kernel pricer

	string method("BackwardAnalytic");
	MethodParameters quality(0);
	KernelPricer::OptionalArgs optargs2;
	NumeriX::Pro::KernelPricer kernelPricer(nxApp, componentsInstrument, model, method, quality, todayDate, optargs2);
	cout << "NumerixSwaption::setEngine(Model& model) 11111" << endl;
	npv = kernelPricer.getPV("Opt");
	cout << "NumerixSwaption::setEngine(Model& model) " << npv << endl;
}





double NumerixSwaption::NPV()
{
	return npv;
}



vector<string> NumerixSwaption::createScript() {
	vector<string> script;
	script.push_back("PRODUCTS");
	script.push_back("DISCOUNTING CouponLeg, FundingLeg, Swap, Opt");
	script.push_back("TEMPORARY StructuredCoupon, FloatCoupon");
	script.push_back("PAYMENTLOG CouponLog, FundLog");
	script.push_back("END PRODUCTS");
	script.push_back("");
	script.push_back("PAYOFFSCRIPT");
	script.push_back("// Calculate the value of the Coupon Leg");
	script.push_back("IF ISACTIVE(CouponDates) THEN");
	script.push_back("StructuredCoupon = Fixed * CouponDatesDCF * Notional");
	script.push_back("LOGPAYMENT(StructuredCoupon, CouponDates, THISPAY, CouponLog)");
	script.push_back("CouponLeg += CASH(StructuredCoupon, CouponDates, THISPAY)");
	script.push_back("END IF");
	script.push_back("");
	script.push_back("// Calculate the value of the Funding Leg");
	script.push_back("IF ISACTIVE(FundDates) THEN");
	script.push_back("FloatCoupon = (FundIndex + Spread) * FundDatesDCF * Notional");
	script.push_back("LOGPAYMENT(FloatCoupon, FundDates, THISPAY, FundLog)");
	script.push_back("FundingLeg += CASH(FloatCoupon, FundDates, THISPAY)");
	script.push_back("END IF");
	script.push_back("");
	script.push_back("// Calculate the value of the Swap");
	script.push_back("Swap = FundingLeg - CouponLeg");
	script.push_back("");
	script.push_back("// Calculate the value of the Option");
	if (!isEuropean)
	{
		script.push_back("IF ISACTIVE(CallDates) THEN");
		script.push_back("Opt = MAX(Opt, -Swap)");
		script.push_back("END IF");
	}
	else
	{
		script.push_back("IF ISACTIVE(ExpiryDate) THEN");
		script.push_back("Opt = MAX(0, -Swap)");
		script.push_back("END IF");
	}
	script.push_back("");
	script.push_back("");
	script.push_back("END PAYOFFSCRIPT");


	cout << "NumerixSwaption::createScript() " << endl;
	for (int i = 0; i < script.size(); i++)
	{
		string line = script[i];
		cout << "Script:" << line << endl;
	}
	return script;
}
