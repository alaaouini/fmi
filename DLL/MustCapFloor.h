#pragma once
#include"EfficiencyLibProduct.h"
#include "CurveData.h"
#include "CapFloorVolatility.h"
//#include "LMMEfficiencyEngine.h"
#include "LMMEfficiencyEngine.h"
#include "CapFloorEfficiencyPricer.h"
#include "DllHeader.h"
#include "NumerixCapFloor.h"

#include "NumeriX/Pro/Instruments/CapFloor.h"
#include "NumeriX/Pro/Analytics/CapFloorAnalytic.h"
#include "NumeriX/Pro/Date.h"

#include "NumeriX/Pro/Models/Model.h"

using namespace QuantLib;







class EFFICIEBCYLIBDLL_API MustCapFloor : public EfficiencyLibProduct
{
public:
	MustCapFloor(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdldoc, string trId);
	void setComponentsQuantLib();
	void makeCapFloor(Handle<QuantLib::YieldTermStructure> forwardingTermStructure);
	virtual ~MustCapFloor(){}
	/*MustCapFloor(ComponentPrincipalMust principal, ComponentCashFlowMust floatingLeg, ComponentIndexMust index, DateMust startDate, DateMust endDate, RateMust fixe_dRate);
	double Price(Handle<QuantLib::YieldTermStructure> discountingTermStructure, Handle<QuantLib::YieldTermStructure> forwardingTermStructure, int i, string pricingEngineName) override;
	boost::shared_ptr< PricingEngine >  SetPricingEngine(string pricingEngineName, Handle<QuantLib::YieldTermStructure> discountingTermStructure, Handle<QuantLib::YieldTermStructure> forwardingTermStructure) override;
	double PriceNada_Imane();*/
	virtual Real price();
	boost::shared_ptr<BlackCapFloorEngine> setEngineQuantLibBalckFT();
	boost::shared_ptr<AnalyticCapFloorEngine>  setEngineQuantLibLMM();
	void setEngineQuantLibHullWhite();
	boost::shared_ptr<NumeriX::Pro::CapFloorAnalytic> setEngineNumerixBlackFT();
	boost::shared_ptr<NumerixCapFloor> setEngineNumerixHW();
	//Test
	//Real price2(LMMEfficiencyEngine engine);


#pragma region"CapFloor s Parametres"
private:
	//index
	boost::shared_ptr<IborIndex> myIndex ;
	//Leg
	Leg floatingLeg;
	/* Rate */
	std::vector<Real> Rates;
	vector<double>  numerixRates;
	EfficiencyProduct::EfficiencyModelProduct modelProduct;
	/*Maket data*/
	Handle<OptionletVolatilityStructure> caplets;
	boost::shared_ptr<CapFloorTermVolSurface> capVolSurface;
	//RelinkableHandle<OptionletVolatilityStructure> caplets;
	Handle<OptionletVolatilityStructure> capletsVolatilies;
	CapFloorVolatility cVol;
	RelinkableHandle<YieldTermStructure> forwardingTermStructure;

	boost::shared_ptr<NumerixCapFloor> numerixCapFloor;
	boost::shared_ptr<NumeriX::Pro::CapFloor> numerixCapFloorInstrument;

public:
	/* TermStructure*/
	boost::shared_ptr<QuantLib::CapFloor> capfloor;


#pragma endregion 
	Real computeDelta(Real shift);
	Real computeGamma(Real shift);
	Real computeVega();
	Real computeTheta();


	//for LMM Efficiency
	float CalculPrixRelatifCap(float strike, Matrix &matForward, std::vector<QuantLib::Date> &dates, QuantLib::Date debut, QuantLib::Date fin, QuantLib::Date datedecalcul){
		int n = dates.size();
		std::vector<Time> D(n);
		float ret = 0;
		std::vector<Time> delta(n);
		int i = 0;
		int j = 0;
		int initial = 0;
		int finale = 0;
		int M = 0;
		int datecalcul = 0;
		float strikeprice = 0;

		float prixducap = 0;
		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;
		}

		for (int j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
		}
		delta[n - 1] = delta[n - 2];

		for (i = 0; i < n; i++){

			if (dates[i] == debut) {
				initial = i;
				//std::cout <<"la valeur initial"<< initial << std::endl;
			}

			if (dates[i] == fin) {
				finale = i;
				//std::cout << "la valeur final "<<finale << std::endl;
			}

			if (dates[i] == datedecalcul){

				datecalcul = i;
				//std::cout << "la valeur du debut"<<datecalcul << std::endl;
			}
		}

		Matrix ZC = PrixZCForward(matForward, dates);

		M = finale - initial + 2;
		std::vector<float> price(M);
		std::vector<float> pricefinal(M);

		for (i = initial; i<finale + 1; i++){

			if (matForward[i][i]>strike) {
				//std::cout << "le taux superieur au strike :" << matForward[i][i] << std::endl;
				strikeprice = float((matForward[i][i] - strike)*delta[i] * ZC[i][i]);//la relation des caplets
				for (j = initial; j < i; j++){
					price[j] = Relationindirectinf(j, ZC, i, strikeprice, finale);

				}

				price[i] = strikeprice / ZC[finale][i];

				for (j = i + 1; j < finale; j++){
					price[j] = Relationindirectsup(j, ZC, i, strikeprice, finale);
				}

				price[finale] = calculprixrelatifmaturity(j, ZC, i, strikeprice, finale, delta);

				for (j = initial; j < finale + 1; j++){
					prixducap = prixducap + price[j];
				}

				prixducap = prixducap*ZC[finale][datecalcul] / M;
				pricefinal[i] = prixducap;
				//std::cout << "Prix du caplet � l'instant T" <<i<<":"<< pricefinal[i] << std::endl;

			}
			else {
				pricefinal[i] = 0;
				//std::cout << "Prix du caplet � l'instant T" << i << ":" << pricefinal[i] << std::endl;

			}
		}

		for (i = initial; i < finale + 1; i++){
			ret = ret + pricefinal[i];
		}

		return ret;
	}
	Matrix PrixZCForward(Matrix &matForward, std::vector<QuantLib::Date> &dates){
		int n = dates.size();
		std::vector<Time> delta(n);
		std::vector<Time> D(n);
		Matrix ZC(n, n);
		int i, j;
		for (i = 0; i < n; i++){
			D[i] = daysBetween(QuantLib::Date(6, October, 2014), dates[i]) / 360;

		}
		for (int j = 0; j < n - 1; j++){
			delta[j] = D[j + 1] - D[j];
		}
		delta[n - 1] = delta[n - 2];



		for (j = 0; j < n; j++){
			for (i = 0; i < j; i++){
				ZC[i][j] = 0;
			}

			for (i = j; i < n; i++){

				ZC[i][j] = 1 / float(1 + delta[j] * matForward[i][j]);

			}
		}

		return ZC;
	}
	float Relationindirectinf(int indice, Matrix &ZC, int strikeindice, float strikeprice, int fin){
		//Calcul du prix relatif � partir du prix strike du cap (cas inferieur)
		float Prelatifinf = 0;
		float produitzerocoupon = 1 / ZC[fin][indice];

		for (int i = indice; i < strikeindice + 1; i++){
			produitzerocoupon = produitzerocoupon*(float)ZC[i][i];
		}

		Prelatifinf = float(strikeprice * produitzerocoupon);

		return Prelatifinf;
	}
	float Relationindirectsup(int indice, Matrix &ZC, int strikeindice, float strikeprice, int fin){
		//Calcul du prix relatif � partir du prix strike du cap (cas superieur) ;
		float Prelatif = 0;
		float produitzerocoupon = 1;

		for (int i = strikeindice; i < fin + 1; i++){

			produitzerocoupon = produitzerocoupon*ZC[i][i];

		}

		Prelatif = float(strikeprice / produitzerocoupon);

		return Prelatif;
	}
	float calculprixrelatifmaturity(int indice, Matrix &ZC, int strikeindice, float strikeprice, int fin, std::vector<Time> &delta){
		float Prelatif = 0;
		float produitzerocoupon = 1 / ZC[strikeindice][strikeindice];

		for (int i = indice; i < fin; i++){
			produitzerocoupon = produitzerocoupon*(float)(1 + delta[i + 1] * ZC[i + 1][i + 1]);
		}

		Prelatif = float(strikeprice * produitzerocoupon);

		return Prelatif;
	}
};