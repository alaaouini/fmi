#include "NumerixIndexMust.h"



NumerixIndexMust::NumerixIndexMust()
{
}


NumerixIndexMust::~NumerixIndexMust()
{
}


void NumerixIndexMust::setComponent(TiXmlDocument doc, string nameIndex, string tradeId)
{
	int test = 0;
	TiXmlElement *Component;
	TiXmlElement *ComponentSpread;
	TiXmlNode *IndexPointer;

	TiXmlNode *SpreadPointer;

	spreadString = "";
	// le Id du trade qu'on cherche
	TiXmlHandle docMUSTHandle(&doc);
	TiXmlElement *Trade = docMUSTHandle.FirstChild("BODY").FirstChild("TRADELIST").FirstChild("MUST_TR").ToElement();

	while (Trade)
	{
		if (strcmp(Trade->FirstChild("TradeId")->ToElement()->GetText(), tradeId.c_str()) == 0)
		{
			Component = Trade->FirstChild("MPTradeData")->FirstChild("MPTRDATA")->FirstChild("MPTrDataXML")->FirstChild("MPTRDATAXML")->
				FirstChild("STRUCTURED_INSTRUMENT")->FirstChild("COMPONENT_LIST")->ToElement();

			IndexPointer = Component->FirstChild("COMPONENT_INDEX");
			while (IndexPointer)
			{
				if (strcmp(IndexPointer->ToElement()->Value(), "COMPONENT_INDEX") == 0) // pour se limiter � la composante index
				{

					if (strcmp(IndexPointer->ToElement()->FirstChild("NAME")->ToElement()->GetText(), nameIndex.c_str()) == 0)
					{

						if (IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("CCY") &&
							IndexPointer->ToElement()->FirstChild("INTBASIS")->ToElement() &&
							IndexPointer->ToElement()->FirstChild("RESET_SCHED")->FirstChild("BASIC_SCHED")->FirstChild("SCHED_DEF")->FirstChild("FREQ")->ToElement() &&
							IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("INDEX")->ToElement() &&
							IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("TERM")->ToElement())
						{
							IndexCcy = IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("CCY")->ToElement()->GetText();

							IndexBasis = IndexPointer->ToElement()->FirstChild("INTBASIS")->ToElement()->GetText();

							IndexFreq = IndexPointer->ToElement()->FirstChild("RESET_SCHED")->FirstChild("BASIC_SCHED")->FirstChild("SCHED_DEF")->FirstChild("FREQ")->ToElement()->GetText();

							IndexIndex = IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("INDEX")->ToElement()->GetText();
							IndexTerm = IndexPointer->ToElement()->FirstChild("INDEX_CHAR")->FirstChild("TERM")->ToElement()->GetText();

						}
						SpreadPointer = IndexPointer->ToElement()->FirstChild("SPREAD_DEF")->FirstChild("BP");
						if (SpreadPointer)
						{
							spreadString = SpreadPointer->ToElement()->GetText();
							cout << "1 NumerixIndexMust::setComponent spreadString:" << spreadString << endl;
						}

						if (spreadString == "")
						{
							ComponentSpread = Trade->FirstChild("MPTradeData")->FirstChild("MPTRDATA")->FirstChild("MPTrDataXML")->FirstChild("MPTRDATAXML")->FirstChild("STRUCTURED_INSTRUMENT")->FirstChild("VARIABLE_LIST")->FirstChild("VARIABLE")->ToElement();
							while (ComponentSpread)
							{
								if (strcmp(ComponentSpread->Value(), "VARIABLE") == 0 && strcmp(ComponentSpread->FirstChild("TYPE")->ToElement()->GetText(), "MATRIX") == 0)
								{
									if (strcmp(ComponentSpread->FirstChild("NAME")->ToElement()->GetText(), "MatrixSpread") == 0)
									{
										MatriceMust* matrixOfSpread = new MatriceMust();
										matrixOfSpread->setComponent(doc, "MatrixSpread", tradeId);
										matrixSpread = this->eff_matice_spread(matrixOfSpread->matrice);
										for (std::size_t sizeMat = 0; sizeMat < matrixOfSpread->matrice.size(); sizeMat++)
										{

											spreads.push_back(std::get<2>(matrixSpread[sizeMat]));
											cout << "2 NumerixIndexMust::setComponent spreadString:" << std::get<2>(matrixSpread[sizeMat]) << endl;
										}
										test++;
									}
								}
								ComponentSpread = ComponentSpread->NextSiblingElement();
							}
							test++;
						}
						else
						{
							spreads.push_back(this->eff_convert_rate(spreadString));
							test++;
						}

					}

				}
				IndexPointer = IndexPointer->NextSibling();
			}


		}

		Trade = Trade->NextSiblingElement();
	}

	if (IndexCcy == "" || IndexBasis == "" || IndexFreq == "" || IndexIndex == "" || IndexTerm == "")
	{
		throw string("Erreur dans la composante INDEX !");

	}
	else
	{
		numerixIndexFrequency = "";
		numerixIndexFrequency += IndexTerm[0];
		numerixIndexFrequency += "M";
	}
	if (spreads.size() > 1) spread = spreads[0]; else spread = 0.0;
	cout << "3 NumerixIndexMust::setComponent spread:" << spread << endl;
}



string NumerixIndexMust::eff_numerix_convert_frequency(char freq_char) const
{
	//cette fonction fait la conversion 
	//de la frequence de type string lue de XML de MUST
	string result;

	switch (freq_char)
	{
	case 'A':
		result = "1Y";
		break;
	case 'S':
		result = "6M";
		break;
	case 'Q':
		result = "3M";
		break;
	case 'M':
		result = "1M";
		break;
	case 'W':
		result = "1W";
		break;
	
	}

	return result; // retourne frequency de type QuantLib
}