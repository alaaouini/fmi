#ifndef EFFICIENCYLIBPRODUCT_H
#define EFFICIENCYLIBPRODUCT_H
#pragma once
//#include <C:/local/boost_1_60_0/boost/algorithm/string.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include "Portefeuille.h"
#include "EfficiencyLibComponents.h"
#include "Mapping.h"
#include "DllHeader.h"
#include "NumeriXPrincipalMust.h"
#include "NumerixIndexMust.h"
#include "NumerixDateMust.h"
#include "NumerixRateMust.h"
#include "NumerixCashFlowMust.h"


#include "NumeriX/Pro/Application.h"
#include "NumerixMarketData.h"



class EFFICIEBCYLIBDLL_API EfficiencyLibProduct:public NumerixMarketData {

public:
	//EfficiencyLibProduct(){}
	EfficiencyLibProduct(QuantLib::Date valuationDate, EfficiencyProduct::EfficiencyTypeProduct typeProd, string paths, EfficiencyProduct::EfficiencyModelProduct modelProd, TiXmlDocument hdoc, string trId) :today(valuationDate), typeProduct(typeProd), modelProduct(modelProd), doc(hdoc), trade_id(trId){
		tab_mapp = Mapping(paths, typeProd);
		}
	
	virtual ~EfficiencyLibProduct(){}

	NumeriX::Pro::Application nxApp;
private: 

	

	EfficiencyProduct::EfficiencyTypeProduct typeProduct;
	EfficiencyProduct::EfficiencyModelProduct modelProduct;
	string trade_id;
	//TiXmlHandle Xmlhdldoc;
	TiXmlDocument doc;

	/*Table de Mapping*/
	Mapping tab_mapp;

public:
	void EfficiencyLibProduct::setComponentsForSwap(){

		/* Mapping ... � voir  */
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("FixedRateQuantLib");
		string fixedLegMust = tab_mapp.search_mapping("FixedLegQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");

		// nominal
		ComponentPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = ComponentPrincipalMust.nominal[0];

		// settlementDate
		startDateObj.setComponent(doc, startDateMust, trade_id);
		QuantLib::Date settlementDate = startDateObj.dateQ;

		// maturity
		endDateObj.setComponent(doc, endDateMust, trade_id);
		QuantLib::Date maturity = endDateObj.dateQ;

		// fixedRate
		fixedRateObj.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = fixedRateObj.rates[0];
		//Rate fixedRate = 0.011;
		fixedRateObj.SetMatrixRate(settlementDate, maturity, fixedRateObj.rates);

		//=============FIXED LEG================
		fixedLegObj.setComponent(doc, fixedLegMust, trade_id);
		Frequency fixedLegFrequency = fixedLegObj.freqQ;
		DayCounter fixedLegDayCounter = fixedLegObj.basisQ;

		//=============FLOATING LEG================
		floatingLegObj.setComponent(doc, floatingLegMust, trade_id);
		Frequency floatingLegFrequency = floatingLegObj.freqQ;
		DayCounter floatingLegDayCounter = floatingLegObj.basisQ;

		// Index
		ComponentIndexMust.setComponent(doc, indexMust, trade_id);
		ComponentIndexMust.SetMatrixSpread(settlementDate, maturity, ComponentIndexMust.spreads);

	}

	void EfficiencyLibProduct::setComponentsForNumerixSwap() {

		/* Mapping ... � voir  */
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("FixedRateQuantLib");
		string fixedLegMust = tab_mapp.search_mapping("FixedLegQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");

		// nominal
		NumeriXPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = NumeriXPrincipalMust.nominal;

		// settlementDate
		NumerixstartDateObj.setComponent(doc, startDateMust, trade_id);
		//QuantLib::Date settlementDate = startDateObj.dateQ;

		// maturity
		NumerixmaturityObj.setComponent(doc, endDateMust, trade_id);
		//QuantLib::Date maturity = endDateObj.dateQ;

		// fixedRate
		numerixFixedRate.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = fixedRateObj.rates[0];
		//Rate fixedRate = 0.011;
		//fixedRateObj.SetMatrixRate(settlementDate, maturity, fixedRateObj.rates);

		//=============FIXED LEG================
		NumerixfixedLeg.setComponent(doc, fixedLegMust, trade_id);
		//Frequency fixedLegFrequency = fixedLegObj.freqQ;
		//DayCounter fixedLegDayCounter = fixedLegObj.basisQ;

		//=============FLOATING LEG================
		NumerixfloatingLeg.setComponent(doc, floatingLegMust, trade_id);
		//Frequency floatingLegFrequency = floatingLegObj.freqQ;
		//DayCounter floatingLegDayCounter = floatingLegObj.basisQ;

		// Index
		NumeriXIndexMust.setComponent(doc, indexMust, trade_id);
		//ComponentIndexMust.SetMatrixSpread(settlementDate, maturity, ComponentIndexMust.spreads);

	}
	void EfficiencyLibProduct::setComponentsForCapFloor(){
		/* Mapping .. � vvoir*/
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("StrikeQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");
		string capOrFloor = tab_mapp.search_mapping("CapOrFloor");

		/* Cap or Floor */
		capFloorType.setComponent(doc, capOrFloor, trade_id);
		string isCapOfFloor = capFloorType.valeur;


		// nominal
		ComponentPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = ComponentPrincipalMust.nominal[0];


		// settlementDate
		startDateObj.setComponent(doc, startDateMust, trade_id);
		QuantLib::Date settlementDate = startDateObj.dateQ;

		// maturity
		endDateObj.setComponent(doc, endDateMust, trade_id);
		QuantLib::Date maturity = endDateObj.dateQ;

		// fixedRate
		fixedRateObj.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = fixedRateObj.rates[0];
		fixedRateObj.SetMatrixRate(settlementDate, maturity, fixedRateObj.rates);


		//=============FLOATING LEG================
		floatingLegObj.setComponent(doc, floatingLegMust, trade_id);
		Frequency floatingLegFrequency = floatingLegObj.freqQ;
		DayCounter floatingLegDayCounter = floatingLegObj.basisQ;


		// Index
		ComponentIndexMust.setComponent(doc, indexMust, trade_id);
		ComponentIndexMust.SetMatrixSpread(settlementDate, maturity, ComponentIndexMust.spreads);
	}


	void EfficiencyLibProduct::setComponentsForNumerixCapFloor() {
		/* Mapping .. � voir*/
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("StrikeQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");
		string capOrFloor = tab_mapp.search_mapping("CapOrFloor");

		// nominal
		NumeriXPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = NumeriXPrincipalMust.nominal;

		// settlementDate
		NumerixstartDateObj.setComponent(doc, startDateMust, trade_id);

		// maturity
		NumerixmaturityObj.setComponent(doc, endDateMust, trade_id);

		// fixedRate
		numerixFixedRate.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = numerixFixedRate.rates[0];

		//=============FLOATING LEG================
		NumerixfloatingLeg.setComponent(doc, floatingLegMust, trade_id);

		// Index
		NumeriXIndexMust.setComponent(doc, indexMust, trade_id);

		//****************************
		/* Cap or Floor */
		capFloorType.setComponent(doc, capOrFloor, trade_id);
		string isCapOfFloor = capFloorType.valeur;

	}

	void EfficiencyLibProduct::setComponentsForSwaption(){
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string maturityOption = tab_mapp.search_mapping("maturityOptionQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string option = tab_mapp.search_mapping("OptionQuantLib");
		string fixedLegMust = tab_mapp.search_mapping("FixedLegQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("FixedRateQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");
		string typeOp = tab_mapp.search_mapping("OptionType");
		
		// nominal
		ComponentPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = ComponentPrincipalMust.nominal[0];

		// style de l'option
		styleOptionObj.setComponent(doc, typeOp, trade_id);
		style = EfficiencyProduct::stringToSwaptionStyle(styleOptionObj.valeur);

		//switch (style){
		//	case EfficiencyProduct::SwaptionStyle::EURO: /* stratOpDate; startSwpadate = endOpDate;  endOpSwapDate*/
		//	{
		//		// First Date : Option
		//		startDateObj.setComponent(doc, startDateMust, trade_id);

		//		// maturit� de l'option
		//		maturityObj.setComponent(doc, maturityOption, trade_id);

		//		// date de fin de la swaption
		//		endDateObj.setComponent(doc, endDateMust, trade_id);
		//	}
		//	break;
		//	case EfficiencyProduct::SwaptionStyle::AMER: /* stratOpDate= startSwpaDate + 2j, endOpDate = EarliestDate;  endOpSwapDate*/
		//	{
		//		// First Date : Option
		//		startDateObj.setComponent(doc, startDateMust, trade_id);

		//		// maturit� de l'option
		//		maturityObj.setComponent(doc, maturityOption, trade_id);

		//		// date de fin de la swaption
		//		endDateObj.setComponent(doc, endDateMust, trade_id);
		//	}
		//	break;
		//	case EfficiencyProduct::SwaptionStyle::BERM:
		//	{
		//		/*recup�rer la freuquence*/
		//	}
		//	break;
		//	default:
		//	{

		//	}
		//	break;

		//}
		// First Date : Option
		startDateObj.setComponent(doc, startDateMust, trade_id);
		QuantLib::Date startDate = startDateObj.dateQ;

		// maturit� de l'option
		maturityObj.setComponent(doc, maturityOption, trade_id);
		QuantLib::Date maturity = maturityObj.dateQ;

		// date de fin de la swaption
		endDateObj.setComponent(doc, endDateMust, trade_id);
		QuantLib::Date endDate = endDateObj.dateQ;

		// fixedRate
		fixedRateObj.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = fixedRateObj.rates[0];
		fixedRateObj.SetMatrixRate(maturity, endDate, fixedRateObj.rates);

		// Option
		componentOption.setComponents(doc, option, trade_id);


		//=============FIXED LEG================
		fixedLegObj.setComponent(doc, fixedLegMust, trade_id);
		Frequency fixedLegFrequency = fixedLegObj.freqQ;
		DayCounter fixedLegDayCounter = fixedLegObj.basisQ;

		//=============FLOATING LEG================
		floatingLegObj.setComponent(doc, floatingLegMust, trade_id);
		Frequency floatingLegFrequency = floatingLegObj.freqQ;
		DayCounter floatingLegDayCounter = floatingLegObj.basisQ;

		// Index
		ComponentIndexMust.setComponent(doc, indexMust, trade_id);
		ComponentIndexMust.SetMatrixSpread(startDate, endDate, ComponentIndexMust.spreads);

	}


	void EfficiencyLibProduct::setComponentsForNumerixSwaption() {
		string nominalMust = tab_mapp.search_mapping("NominalQuantLib");
		string startDateMust = tab_mapp.search_mapping("startDateQuantLib");
		string maturityOption = tab_mapp.search_mapping("maturityOptionQuantLib");
		string endDateMust = tab_mapp.search_mapping("endDateQuantLib");
		string option = tab_mapp.search_mapping("OptionQuantLib");
		string fixedLegMust = tab_mapp.search_mapping("FixedLegQuantLib");
		string floatingLegMust = tab_mapp.search_mapping("FloatingLegQuantLib");
		string fixedRateMust = tab_mapp.search_mapping("FixedRateQuantLib");
		string indexMust = tab_mapp.search_mapping("IndexQuantLib");
		string typeOp = tab_mapp.search_mapping("OptionType");

		// nominal
		NumeriXPrincipalMust.setComponent(doc, nominalMust, trade_id);
		Real nominal = NumeriXPrincipalMust.nominal;

		// style de l'option
		styleOptionObj.setComponent(doc, typeOp, trade_id);
		style = EfficiencyProduct::stringToSwaptionStyle(styleOptionObj.valeur);

		// First Date : Option
		NumerixstartDateObj.setComponent(doc, startDateMust, trade_id);

		// maturit� de l'option
		NumerixmaturityObj.setComponent(doc, maturityOption, trade_id);

		// date de fin de la swaption
		NumerixendDateObj.setComponent(doc, endDateMust, trade_id);

		// fixedRate
		numerixFixedRate.setComponent(doc, fixedRateMust, trade_id);
		Rate fixedRate = numerixFixedRate.rates[0];

		//=============FIXED LEG================
		NumerixfixedLeg.setComponent(doc, fixedLegMust, trade_id);

		//=============FLOATING LEG================
		NumerixfloatingLeg.setComponent(doc, floatingLegMust, trade_id);

		//================Index====================
		NumeriXIndexMust.setComponent(doc, indexMust, trade_id);
		float spread = NumeriXIndexMust.spread;
	}

	virtual Real price() = 0;//Just for tests
	public: Real npvFixedLeg = 0;
			Real npvFloatingLeg = 0;
			Real delta = 0;
			Real gamma = 0;
			Real vega = 0;
			Real theta = 0;
			QuantLib::Date today;
		//	RiskParameter RiskParameter;

	ComponentCashFlowMust ComponentCFlowMust;
	EnumMust styleOptionObj;
	EnumMust capFloorType;
	MatriceMust MatriceMust;

	/* Les Composantes Quantlib Must*/
	ComponentCashFlowMust floatingLegObj;
	ComponentCashFlowMust fixedLegObj;
	 ComponentIndexMust ComponentIndexMust;
	 ComponentPrincipalMust ComponentPrincipalMust;
	 DateMust startDateObj;
	 DateMust endDateObj;
	 DateMust maturityObj;
	 RateMust fixedRateObj;
	 

	 /* Les Composantes NumeriX Must*/
	 NumerixCashFlowMust NumerixfloatingLeg;
	 NumerixCashFlowMust NumerixfixedLeg;
	 NumerixIndexMust NumeriXIndexMust;
	 NumeriXPrincipalMust NumeriXPrincipalMust;
	 NumerixDateMust NumerixstartDateObj;
	 NumerixDateMust NumerixmaturityObj;
	 NumerixRateMust numerixFixedRate;
	 NumerixDateMust NumerixendDateObj;


	 SwapFloatingLeg swapFloatingLeg;
	 SwapFixedLeg swapFixedLeg;
	 ComponentOptionMust componentOption;
	 EfficiencyProduct::SwaptionStyle style;




}; 
#endif